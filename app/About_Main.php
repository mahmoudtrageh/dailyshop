<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About_Main extends Model
{
    protected $table='about_main';
     protected  $fillable=[

             'title',
             'content',
             'date',
             'img',

         ];
}
