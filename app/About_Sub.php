<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About_Sub extends Model
{
    protected $table='about_sub';
    protected  $fillable=[

        'title1',
        'title2',
        'title3',
        'content1',
        'content2',
        'content3',


    ];
}
