<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected  $fillable = [

        'code',
        'limit',
        'discount',
        'users_count',
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice', 'code');
    }
}
