<?php

namespace App\Http\Controllers\Admin;

use App\About_Main;
use App\About_Sub;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
public function __construct()
    {
        $this->middleware('HasPermission:7');
    }

    public function index()

    {
        $main = About_Main::first();
        $sub = About_Sub::first();
        return view('admin.about', compact('main', 'sub'));

    }

    public function edit_about_main(Request $request)

    {
        $this->validate($request, [

            'title' => 'required',
            'content' => 'required',

        ],

            [
                'title.required' => 'برجاء إدخال العنوان',
                'content.required' => 'برجاء إدخال المحتوي',

            ]);

        $input = $request->all();
        $input['date'] = today()->toDateString();
        $checker = About_Main::first();

        if ($file = $request->file('img'))
        {
            @unlink(base_path('images/'.$checker->img));
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(base_path('/images'), $name);
            $input['img'] = $name;

        }
        if (!$file = $request->file('img'))
        {
            $input['img'] = $checker->img;
        }
        if ($checker->update($input)) {
            session()->flash('success', 'تم تعديل البيانات بنجاح');
            return redirect()->back();
        }

        return redirect()->back()->withInput($request->all());

    }

    public function edit_about_sub(Request $request)

    {
        $this->validate($request, [

            'title1' => 'required',
            'title2' => 'required',
            'title3' => 'required',
            'content1' => 'required',
            'content2' => 'required',
            'content3' => 'required',

        ],

            [
                'title1.required' => 'برجاء إدخال العنوان الأول',
                'title2.required' => 'برجاء إدخال العنوان الثانى',
                'title3.required' => 'برجاء إدخال العنوان الثالث',
                'content1.required' => 'برجاء إدخال المحتوي الأول',
                'content2.required' => 'برجاء إدخال المحتوي الثانى',
                'content3.required' => 'برجاء إدخال المحتوي الثالث',

            ]);

        if (About_Sub::first()->update($request->all()))

        {

            session()->flash('success', 'تم تعديل البيانات بنجاح');
            return redirect()->back();

        }
        return redirect()->back()->withInput($request->all());

    }
}
