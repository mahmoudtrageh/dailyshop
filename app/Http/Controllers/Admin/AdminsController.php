<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Gate;
use Illuminate\Http\Request;

class AdminsController extends Controller
{
 public function __construct()
    {
        $this->middleware('HasPermission:1');
    }

    public function index()

    {
        $roles = Role::all();
        $admins = User::where('id', '!=', 1)->where('user_type','admin')->get();
        return view('admin.admin', compact('roles', 'admins'));
    }

    public function add_admins(Request $request)

    {
        $this->validate($request, [

            'name' => 'required|unique:users,name',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6|max:15',
            'role_id' => 'required',

        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
                'password.required' => 'برجاء إدخال كلمه المرور',
                'password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                'password.max' => 'كلمه المرور لا تزيد عن 15 حوف',
                'role_id.required' => 'برجاء إختيار صلاحيه واحده على الأقل',

            ]);

        $input = $request->all();
        $input['password']   = bcrypt($request->password);
        $input['user_type'] = 'admin';
        if ($user = User::create($input)) {
            {
                $user->role()->Attach(Role::find($input['role_id']));
            }
            session()->flash('success', 'تم إضافه المشرف بنجاح');
            return redirect()->back();
        }

        return redirect()->back()->withInput($request->all())->exceptInput($request->password);

    }

    public function edit_admins(Request $request)
    {

        $input = $request->all();
        $admin = User::find($input['admin_id']);

        if ($request->password == null) {
            $this->validate($request, [

                'name' => 'required|unique:users,name,' . $admin->id,
                'email' => 'required|unique:users,email,' . $admin->id,
                'role_id' => 'required',

            ],

                [
                    'name.required' => 'برجاء إدخال الإسم',
                    'name.unique' => 'الإسم مسجل بالفعل',
                    'email.required' => 'برجاء إدخال البريد الإلكترونى',
                    'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
                    'role_id.max' => 'برجاء إختيار صلاحيه واحده على الأقل',

                ]);
        } else {

            $this->validate($request, [

                'name' => 'required|unique:users,name,' . $admin->id,
                'email' => 'required|unique:users,email,' . $admin->id,
                'password' => 'required|min:6|max:15',
                'role_id' => 'required',

            ],

                [
                    'name.required' => 'برجاء إدخال الإسم',
                    'name.unique' => 'الإسم مسجل بالفعل',
                    'email.required' => 'برجاء إدخال البريد الإلكترونى',
                    'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
                    'password.required' => 'برجاء إدخال كلمه المرور',
                    'password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                    'password.max' => 'كلمه المرور لا تزيد عن 15 حوف',
                    'role_id.max' => 'برجاء إختيار صلاحيه واحده على الأقل',

                ]);

        }

        if ($request->password != null) {
            $input['password'] = bcrypt($request->password);
        } else {
            $input['password'] = $admin->password;

        }
        if ($admin->update($input)) {
            $admin->role()->sync(Role::find($input['role_id']));
            session()->flash('success', 'تم تعديل البيانات بنجاح');
            return redirect()->back();
        }

    }

    public function delete_admins(Request $request)

    {

        $admin=User::find($request->admin_id);
        $admin->role()->detach();
        $admin->delete();
        session()->flash('deleted','تم حذف المشرف بنجاح');
        return redirect()->back();

    }
}
