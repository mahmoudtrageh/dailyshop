<?php

namespace App\Http\Controllers\Admin;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
public function __construct()
    {
        $this->middleware('HasPermission:9');
    }

    public function index($id)

    {
        $country_id = $id;
        $cities = City::where('country_id',$id)->get();
        return view('admin.cities', compact('cities', 'country_id'));
    }

    public function add_cities(Request $request)

    {

        $this->validate($request, [
            'name' => 'required|unique:cities,name',
            'delivery' => 'required|numeric',
        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => ' المدينه مسجله بالفعل',
                'delivery.required' => ' برجاء إدخال سعر الشحن',
                'delivery.numeric' => 'برجاء إدخال سعر الشحن بصيغه أرقام',

            ]);

        if (City::create($request->all())) {

            return redirect()->back()->withSuccess('تم إضافه المدينه بنجاح');

        } else {
            return redirect()->back()->withInput($request->all());
        }
    }

    public function edit_cities(Request $request)

    {

        $this->validate($request, [
            'name' => 'required|unique:cities,name',
            'delivery' => 'required|numeric',
        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => ' المدينه مسجله بالفعل',
                'delivery.required' => ' برجاء إدخال سعر الشحن',
                'delivery.digits' => 'برجاء إدخال سعر الشحن بصيغه أرقام',

            ]);

        if (City::find($request->city_id)->update($request->all())) {

            session()->flash('success', 'تم تعديل المدينه بنجاح');
            return redirect()->back();

        } else {
            return redirect()->back()->withInput($request->all());
        }

    }

    public function delete_cities(Request $request)

    {
        $del = City::find($request->city_id);
        $del->delete();
        session()->flash('deleted', 'تم حذف المدينه بنجاح');
        return redirect()->back();

    }
}
