<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use DemeterChain\C;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountriesController extends Controller
{
public function __construct()
    {
        $this->middleware('HasPermission:9');
    }

    public function index()

    {
        $countries = Country::all();
        return view('admin.country', compact('countries'));
    }

    public function add_countries(Request $request)

    {
        $this->validate($request, [

            'name' => 'required|unique:countries,name',
        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => ' الدوله مسجله بالفعل',

            ]);
        if (Country::create($request->all())) {
            session()->flash('success', 'تم إضافه الدوله بنجاح');
            return redirect()->back();

        }

        return redirect()->back()->withInput($request->all());

    }

    public function edit_countries(Request $request)

    {
        $checker = Country::find($request->country_id);
        $this->validate($request, [

            'name' => 'required|unique:countries,name,' . $checker->id,
        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'إسم الدوله مسجل بالفعل',

            ]);

        if ($checker->update($request->all())) {
            session()->flash('success', 'تم تعديل الدوله بنجاح');
            return redirect()->back();

        }
        return redirect()->back()->withInput($request->all());

    }

    public function delete_countries(Request $request)
    {
        $del = Country::find($request->country_id);
        City::where('country_id',$request->country_id)->delete();
        $del->delete();
        session()->flash('deleted', 'تم حذف الدوله بنجاح');
        return redirect()->back();
    }
}
