<?php

namespace App\Http\Controllers\Admin;


use App\Coupon;
use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponsController extends Controller
{
    public function index()
    {
        $coupons = Coupon::all();
        foreach($coupons as $coupon){


        $invoices = Invoice::where('coupon', $coupon->code)->get();

        }
        return view('admin.coupons', compact('coupons', 'invoices'));
    }

    public function add_coupons(Request $request)

    {

        $this->validate($request, [

            'limit' => 'required|numeric',
            'discount' => 'required|numeric',
        ],

            [
                'limit.required' => 'برجاء إدخال الحد الأقصى',
                'discount.unique' => 'برجاء إدخال نسبة الخصم',
            ]);

        $input = $request->all();
        $input['code'] = str_random(10);

        if ($coupon = Coupon::create($input)) {
            session()->flash('success', 'تم إضافه الكوبون بنجاح');
            return redirect()->back();
        }

        return redirect()->back()->withInput($request->all());


    }

    public function edit_coupons(Request $request)
    {

        $checker = Coupon::find($request->coupon_id);
        $input = $request->all();
        $this->validate($request, [

            'limit' => 'required|numeric',
            'discount' => 'required|numeric',

        ],

            [
                'limit.required' => 'برجاء إدخال الحد الأقصى',
                'discount.unique' => 'برجاء إدخال نسبة الخصم',
            ]);


        if ($checker->update($input)) {
            session()->flash('success', 'تم تعديل الكوبون بنجاح');
            return redirect()->back();
        }

        return redirect()->back();


    }

    public function delete_coupons(Request $request)

    {

        $coupon = Coupon::find($request->coupon_id);
        $coupon->delete();
        session()->flash('deleted', 'تم حذف الكوبون بنجاح');
        return redirect()->back();

    }
}
