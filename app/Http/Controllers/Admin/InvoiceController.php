<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Invoice;
use App\InvoiceDetail;
use App\Product;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
public function __construct()
    {
        $this->middleware('HasPermission:4');
    }

    public function index()
    {
        $invoices = Invoice::all();
        return view('admin.invoice', compact('invoices'));
    }

    public function details($id)
    {
        $invoices_details = InvoiceDetail::where('invoice_id', $id)->get();
        $invoice = Invoice::where('id', $id)->first();
        $signature = Setting::first();
        $delivery = City::where('name', $invoice->city)->first();
        $sub = array_sum($invoices_details->pluck('total')->toArray());
        return view('admin.invoice-details', compact('invoices_details', 'signature', 'invoice', 'delivery', 'sub'));
    }

    public function delete(Request $request)
    {
        foreach (InvoiceDetail::where('invoice_id', $request->invoice_id)->get() as $invoice) {
            $invoice->delete();
        }
        Invoice::where('id', $request->invoice_id)->first()->delete();
        return redirect()->back()->withsuccess('تم حذف الفاتوره بنجاح');
    }
}
