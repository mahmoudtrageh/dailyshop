<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{


    public  function index()
    {
        return view('admin.login');
    }
    public function login(Request $request){


        $this->validate($request, [
            'email'=>'required',
            'password'=>'required',
        ] , [
            'email.required'=>'برجاء إدخال إسم المتسخدم أو  البريد الإلكتروني',
            'password.required'=>'برجاء إدخال كلمه المرور',
        ]);


        if (Auth::attempt(['email'=>$request->email,'password'=>$request->password]))

        {

            return redirect('admin');

        }
        elseif (Auth::attempt(['name'=>$request->email,'password'=>$request->password]))
        {

            return redirect('admin');

        }

        session()->flash('login_error','برجاء التأكد من إسم المستخدم وكلمه المرور');
        return redirect()->route('login')->withInput($request->all())->exceptInput($request->password);
    }
    public function logout(){

        Auth::logout();
        return redirect()->route('login');

    }
}
