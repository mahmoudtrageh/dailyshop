<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{
public function __construct()
    {
        $this->middleware('HasPermission:6');
    }

    public function index()

    {
        $messages = Message::all();
        return view('admin.messages', compact('messages'));
    }

    public function delete_message($id)

    {
        Message::find($id)->delete();
        session()->flash('deleted', 'تم مسح الرساله بنجاح');
        return redirect()->back();

    }
    public function delete_all()

    {
        Message::truncate();
        session()->flash('deleted', 'تم مسح الرساله بنجاح');
        return redirect()->back();

    }
}
