<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notification;

class NotificationController extends Controller
{


    public function readNotiy(Request $request)
    {
        auth()->user()->unreadNotifications()->find($request->id)->MarkAsRead();
        return redirect()->route('messages');
    }
}
