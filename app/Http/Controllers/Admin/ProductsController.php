<?php

namespace App\Http\Controllers\Admin;

use App\Color;
use App\Product;
use App\Section;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;


class ProductsController extends Controller
{
public function __construct()
    {
        $this->middleware('HasPermission:3');
    }


    public function index()

    {
        $colors = Color::all();
        $sections = Section::all();
        $products = Product::all();
        return view('admin.products', compact('colors', 'sections', 'products'));


    }

    public function add_products(Request $request)

    {

        $this->validate($request, [

            'name' => 'required|unique:products,name',
            'code' => 'required|unique:products,code',
            'price_before' => 'required|numeric',
            'quantity' => 'required|numeric',
            'section_id' => 'required',
            'img' => 'required',
            'img2' => 'required',
            'color_id' => 'required',
            'description' => 'required',

        ],

            [
                'name.required' => 'برجاء إدخال إسم المنتج',
                'name.unique' => 'المنتج مسجل بالفعل',
                'code.required' => 'برجاء إدخال كود المنتج',
                'code.unique' => 'الكود مسجل بالفعل',
                'price_before.required' => 'برجاء إدخال سعر المنتج قبل الخصم',
                'price_before.numeric' => 'برجاء إدخال سعر المنتج قبل الخصم بصيغه أرقام',
                'quantity.required' => 'برجاء إدخال كميه المنتج ',
                'section_id.required' => 'برجاء إختيار القسم  ',
                'img.required' => 'برجاء إختيار الصوره  ',
                'img2.required' => 'برجاء إختيار صوره اخري  ',
                'color_id.required' => 'برجاء إختيار لون واحد على الأقل  ',
                'description.required' => 'برجاء إدخال وصف المنتج',

            ]);

        $input = $request->all();


        $file = $request->file('img');
        $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
        $input['img'] = $name;
        $img = Image::make($file);
//        $watermark=Image::make(base_path('images/').settings()->water_mark);
//        $watermark->resize(150, 150);
//        $img->insert($watermark,'bottom-right', 10, 10);
        $img->save(base_path('images/'.$name));

        $file2 = $request->file('img2');
        $name2 = rand(0000, 9999) . time() . '.' . $file2->getClientOriginalExtension();
        $input['img2'] = $name2;
        $img2 = Image::make($file2);
//        $watermark2 = Image::make(base_path('images/').settings()->water_mark);
//        $watermark2->resize(150, 150);
//        $img2->insert($watermark2,'bottom-right', 10, 10);
        $img2->save(base_path('images/'.$name2));

        if ($product = Product::create($input)) {
            $product->color()->Attach(Color::find($input['color_id']));
            session()->flash('success', 'تم إضافه المنتج بنجاح');
            return redirect()->back();
        }

        return redirect()->back()->withInput($request->all());


    }

    public function edit_products(Request $request)
    {

        $checker = Product::find($request->product_id);
        $input = $request->all();
        $this->validate($request, [

            'name' => 'required|unique:products,name,' . $checker->id,
            'code' => 'required|unique:products,code,' . $checker->id,
            'price_before' => 'required',
            'price_after' => 'required',
            'quantity' => 'required',
            'section_id' => 'required',
            'category' => 'required',
            'color_id' => 'required',
            'description' => 'required',

        ],

            [
                'name.required' => 'برجاء إدخال إسم المنتج',
                'name.unique' => 'المنتج مسجل بالفعل',
                'code.required' => 'برجاء إدخال كود المنتج',
                'code.unique' => 'الكود مسجل بالفعل',
                'price_before.required' => 'برجاء إدخال سعر المنتج قبل الخصم',
                'price_after.required' => 'برجاء إدخال سعر المنتج بعد الخصم',
                'quantity.required' => 'برجاء إدخال كميه المنتج ',
                'section_id.required' => 'برجاء إختيار القسم  ',
                'category.required' => 'برجاء إختيار الفئة  ',
                'color_id.required' => 'برجاء إختيار لون واحد على الأقل  ',
                'description.required' => 'برجاء إدخال وصف المنتج',

            ]);

        if ($file = $request->file('img')) {
            @unlink(base_path('images/' . $checker->img));
            $file = $request->file('img');
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $input['img'] = $name;
            // open an image file
            $img = Image::make($file);
//            $watermark=Image::make(base_path('images/').settings()->logo);
//            $watermark->resize(100, 100);
//            // and insert a watermark for example
//            $img->insert($watermark,'bottom-right', 1,1);

            // finally we save the image as a new file
            $img->save(base_path('images/'.$name));

        }
        if (!$file = $request->file('img')) {
            $input['img'] = $checker->img;
        }


        if ($file2 = $request->file('img2')) {
            @unlink(base_path('images/' . $checker->img2));
            $file2 = $request->file('img2');
            $name2 = rand(0000, 9999) . time() . '.' . $file2->getClientOriginalExtension();
            $input['img2'] = $name2;
            // open an image file
            $img2 = Image::make($file2);
//            $watermark2=Image::make(base_path('images/').settings()->logo);
//            $watermark2->resize(100, 100);
//            // and insert a watermark for example
//            $img2->insert($watermark2,'bottom-right', 1,1);

            // finally we save the image as a new file
            $img2->save(base_path('images/'.$name2));

        }
        if (!$file2 = $request->file('img2')) {
            $input['img2'] = $checker->img2;
        }

        if ($checker->update($input)) {
            $checker->color()->sync(Color::find($input['color_id']));
            session()->flash('success', 'تم تعديل القسم بنجاح');
            return redirect()->back();
        }

        return redirect()->back();


    }

    public function delete_products(Request $request)

    {

        $prodcut = Product::find($request->product_id);
        @unlink(base_path('images/' . $prodcut->img));
        $prodcut->color()->detach();
        $prodcut->delete();
        session()->flash('deleted', 'تم حذف المنتج بنجاح');
        return redirect()->back();

    }

    public function updateStatus(Request $request)
    {
        $service = Product::find($request->id);
        $service->is_active = $request->status;
        $service->save();
        $check = $service->is_active;
        return response()->json(["status" => "ok", 'check' => $check]);
    }
}
