<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Section;
use Image;
use Illuminate\Http\Request;

class SectionsController extends Controller
{
public function __construct()
    {
        $this->middleware('HasPermission:2');
    }

    public function index()

    {
        $sections = Section::all();
        return view('admin.sections', compact('sections'));

    }

    public function add_sections(Request $request)

    {
        $this->validate($request, [

            'name' => 'required|unique:sections,name',

        ],

            [
                'name.required' => 'برجاء إدخال إسم القسم',
                'name.unique' => 'القسم مسجل بالفعل',

            ]);

        $input = $request->all();
        
        $file = $request->file('img');
        $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
        $input['img'] = $name;
        $img = Image::make($file);
        //$watermark=Image::make(base_path('images/').settings()->water_mark);
       // $watermark->resize(150, 150);
       // $img->insert($watermark,'bottom-right', 10, 10);
        $img->save(base_path('images/'.$name));
        if ($user = Section::create($input)) {

            session()->flash('success', 'تم إضافه القسم بنجاح');
            return redirect()->back();
        }

        return redirect()->back()->withInput($request->all());


    }

    public function edit_sections(Request $request)
    {

        $input = $request->all();
        if ($file = $request->file('img')) {
            @unlink(base_path('images/' . $checker->img));
            $file = $request->file('img');
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $input['img'] = $name;
            // open an image file
            $img = Image::make($file);
          //  $watermark=Image::make(base_path('images/').settings()->logo);
          //  $watermark->resize(100, 100);
            // and insert a watermark for example
           // $img->insert($watermark,'bottom-right', 1,1);

            // finally we save the image as a new file
            $img->save(base_path('images/'.$name));

        }
        if (!$file = $request->file('img')) {
            $input['img'] = $checker->img;
        }

        $section = Section::find($input['section_id']);

        if ($section->update($input)) {
            session()->flash('success', 'تم تعديل البيانات بنجاح');
            return redirect()->back();
        }

    }

    public function delete_sections(Request $request)

    {

        $section=Section::find($request->section_id);
                @unlink(base_path('images/' . $section->img));

        $section->delete();
        session()->flash('deleted','تم حذف القسم بنجاح');
        return redirect()->back();

    }

}
