<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Markat;
use App\Nabarat;
use App\Setting;
use App\Slider;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('HasPermission:8');
    }

    public function index()

    {
        $settings = Setting::first();
        $sliders = Slider::all();
        $nabarats = Nabarat::all();
        $markats = Markat::all();
        $contact = Contact::first();
        return view('admin.settings', compact('settings', 'markats', 'sliders', 'contact', 'nabarats'));
    }


    /*
     * Settings Section
     */


    public function edit_settings(Request $request)

    {

        $this->validate($request, [

            'facebook' => 'required|url',
            'twitter' => 'required|url',
            'instagram' => 'required|url',
            'site_name' => 'required',
            'site_description' => 'required',

        ],

            [
                'facebook.required' => 'برجاء إدخال رابط الفيسبوك',
                'facebook.url' => 'برجاء إدخال رابط الفيسبوك كاملاً (https://www.example.com)',
                'twitter.unique' => 'برجاء إدخال رابط التويتر ',
                'twitter.url' => 'برجاء إدخال رابط الفيسبوك كاملاً (https://www.example.com)',
                'instagram.required' => 'برجاء إدخال رابط الإنستغرام',
                'instagram.url' => 'برجاء إدخال رابط الفيسبوك كاملاً (https://www.example.com)',
                'site_name.unique' => 'برجاء إدخال إسم الموقع',
                'site_description.required' => 'برجاء إدخال وصف  الموقع',

            ]);

        $input = $request->all();
        $checker = Setting::first();

        if ($file = $request->file('logo')) {
            @unlink(base_path('images/' . $checker->logo));
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(base_path('/images'), $name);
            $input['logo'] = $name;
        }
        if (!$file = $request->file('logo')) {
            $input['logo'] = $checker->logo;
        }

        if ($file = $request->file('icon')) {
            @unlink(base_path('images/' . $checker->icon));
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(base_path('/images'), $name);
            $input['icon'] = $name;
        }
        if (!$file = $request->file('icon')) {
            $input['icon'] = $checker->icon;
        }
        if ($file = $request->file('water_mark')) {
            @unlink(base_path('images/' . $checker->water_mark));
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(base_path('/images'), $name);
            $input['water_mark'] = $name;
        }
        if (!$file = $request->file('water_mark')) {
            $input['water_mark'] = $checker->water_mark;
        }

        if ($checker->update($input)) {
            session()->flash('success', 'تم تعديل البيانات بنجاح');
            return redirect()->back();
        }
        return redirect()->back()->withInput($request->all());

    }

    /*
     * Sliders Section
     */

    public function add_sliders(Request $request)

    {
        $this->validate($request, [

            'img' => 'required',

        ],

            [
                'img.required' => 'برجاء إختيار الصوره',

            ]);
        $input = $request->all();
        $file = $request->file('img');
        $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
        $file->move(base_path('/images'), $name);
        $input['img'] = $name;

        Slider::create($input);
        session()->flash('success', 'تم إضافه السلايدر بنجاح');
        return redirect()->back();

    }


    public function edit_sliders(Request $request)

    {
        $this->validate($request, [

            'img' => 'required',

        ],

            [
                'img.required' => 'برجاء إختيار صوره جديده',

            ]);
        $checker = Slider::find($request->slider_id);

        @unlink(base_path('images/' . $checker->img));
        $file = $request->file('img');
        $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
        $file->move(base_path('/images'), $name);
        $input['img'] = $name;
        $checker->update($input);

        session()->flash('success', 'تم تعديل السلايدر بنجاح');
        return redirect()->back();
    }


    public function delete_sliders(Request $request)

    {
        $checker = Slider::find($request->slider_id);
        @unlink(base_path('images/' . $checker->img));
        $checker->delete();
        session()->flash('deleted', 'تم حذف السلايدر بنجاح');
        return redirect()->back();
    }

    /*
     * Contact Section
     */

    public function edit_contact(Request $request)

    {


        $this->validate($request, [

            'phone' => 'required|numeric',
            'address' => 'required',
            'email' => 'required|email',


        ],

            [
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'phone.numeric' => 'برجاء إدخال رقم الهاتف بصيغه أرقام فقط',
                'address.required' => 'برجاء إدخال العنوان',
                'email.required' => 'برجاء إدخال البريد الالكتروني',
                'email.email' => 'برجاء إدخال البريد الالكتروني كاملاً (Example@example.com)',
            ]);
        if (Contact::first()->update($request->all())) {

            session()->flash('success', 'تم تعديل البيانات بنجاح');
            return redirect()->back();

        }
        return redirect()->back()->withInput($request->all());

    }

    public function edit_banarat(Request $request)

    {
        $row = Nabarat::find($request->id);
        if ($file = $request->file('img1')) {
            @unlink(base_path('images/' . $row->img1));
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(base_path('/images'), $name);
            $input['img1'] = $name;
        }
        $row->update($input);
        session()->flash('success', 'تم تعديل السلايدر بنجاح');
        return redirect()->back();

    }


    public function add_marka(Request $request)

    {
        $this->validate($request, [

            'img' => 'required',

        ],

            [
                'img.required' => 'برجاء إختيار الصوره',

            ]);
        $input = $request->all();
        $file = $request->file('img');
        $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
        $file->move(base_path('/images'), $name);
        $input['img'] = $name;

        Markat::create($input);
        session()->flash('success', 'تم إضافه السلايدر بنجاح');
        return redirect()->back();

    }


    public function edit_marka(Request $request)

    {
        $this->validate($request, [

            'img' => 'required',

        ],

            [
                'img.required' => 'برجاء إختيار صوره جديده',

            ]);
        $checker = Markat::find($request->marka_id);

        @unlink(base_path('images/' . $checker->img));
        $file = $request->file('img');
        $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
        $file->move(base_path('/images'), $name);
        $input['img'] = $name;
        $checker->update($input);

        session()->flash('success', 'تم تعديل السلايدر بنجاح');
        return redirect()->back();
    }


    public function delete_marka(Request $request)

    {
        $checker = Markat::find($request->marka_id);
        @unlink(base_path('images/' . $checker->img));
        $checker->delete();
        session()->flash('deleted', 'تم حذف السلايدر بنجاح');
        return redirect()->back();
    }


}
