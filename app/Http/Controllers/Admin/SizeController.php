<?php

namespace App\Http\Controllers\Admin;

use App\Size;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index($id)

    {
        $sizes = Size::where('user_id', $id)->orderBy('created_at', 'DESC')->paginate(1);
        return view('admin.sizes',compact('sizes'));
    }

}
