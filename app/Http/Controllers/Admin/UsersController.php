<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

public function __construct()
    {
        $this->middleware('HasPermission:5');
    }

    public function index()

    {
        $users = User::where('user_type', 'user')->get();
        return view('admin.users', compact( 'users'));
    }

    public function add_users(Request $request)

    {
        $this->validate($request, [

            'name' => 'required|unique:users,name',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6|max:15',
            'phone' => 'required',

        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
                'password.required' => 'برجاء إدخال كلمه المرور',
                'password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                'password.max' => 'كلمه المرور لا تزيد عن 15 حوف',
                'phone.required' => 'برجاء إدخال رقم الهاتف',

            ]);

        $input = $request->all();
        $input['password']   = bcrypt($request->password);
        $input['user_type'] = 'user';
        if (User::create($input)) {
            {
            session()->flash('success', 'تم إضافه المستخدم بنجاح');
            return redirect()->back();
            }
        }

        return redirect()->back()->withInput($request->all())->exceptInput($request->password);

    }

//    public function edit_users(Request $request)
//    {
//
//        $input = $request->all();
//        $admin = User::find($input['admin_id']);
//
//        if ($request->password == null) {
//            $this->validate($request, [
//
//                'name' => 'required|unique:users,name,' . $admin->id,
//                'email' => 'required|unique:users,email,' . $admin->id,
//                'role_id' => 'required',
//
//            ],
//
//                [
//                    'name.required' => 'برجاء إدخال الإسم',
//                    'name.unique' => 'الإسم مسجل بالفعل',
//                    'email.required' => 'برجاء إدخال البريد الإلكترونى',
//                    'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
//                    'role_id.max' => 'برجاء إختيار صلاحيه واحده على الأقل',
//
//                ]);
//        } else {
//
//            $this->validate($request, [
//
//                'name' => 'required|unique:users,name,' . $admin->id,
//                'email' => 'required|unique:users,email,' . $admin->id,
//                'password' => 'required|min:6|max:15',
//                'role_id' => 'required',
//
//            ],
//
//                [
//                    'name.required' => 'برجاء إدخال الإسم',
//                    'name.unique' => 'الإسم مسجل بالفعل',
//                    'email.required' => 'برجاء إدخال البريد الإلكترونى',
//                    'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
//                    'password.required' => 'برجاء إدخال كلمه المرور',
//                    'password.min' => 'كلمه المرور لا تقل عن 6 حروف',
//                    'password.max' => 'كلمه المرور لا تزيد عن 15 حوف',
//                    'role_id.max' => 'برجاء إختيار صلاحيه واحده على الأقل',
//
//                ]);
//
//        }
//
//        if ($request->password != null) {
//            $input['password'] = bcrypt($request->password);
//        } else {
//            $input['password'] = $admin->password;
//
//        }
//        if ($admin->update($input)) {
//            $admin->role()->sync(Role::find($input['role_id']));
//            session()->flash('success', 'تم تعديل البيانات بنجاح');
//            return redirect()->back();
//        }
//
//    }

    public function delete_users(Request $request)

    {

        $user=User::find($request->user_id);
        $user->delete();
        session()->flash('deleted','تم حذف المستخدم بنجاح');
        return redirect()->back();

    }

    public function updateStatus(Request $request)
    {
        $user = User::find($request->id);
        $user->is_active = $request->status;
        $user->save();
        $check = $user->is_active;
        return response()->json(["status" => "ok", 'check' => $check]);
    }

}
