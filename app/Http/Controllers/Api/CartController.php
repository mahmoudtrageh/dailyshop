<?php

namespace App\Http\Controllers\Api;

use App\Cart;
use App\City;
use App\Country;
use App\Invoice;
use App\InvoiceDetail;
use App\Notifications\NewNotification;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class CartController extends Controller
{
    public function Cart_change(Request $request)
    {
        $v = validator($request->all(), [
            'id' => 'required',
        ],
            [
                'id.required' => 'enter product please',

            ]);
        if ($v->fails()) {
            $error = $v->errors()->first();
            if  ($error=='enter product please')
            $error = 'برجاء إدخال  رقم المنتج';
            else
            $error = 'حدث خطأ';
            $result = [
                'status' =>
                    ['type' => '0', 'title' => ['en'=>$v->errors()->first(),'ar'=>$error]],
            ];
            return response()->json($result);
        }
        if (!Product::where('id', $request->id)->first()) {
            $result = [
                'status' =>
                    ['type' => '0', 'title' =>['ar'=>'المنتج غير موجود','en'=>'product doesn\'t exist']],
            ];
            return response()->json($result);
        }
        $user = User::where('user_type', 'user')->where('api_token', $request->header('token'))->first()->id;
        $product = $request->id;
        $checker = Cart::where('user_id', $user)->where('product_id', $product)->first();
        if ($checker) {
            $checker->delete();
            $result = [
                'status' =>[
                     'type' => '1',
                     'title' => ['ar'=>'تم حذف المنتج من سلة الشراء','en'=>'removed from cart'],
                     'inCart'=>false
                     ]
            ];
            return response()->json($result);
        } else {
            $cart = Cart::create([
                'user_id' => $user,
                'product_id' => $product
            ]);

            if ($cart) {
                $result = [
                    'status' =>
                    [
                    'type' => '1', 'title' => ['ar'=>'تم إضافة المنتج الى سله الشراء','en'=>'added to cart'],
                    'inCart'=>true
                    ]
                ];
                return response()->json($result);
            } else {
                $result = [
                    'status' =>
                        ['type' => '0', 'title' => ['ar'=>'حدث خطأ , برجاء المحاوله لاحقاً','en'=>'error occured']]
                ];
                return response()->json($result);
            }
        }
    }

    public function get_delivery_data()
    {
        $country = Country::with('cities')->get();

        $result = [
            'status' =>
                ['type' => '1'],'data' => $country
        ];
        return response()->json($result);

    }


    public function check_out(Request $request)
    {
        $user = User::where('user_type', 'user')->where('api_token', $request->header('token'))->first();
        if (count($user->cart) == 0) {
            $result = [
                'status' =>
                    ['type' => '0', 'title' => ['ar'=>'لا يوجد منتجات بسلة الشراء','en'=>'nothing found in cart']]
            ];
            return response()->json($result);
        }
        $v = validator($request->all(), [
            'country' => 'required',
            'city' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'bbox' => 'required',
            'quantity' => 'required',
        ],
            [
                'country.required' => 'please enter country',
                'city.required' => 'please choose city',
                'address.required' => 'please choose address',
                'phone.required' => 'please choose phone',
                'bbox.required' => 'please choose postal code',
                'quantity.required' => 'please choose quantity',
            ]);
        if ($v->fails()) {
            $error = $v->errors()->first();
            if  ($error == 'please enter country')
            $error = 'برجاء إختيار الدوله';
            else if($error == 'please choose city')
            $error = 'برجاء إختيار المدينه';
            else if($error == 'please choose address')
            $error = 'برجاء إدخال عنوان الشارع';
            else if($error == 'please choose phone')
            $error = 'برجاء إدخال رقم الهاتف';
            else if($error == 'please choose postal code')
            $error = 'برجاء إدخال الرمز البريدى';
            else if($error == 'please choose qunatity')
            $error = 'برجاء إدخال الكميه';
            else
            $error = 'حدث خطأ';
            
        
            $result = [
                'status' =>
                    ['type' => '0', 'title' =>['en'=> $v->errors()->first(),'ar'=>$error]],
            ];
            return response()->json($result);
        }
        $country = Country::find($request->country);
        $city = City::find($request->city);
        $products = $user->cart;
        $invoice = Invoice::create([
            'name' => $user->name,
            'user_id'=>$user->id,
            'phone' => $request->phone,
            'country' => $country->name,
            'city' => $city->name,
            'address' => $request->address,
            'delivery' => $city->delivery,
            'bbox' => $request->bbox,
        ]);


        if ($invoice) {
            foreach ($products as $i => $product) {
                InvoiceDetail::create([
                    'invoice_id' => $invoice->id,
                    'product_name' => $product->name,
                    'quantity' => $request->quantity[$i],
                    'total' => $product->price_after * $request->quantity[$i]
                ]);
            }
            $invoice->update([
                'grand_total' => array_sum(InvoiceDetail::where('invoice_id', $invoice->id)->pluck('total')->toArray())
            ]);
            foreach (Cart::where('user_id', $user->id)->get() as $cart) {
                $cart->delete();
            }
            $data = ' تم إستلام فاتوره جديده';
            $url = 'message';
            $users = User::where('user_type', 'admin')->get();
            Notification::send($users, new  NewNotification($data, $url));
            $result = [
                'status' =>
                    [
                     'type' => '1',
                     'title' => ['ar'=>'تم الطلب وسيتم التواصل معكم فى أقرب وقت للتأكيد',
                       'en'=>'successfully checked out']
                    ]
            ];
            return response()->json($result);

        } else {
            $result = [
                'status' =>
                    ['type' => '0', 'title' => ['ar'=>'حدث خطأ , برجاء المحاوله مره أخرى','en'=>'error occured']]
            ];
            return response()->json($result);
        }

    }
    
    public function get_cart(Request $request){
        $token = $request->header('token');
        $user = User::where('api_token',$token)->first();
        $carts = $user->cart;
         foreach ($carts as $cart){
            $cart->img = url('images/'.$cart->img);
         }
         
         $totalPriceAfter = strval(array_sum($carts->pluck('price_after')->toArray()));
         $totalPriceBefore = strval(array_sum($carts->pluck('price_before')->toArray()));
         
        $result = [
                'status' =>
                    ['type' => '1'],
                        'data'=>['totalPriceAfter'=>$totalPriceAfter,
                                 'totalPriceBefore'=>$totalPriceBefore,
                                 'cart'=>$carts
                                ]
                        
                    
            ];
            return response()->json($result);
        
    }

}
