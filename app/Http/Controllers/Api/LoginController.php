<?php

namespace App\Http\Controllers\Api;
use Hash;
use App\Mail\ResetPassword;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function registered(Request $request)

    {
        $v = Validator($request->all(), [
            'name' => 'bail|required|unique:users,name',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6|max:15',
            'password_confirmation' => 'required|same:password',
            'phone' => 'required',
        ],
            [
                 'name.required' => 'Please Enter The Name',
                    'name.unique' => 'name already taken',
                    'password.required' => 'Please Enter The Password',
                    'password_confirmation.required' => 'Please Enter The Password Confirmation',
                    'email.required' => 'Please Enter The Email',
                    'email.unique'=>'email is taken',
                    'phone.required' => 'Please Enter The Phone Number',
                    'phone.max' => 'Max Phone Number Is 20 Digits',
                    'password.max' => 'Max Password Is 10 Characters',
                    'password.min' => 'Min Password Is 6 Characters',
                    'password_confirmation.same' => 'Password Confirmation Doesn`\t Match',
            ]);
        if ($v->fails()) {
               $error = $v->errors()->first();
	    if	($error == 'Please Enter The Name')
	    $error = 'من فضلك ادخل الاسم' ;
	   
	    else if($error == 'Please Enter The Password')
	    $error = 'من فضلك ادخل كلمة المرور' ;
	    else if($error == 'Please Enter The Password Confirmation')
	    $error = 'من فضلك ادخل تأكيد كلمة المرور' ;
	    else if($error == 'Please Enter The Email')
	    $error = 'من فضلك ادخل الايميل' ;
	    else if($error == 'Please Enter The Phone Number')
	    $error = 'من فضلك ادخل الهاتف' ;
	    else if($error == 'Max Phone Number Is 20 Digits')
	    $error = 'رقم الهاتف لا يزيد عن 20 رقم' ;
	    else if($error == 'Max Password Is 10 Characters')
	    $error = 'كلمة المرور لا تزيد عن 10 حروف' ;
	    else if($error == 'Min Password Is 6 Characters')
	    $error = 'كلمة المرور ليست اقل من 6 حروف' ;
	    else if($error == 'Password Confirmation Doesn`\t Match')
	    $error = 'كلمة المرور غير متطابقة' ;
	    else if($error == 'email is taken')
	    $error = 'هذا الايميل مستخدم من قبل' ;
	    else if($error == 'name already taken')
	    $error = 'هذا الاسم مستخدم من قبل';
	    else
	    $error = 'تأكد من بياناتك فضلاً' ;
	    
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['en'=>$v->errors()->first(),'ar'=>$error]],

            ];
            return response()->json($Result);
        }
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        $input['api_token'] = str_random(60);
        if ($register = User::create($input)) {
            {
                $user = User::where('id', $register->id)->orderBy('id')->first();
                $user->img = url('images/' . $user->img);
                $Result = [
                    'status' =>
                        ['type' => '1', 'title' => ['ar'=>'تم تسجيل الحساب بنجاح','en'=>'user created successfuly']],
                    'data' => $user
                ];
            }
        } else {
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['en'=>'Error Occurred','ar'=>'حدث خطأ']],
            ];
        }
        return response()->json($Result);
    }

    public function login(Request $request)
    {
        $v = Validator($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'Please Enter The email',
            'password.required' => 'Please Enter The Password',
        ]);
        if ($v->fails()) {
       $error = $v->errors()->first();
	    if	($error == 'Please Enter The email')
	    $error = 'من فضلك ادخل الايميل' ;
	   
	    else if($error == 'Please Enter The Password')
	    $error = 'من فضلك ادخل كلمة المرور' ;
	    else
	    $error = 'حدث خطأ';
	       
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['en'=>$v->errors()->first(),'ar'=>$error]],
            ];
            return response()->json($Result);
        }
        if ($user = User::where('user_type', 'user')->where('email', $request->email)->first()) {
            if (!$user->is_active) {
                $Result = [
                    'status' =>
                        ['type' => '0', 'title' => ['ar'=>'المستخدم ليس فعال بعد', 'en'=>'user isnt active']],
                ];
                return response()->json($Result);
            }
            if(!Hash::check($request->password,$user->password)){
                 $Result = [
                    'status' =>
                        ['type' => '0', 'title' => ['ar'=>'كلمه المرور خطأ ', 'en'=>'wrong password']],
                ];
                return response()->json($Result);
            }
            $user->update(['api_token' => str_random(60)]);
            $user->img = url('images/' . $user->img);
            $Result = [
                'status' =>
                        ['type' => '1', 'title' => ['ar'=>'تم تسجيل الدخول بنجاح','en'=>'signed in successfuly']],
                'data' => $user
            ];
            return response()->json($Result);
        }
        $Result = [
            'status' =>
                ['type' => '0', 'title' => ['en'=>'email or password is incorrect', 'ar'=>'تأكد من كلمة المرور او الايميل']],
        ];
        return response()->json($Result);

    }

    public function check(Request $request)

    {
        $v = Validator($request->all(), [

            'email' => 'required',
        ],

            [
                'email.required' => 'please enter email',
            ]);
        if ($v->fails()) {
            $error = $v->errors()->first(); 
        if ($error == 'please enter email')
	    $error = 'من فضلك ادخل الايميل' ;
	   else
	   $error = 'حدث خطأ';
	    
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['en'=>$v->errors()->first(),'ar'=>$error]],
            ];
            return response()->json($Result);
        }
        if ($user = User::where('user_type', 'user')->where('email', $request->email)->first()) {
            $email = $user->email;
            $code = str_random(5);
            Mail::to($user)->send(new ResetPassword($code));
            if ($user->code) {
                $user->code()->update(['code' => $code, 'updated_at' => Carbon::now()]);
                $Result = [
                    'status' =>
                    ['type' => '1', 'title' => ['en'=>'Code Sent To Your Email Successfully','ar'=>'تم ارسال الكود الى البريد الالكترونى الخاص بك بنجاح']]
                    
                ];
                return response()->json($Result);
            }
            $user->code()->create(['code' => $code, 'updated_at' => Carbon::now()]);
            $Result = [
                'status' =>
                    ['type' => '1', 'title' => ['en'=>'Code Sent To Your Email Successfully','ar'=>'تم ارسال الكود الى البريد الالكترونى الخاص بك بنجاح']]
               
            ];
        } else {
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['ar'=>'البريد الإلكترونى غير مسجل','en'=>'email not found']],
            ];
        }
        return response()->json($Result);

    }

    public function code_confirmation(Request $request)

    {
        $email = ($request->email);
        if ($request->code == null) {
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['ar'=>'برجاء إدخال الكود', 'en'=>'please enter the code']],
            ];
            return response()->json($Result);
        }
        if (!$request->email || !User::where('user_type', 'user')->where('email', $request->email)->first() ||
         !User::where('user_type', 'user')->where('email', $request->email)->first()->code) {
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['ar'=>'حدث خطأ , برجاء المحاولة مرة أخرى', 'en'=>'error occured']],
            ];
            return response()->json($Result);
        } else {
            $user = User::where('user_type', 'user')->where('email', $request->email)->first();
            if ($request->code != $user->code->code) {
                $Result = [
                    'status' =>
                        ['type' => '0', 'title' => ['ar'=>'الكود غير صحيح','en'=>'code isn\'t correct']],
                ];
                return response()->json($Result);
            }

            if ($request->code == $user->code->code) {
                if ($user->code->updated_at->diffInHours() < 2) {
                    $user->code->update(['code' => str_random(5), 'updated_at' => Carbon::now()]);
                    $Result = [
                        'status' =>
                            ['type' => '1', 'title' => ['en'=>'Code Match', 'ar'=>'الكود صحيح']]
                        
                    ];
                    return response()->json($Result);
                }

                $Result = [
                    'status' =>
                        ['type' => '0', 'title' => ['إنتهت صلاحيه الكود , برجاء المحاولة مرة أخرى','en'=>'code expired, request a new one']]
                ];
                return response()->json($Result);

            }


        }
    }

    public function pass_change(Request $request)
    {
        $v = validator($request->all(), [
            'password' => 'required|min:6|max:15',
            'password_confirmation' => 'required|same:password',
            'email' => 'required',
        ],
            [
                'password.required' => 'please enter password',
                'password_confirmation.required' => 'please enter password confirm',
                'password_confirmation.same' => 'Password confirmation isnt the same',
                'password.min' => 'min Password is 6 chars',
                'password.max' => 'Password not more than 15 chars',
                'email.required' => 'enter email',
            ]);
        if ($v->fails()) {
            $error = $v->errors()->first(); 
        if ($error == 'please enter password')
	    $error = 'برجاء إدخال كلمه المرور' ;
	   else if ($error == 'please enter password confirm')
	    $error = 'برجاء إدخال تأكيد كلمه المرور' ;
	   else if($error == 'min Password is 6 chars')
	    $error = 'كلمة المرور الجديدة لا تقل عن 6 حروف' ;
	else if($error == 'Password not more than 15 chars')
	    $error = 'كلمه المرور الجديده لا تزيد عن 15 حروف' ;
	else if($error == 'Password confirmation isnt the same')
	    $error = 'تأكيد كلمه المرور لا تتطابق' ;
	    else if($error == 'enter email')
	    $error = 'برجاء إدخال البريد الإلكترونى' ;
	 else
	     $error = 'حدث خطأ ما';
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['en'=>$v->errors()->first(),'ar'=>$error]],
            ];
            return response()->json($Result);
        }
        if ($user = User::where('user_type', 'user')->where('email', $request->email)->first()) {
            $user->update(['password' => bcrypt($request->password)]);
            $user->update(['api_token' => str_random(60)]);
            $user->img = url('images/' . $user->img);
            $Result = [
                'status' =>
                    ['type' => '1', 'title' => ['en'=>'password changed successfully', 'ar'=>'تم تغيير كلمة المرور بنجاح']],
                'data' => $user
            ];
            return response()->json($Result);
        }
        $Result = [
            'status' =>
                ['type' => '0', 'title' => ['ar'=>'حدث خطأ , برجاء المحاوله ثانيه','en'=>'error occured, plz try again']],
        ];
        return response()->json($Result);
    }
    
     public function update_data(Request $request)
    {
        $token = $request->header('token');
        $checker = User::where('api_token', $token)->first();
        $input = $request->all();
        $v = validator($request->all(), [

            'name' => 'required|unique:users,name,' . $checker->id,
            'email' => 'required|unique:users,email,' . $checker->id,
            'phone' => 'required',
        ],
            [
                'name.required' => 'please enter the name',
                'email.required' => 'please enter email',
                'email.unique' => 'email already taken',
                'phone.required' => 'please enter phone',

            ]);
        if ($v->fails()) {
        $error = $v->errors()->first();
        if($error == 'please enter the name')
        $error='برجاء إدخال الإسم';	
        else if ('please enter email')
        $error = 'برجاء إدخال البريد الإلكترونى';
        else if ('email already taken')
        $error = 'برجاء إدخال البريد الإلكترونى';
        else if ('please enter phone')
        $error = 'برجاء إدخال رقم الموبايل';
        else
        $error = 'حدث خطأ';
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => 'd'],
            ];
            return response()->json($Result);
        }
        
        if ($file = $request->file('img')) {
            @unlink(base_path('images/' . $checker->img));
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(base_path('/images'), $name);
            $input['img'] = $name;

        }
        if (!$file = $request->file('img')) {
            $input['img'] = $checker->img;
        }
                        

        if ($checker->update($input)) {
        $checker->img = url('images/' . $checker->img);
            $Result = [
                'status' =>
                    ['type' => '1', 'title' => ['en'=>'Data Changed Successfully','ar'=>'تم تعديل البيانات بنجاح']],
                'data' => $checker
            ];
            return response()->json($Result);
        }

        $Result = [
            'status' => [
              'type' => '0',
              'title' => [
                'ar'=>'حدث خطأ , برجاء المحاولة مرة أخرى',
                'en'=>'something went wrong, plz try again'
                ]
              ]

        ];
        return response()->json($Result);
    }
     
public function update_password(Request $request)
    {
        $v = validator($request->all(), [

            'old_password' => 'required',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',

        ],

            [
                'old_password.required' => 'Please Enter The old password',
                'new_password.required' => 'Please Enter The new Password',
                'new_password.min' => 'min new Password is 6 chars',
                'new_password.max' => 'new Password not more than 15 chars',
                'new_password_confirmation.required' => 'Please Enter The new Password confirmation',
                'new_password_confirmation.same' => 'new Password confirmation isnt the same',

            ]);
        if ($v->fails()) {
              $error = $v->errors()->first();
	    if	($error == 'Please Enter The old password')
	    $error = 'برجاء إدخال كلمه المرور القديمة' ;
	    else if($error == 'Please Enter The new Password')
	    $error = 'برجاء إدخال كلمه المرور الجديدة' ;
	else if($error == 'min new Password is 6 chars')
	    $error = 'كلمة المرور الجديدة لا تقل عن 6 حروف' ;
	else if($error == 'new Password not more than 15 chars')
	    $error = 'كلمه المرور الجديده لا تزيد عن 15 حروف' ;
	else if($error == 'Please Enter The new Password confirmation')
	    $error = 'برجاء إدخال تأكيد كلمه المرور الجديده' ;
	else if($error == 'new Password confirmation isnt the same')
	    $error = 'تأكيد كلمه المرور الجديده لا تتطابق' ;
	else
	    $error = 'حدث خطأ ما';
            $Result = [
                'status' =>
                    ['type' => '0', 'title' => ['en'=>$v->errors()->first(),'ar'=>$error]],
            ];
            return response()->json($Result);
        }
        $user = User::where('api_token', $request->header('token'))->first();
        $input = $request->all();
        $input['new_password'] = bcrypt($request->new_password);
        if (Hash::check($request->old_password, $user->password)) {
            $user->update(['password' => $input['new_password']]);
            $Result = [
                'status' =>
                    ['type' => '1', 'title' => ['ar'=>'تم تحديث كلمه المرور بنجاح','en'=>'successfully updated']],

            ];
            return response()->json($Result);
        }
        $Result = [
            'status' =>
                ['type' => '1', 'title' => ['ar'=>'كلمه المرور القديمه غير صحيحه','en'=>'old password isn\'t correct']],

        ];
        return response()->json($Result);

    }
}
