<?php

namespace App\Http\Controllers\Api;

use App\About_Main;
use App\Country;
use App\Experiment;
use App\Message;
use App\Partner;
use App\Product;
use App\Slider;
use App\Setting;
use App\Section;
//by MAyman
use App\User;
use App\WishList;
use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function get_sliders()
    {
        $sliders = Slider::all();

        foreach ($sliders as $slider) {
            $slider->img = url('images/' . $slider->img);
        }

        if (count($sliders) == 0) {
            $Result = [
                'status' => ['type' => '0'],
                'data' => $sliders
            ];
            return response()->json($Result);
        }
        $Result = [
            'status' => ['type' => '1'],
            'data' => $sliders
        ];
        return response()->json($Result);
    }

    public function contact_send(Request $request)
    {
        $v = validator($request->all(), [
            'name' => 'required',
            'subject' => 'required',
            'email' => 'required',
            'phone' => 'required|max:20',
            'message' => 'required',
        ],
            [
                'name.required' => 'enter name',
                'subject.required' => 'enter subject',
                'email.required' => 'enter email',
                'phone.required' => 'enter phone',
                'phone.max' => 'max num is 20',
                'message.required' => 'enter message',
            ]);
        if ($v->fails()) {
         $error = $v->errors()->first();
         if($error=='enter name')
         $error='برجاء إدخال الاسم';
        else if($error=='enter email')
         $error='برجاء إدخال البريك الإلكترونى';
        else if($error=='enter subject')
         $error='برجاء إدخال عنوان الرسالة';
        else if($error=='enter phone')
         $error='برجاء إدخال رقم الهاتف';
        else if($error=='max num is 20')
         $error='رقم الهاتف لا يتجاوز 20 رقم';	
         else if($error=='enter message')
         $error='من فضلك ادخل الرسالة';
        else
        $error = 'حدث خطأ';
        
            $result = 
            [
                'status' =>
                    ['type' => '0', 'title' => ['en'=>$v->errors()->first(),'ar'=>$error]]
            ];
           
            return response()->json($result);
        }
        if (Message::create($request->all())) {
            $result = [
                'status' =>
                    [
                    'type' => '1',
                     'title' => ['ar'=>'تم إرسال الرسالة بنجاح','en'=>'succefully sent']
                    ]
            ];
        } else {
            $result = [
                'status' =>
                    [
                    'type' => '0',
                     'title' =>[
                       'ar'=>'حدث خطأ اثناء إرسال الرسالة',
                       'en'=>'error cannot sent'
                       ]
                    ]
            ];
        }
        return response()->json($result);
    }

    public function get_about(){
        //About
        $main = About_Main::first();
        $main->img = url('images/' . $main->img);
        //Settings
        $setting = Setting::first();
                $setting->logo = url('images/' . $setting->logo);
                $setting->icon = url('images/' . $setting->icon);
                $setting->water_mark = url('images/' . $setting->water_mark);

        //Contacts
        $contact = Contact::first();
        $Result = [
            'status' => ['type' => '1'],
            'data' => ['about'=>$main,'settings'=>$setting,'contacts'=>$contact]
        ];
        return response()->json($Result);
    }

    public function get_countries()
    {
        $countries = Country::with('numbers')->get();

        if (count($countries) == 0) {
            $Result = [
                'status' => ['type' => '0'],
                'data' => $countries
            ];
            return response()->json($Result);
        }
        $Result = [
            'status' => ['type' => '1'],
            'data' => $countries
        ];
        return response()->json($Result);
    }

     public function get_sections(Request $request){
      
        $sections = Section::all();
        foreach ($sections as $section){
            $section->img = url('images/'.$section->img);
        }
        $result = [
            'status' =>
                ['type' => '1'],
            'data'=>$sections
        ];
        return response()->json($result);
    }
    
      public function get_products(Request $request){
      $token = $request->header('token');
        $v = validator($request->all(), [
            'type' => 'required',
        ],
            [
                'type.required' => 'برجاء إدخال رقم القسم',
            ]);
        if ($v->fails()) {
            $result = [
                'status' =>
                    ['type' => '0',
                        //'title' => 'please enter a type : [ 1 => special , 2 => new , 3 => discounted ]'],
                                                'title' => 'please enter category_id e.g.:1 or 2 or...'],

            ];
            return response()->json($result);
        }//end if $v fails
        
        /* by: MaymanM >> get products with section_id instead of type
        switch($request->type){
            case 1 :
                $type = 'مميز';
                break;
            case 2 :
                $type = 'جديد';
                break ;
            case 3 :
                $type = 'تخفيض';

        }
        */
        //$type = $request->type
        $user = User::where('api_token',$token)->first();
        $products = Product::where('section_id',$request->type)->where('is_active',1)->with('section')->paginate(5);
        foreach ($products as $product){
            $product->img = url('images/'.$product->img);
            
            if ($token != null) {
                
                
                if(in_array($product->id,$user->cart->pluck('id')->toArray())){
                    $product['in_cart'] = true;
                }
                else{
                    $product['in_cart'] = false;
                }
                

                if ($checker = User::where('api_token', $token)->first()) {
                    $user_wish = WishList::where('user_id', $checker->id)->where('product_id', $product->id)->first();

                    if ($user_wish) {
                        $product->wishlist = true;
                    } else {
                        $product->wishlist = false;
                    }
                } else {

                    $result = [
                        'status' =>
                            ['type' => 'Error', 'title' => 'Invalid Token'],

                    ];

                    return response()->json($result);
                }//end else
             }//end if token check
        }//end for each product
           
        $result = [
            'status' =>
                ['type' => '1'],
            'data'=>$products
        ];
        return response()->json($result);
        
    }
    
    public function get_invoice(Request $request){
        $token = $request->header('token'); 
       $user = User::where('api_token',$token)->first();
       $invoices = $user->invoices;
       foreach($invoices as $invoice){
          $invoice['sumAll'] = strval(floatval($invoice->delivery) + floatval($invoice->grand_total));
          $invoice['details'] =  $invoice->details;
       }
         $result = [
            'status' =>
                ['type' => '1'],
            'data'=>$invoices
        ];
        return response()->json($result);
    }
    
}
