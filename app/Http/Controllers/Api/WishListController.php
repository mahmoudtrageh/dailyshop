<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\User;
use App\WishList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WishListController extends Controller
{
    public function wish_change(Request $request, $id)
    {
        $user = User::where('user_type', 'user')->where('api_token', $request->header('token'))->first()->id;
        $product = $id;
        $checker = WishList::where('user_id', $user)->where('product_id', $product)->first();
        if (Product::find($id)) {
            if ($checker) {
                $checker->delete();
                $Result = [
                    'status' =>
                        [
                            'type' => '1', 
                           'title' => ['ar'=>'تم حذف المفضلة','en'=>'removed'],
                           'isFav'=>false
                        ]
                ];
                return response()->json($Result);
            } else {
                $cart = WishList::create([
                    'user_id' => $user,
                    'product_id' => $product
                ]);
                if ($cart) {
                    $Result = [
                        'status' =>
                        [
                          'type' => '1', 
                          'title' => ['ar'=>'تم إضافة الى المفضلة','en'=>'added'],
                          'isFav'=>true
                        ]
                    ];
                    return response()->json($Result);
                } else {
                    $Result = [
                        'status' =>
                            ['type' => '0', 'title' => ['ar'=>'حدث خطأ , برجاء المحاوله ثانيه ','en'=>'error occured']],
                    ];
                    return response()->json($Result);
                }
            }
        }
        $Result = [
            'status' =>
                ['type' => '0', 'title' => ['ar'=>  'المنتج غير موجود','en'=>'product doesn\'t exist'], 'isFav'=>false],
        ];
        return response()->json($Result);
    }
    
    public function get_wishlist(Request $request)
    {
        $user = User::where('user_type', 'user')->where('api_token', $request->header('token'))->first();
        $wishlsits = $user->wishlist;
        foreach ($wishlsits as $wishlsit){
            $wishlsit->img = url('images/'.$wishlsit->img);
            
             if(in_array($wishlsit->id,$user->cart->pluck('id')->toArray())){
                    $wishlsit['in_cart'] = true;
                }
                else{
                    $wishlsit['in_cart'] = false;
                }
            
         }
        
         $Result = [
                    'status' =>
                        [
                            'type' => '1'
                        ],
                        
                        'data'=>$wishlsits
                ];
                return response()->json($Result);

    }//end method get-wishlist
}
