<?php

namespace App\Http\Controllers\Site;

use App\About_Main;
use App\About_Sub;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()

    {
        $subs = About_Sub::all();

        if (auth()->user()) {

            $carts = auth()->user()->cart;

            return view('site.about', compact( 'carts'));

        }

        return view('site.about', compact( 'subs'));

    }
}
