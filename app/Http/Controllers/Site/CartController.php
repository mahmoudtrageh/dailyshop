<?php

namespace App\Http\Controllers\Site;

use App\Cart;
use App\City;
use App\Coupon;
use App\Country;
use App\Invoice;
use App\InvoiceDetail;
use App\Notifications\NewNotification;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class CartController extends Controller
{
    public function index()

    {
        if (auth()->user()) {
            $carts = Auth::user()->cart;
            $countries = Country::all();

            return view('site.cart', compact('carts', 'countries'));

        }
        $countries = Country::all();
        return view('site.cart', compact('countries'));
    }

    public function Cart_change(Request $request)
    {
        if (auth()->user()) {

        $user = Auth::user()->id;
        $product = $request->id;
        $checker = Cart::where('user_id', $user)->where('product_id', $product)->first();
        if ($checker) {
            $checker->delete();
            return response([
                'data' => [
                    'message' => 'order_deleted'
                ]
            ]);
        } else {
            $cart = Cart::create([
                'user_id' => $user,
                'product_id' => $product
            ]);

            if ($cart) {
                return response([
                    'data' => [
                        'message' => 'order_added'
                    ]
                ]);
            } else {
                return response([
                    'data' => [
                        'message' => 'order_error'
                    ]
                ]);
            }
        }

        } else{
            return response([
                'data' => [
                    'message' => 'user_not_logged_in',
                    'url' => route('site.get.login')
                ]
            ]);
        }


    }

    public function check_coupon(Request $request)
    {
        if (auth()->user()) {

            $coupons = Coupon::all();
            foreach($coupons as $coupon){


                $invoices = Invoice::where('coupon', $coupon->code)->get();

            }

            $coupon = $request->code;
            $checker = Coupon::where('code', $coupon)->first();
            if ($checker && count($invoices) < $checker->limit ) {
                return response([
                    'data' => [
                        'message' => 'coupon_valid'
                    ]
                ]);
            }

            if(count($invoices) >= $checker->limit)
            {
                return response([
                    'data' => [
                        'message' => 'coupon_finish'
                    ]
                ]);
            }

            else {
                return response([
                    'data' => [
                        'message' => 'coupon_invalid'
                    ]
                ]);
            }

        } else{
            return response([
                'data' => [
                    'message' => 'user_not_logged_in',
                    'url' => route('site.get.login')
                ]
            ]);
        }


    }

    public function get_city(Request $request)
    {

        $country = Country::find($request->id);
        $countries = $country->cities;

        return response([
            'data' => [
                'response' => 'countries_found',
                'countries' => $countries
            ]
        ]);

    }

    public function get_delivery(Request $request)
    {
        $city = City::find($request->id);
        $delivery = $city->delivery;
        return response([
            'data' => [
                'response' => 'City_Found',
                'delivery' => $delivery
            ]
        ]);
    }

    public function Cart_Delete(Request $request)
    {
        $user = Auth::user()->id;
        $cart = $request->id;

        if (Auth::guest()) {
            return response([
                'data' => [
                    'message' => 'user_not_logged_in',
                    'url' => route('site.get.login')
                ]
            ]);
        }
           if(Cart::where('user_id', $user)->where('product_id', $cart)->first()->delete()) {
               return response([
                   'data' => [
                       'message' => 'cart_delete',
                   ]
               ]);
           }
    }

    public function check_out(Request $request)
    {

        $the_coupon = $request->coupon;
        $coupon_details = Coupon::where('code', $the_coupon)->get();

     if(count(auth()->user()->cart) == 0){
            return redirect()->back()->withError('لا يوجد منتجات فى سله الشراء');
            }
        $this->validate($request, [
            'country' => 'required',
            'city' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'bbox' => 'required',
        ],
            [
                'country.required' => 'برجاء إختيار الدوله',
                'city.required' => 'برجاء إختيار المدينه',
                'address.required' => 'برجاء إدخال عنوان الشارع',
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'bbox.required' => 'برجاء إدخال الرمز البريدى',
            ]);
           
        $country = Country::find($request->country);
        $city = City::find($request->city);
        $products = auth()->user()->cart;

        $invoice = Invoice::create([
            'user_id'=>auth()->user()->id,
            'name' => auth()->user()->name,
            'phone' => $request->phone,
            'country' => $country->name,
            'city' => $city->name,
            'address' => $request->address,
            'delivery' => $city->delivery,
            'bbox' => $request->bbox,
            'coupon' => $request->coupon,
        ]);

        if ($invoice) {
            foreach ($products as $i => $product) {
                if($request->coupon == null) {

                    InvoiceDetail::create([
                        'invoice_id' => $invoice->id,
                        'product_name' => $product->name,
                        'quantity' => $request->quantity[$i],
                        'total' => $product->price_before * $request->quantity[$i]
                    ]);

                } else {
                    foreach($coupon_details as $coupon_detail) {
                        InvoiceDetail::create([
                            'invoice_id' => $invoice->id,
                            'product_name' => $product->name,
                            'quantity' => $request->quantity[$i],
                            'total' => $product->price_before * $request->quantity[$i] / $coupon_detail->discount
                        ]);
                    }
                }

            }
            $invoice->update([
                'grand_total' => array_sum(InvoiceDetail::where('invoice_id', $invoice->id)->pluck('total')->toArray())

            ]);

            if($request->coupon != null){

            }
            foreach (Cart::where('user_id', auth()->user()->id)->get() as $cart) {
                $cart->delete();
            }
            $data = ' تم إستلام فاتوره جديده';
            $url = 'message';
            $users = User::where('user_type', 'admin')->get();
            Notification::send($users, new  NewNotification($data, $url));

            return redirect()->back()->withSuccess('تم الطلب وسيتم التواصل معكم فى أقرب وقت للتأكيد');

        } else {
            return redirect()->back()->withInput($request->all());
        }

    }

}
