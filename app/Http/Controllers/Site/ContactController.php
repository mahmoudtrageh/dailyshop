<?php

namespace App\Http\Controllers\Site;

use App\Contact;
use App\Message;
use App\Notifications\NewNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;

class ContactController extends Controller
{
    public function index()

    {
        $contacts = Contact::first()->get();

        if (auth()->user()) {

            $carts = auth()->user()->cart;
            return view('site.contact', compact('contacts', 'carts'));

        }
        return view('site.contact', compact('contacts'));
    }

    public function send_contact(Request $request)

    {

        $this->validate($request, [

            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required',

        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.email' => 'برجاء إدخال البريد الإلكترونى بصيغه صحيحه (Example@example.com)',
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'subject.required' => 'برجاء إدخال عنوان الرساله',
                'message.required' => 'برجاء إدخال الرساله',
            ]);

        if ($message = Message::create($request->all())) {
            $data = ' تم إستلام رساله جديده';
            $url = 'message';
            $users = User::where('user_type', 'admin')->get();
            Notification::send($users, new  NewNotification($data, $url));
            session()->flash('success', 'تم إرسال الرساله بنجاح وسيتم التواصل معك فى أسرع وقت');
            return redirect()->back();

        }
        return redirect()->back()->withInput($request->all());

    }
}
