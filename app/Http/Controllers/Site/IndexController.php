<?php

namespace App\Http\Controllers\Site;

use App\Markat;
use App\Nabarat;
use App\Product;
use App\Section;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()

    {

        $sliders = Slider::all();
        $banarat = Nabarat::all();
        $special_Products=Product::where('category','مميز')->where('is_active',1)->get();
        $sections=Section::all();

        $products=Product::all();

        foreach($products as $product)
        {
            $section = $product->section;
        }

        $markats = Markat::all();

        $new_products = Product::where('category', 'جديد')->where('is_active',1)->get();

        $popular_products = Product::where('category', 'شائع')->where('is_active',1)->get();

        if (auth()->user())
        {
            $cart=auth()->user()->cart->pluck('id')->toArray();
        $wish=auth()->user()->wishlist->pluck('id')->toArray();
            $carts = auth()->user()->cart;
        return view('site.index', compact('sliders', 'new_products', 'markats', 'popular_products', 'special_Products','sections','products','cart','wish', 'banarat', 'wish', 'cart','section', 'carts'));
        }
        return view('site.index', compact('sliders', 'new_products', 'markats', 'popular_products', 'special_Products','sections','products', 'banarat', 'section'));

    }
}
