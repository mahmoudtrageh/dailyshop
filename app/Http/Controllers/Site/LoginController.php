<?php

namespace App\Http\Controllers\Site;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function register()

    {
        return view('site.register');
    }

    public function registered(Request $request)

    {

        $this->validate($request, [

            'name' => 'required|unique:users,name',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6|max:15',
            'password_confirmation' => 'required|same:password',
            'phone' => 'required',

        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
                'password.required' => 'برجاء إدخال كلمه المرور',
                'password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور',
                'password_confirmation.same' => 'كلمه المرور وتأكيدها لا تتطابق',
                'password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                'password.max' => 'كلمه المرور لا تزيد عن 15 حوف',
                'phone.required' => 'برجاء إدخال رقم الهاتف',

            ]);

        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        $input['user_type'] = 'user';
        if ($user = User::create($input)) {
            {
                auth()->login($user);
                return redirect()->intended('');
            }
        }

        return redirect()->back()->withInput($request->all())->exceptInput($request->password);

    }


    public function index()
    {
        return view('site.login');
    }

    public function login(Request $request)
    {


        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'برجاء إدخال إسم المتسخدم أو  البريد الإلكتروني',
            'password.required' => 'برجاء إدخال كلمه المرور',
        ]);
        if ($checker = User::where('email', $request->email)->first()) {
        if($checker->is_active == 1){
            if ($checker->user_type == 'user') {
          
                if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

                    return redirect()->intended('');

                }
                }
            }
        }


        session()->flash('login_error', 'برجاء التأكد من إسم المستخدم وكلمه المرور');
        return redirect()->route('site-login')->withInput($request->all())->exceptInput($request->password);
    }

    public function logout()
    {

        Auth::logout();
        return redirect()->route('site-index');

    }

    public function forget()

    {
        return view('site.forget-password');
    }


    public function check(Request $request)

    {

        $this->validate($request, [

            'email' => 'required',
        ],

            [
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
            ]);
        if ($user = User::where('email', $request->email)->where('user_type', 'user')->first()) {
            $email = $user->email;
            $code = str_random(5);
            if ($user->code) {
                $user->code()->update(['code' => $code, 'updated_at' => Carbon::now()]);
                return view('site.code', compact('email'));
            }

            $user->code()->create(['code' => $code, 'updated_at' => Carbon::now()]);
            return view('site.code', compact('email'));
        }

        session()->flash('error', 'البريد الإلكترونى غير مسجل');
        return redirect()->back();

    }

    public function code_confirmation(Request $request)

    {
        $email = ($request->email);

        if ($request->code == null) {
            session()->flash('error', 'برجاء إدخال الكود');
            return view('site.code', compact('email'));
        }

        if (!$request->email || !User::where('email', $request->email)->first() || !User::where('email', $request->email)->first()->code) {

            session()->flash('error', 'حدث خطأ , برجاء المحاوله ثانيه');
            return redirect()->route('site-forget');
        } else {

            $user = User::where('email', $request->email)->first();


            if ($request->code != $user->code->code) {
                session()->flash('error', 'الكود غير صحيح');
                return view('site.code', compact('email'));
            }

            if ($request->code == $user->code->code) {
                if ($user->code->updated_at->diffInHours() < 2) {
                    $user->code->update(['code' => str_random(5), 'updated_at' => Carbon::now()]);
                    return view('site.change-password', compact('email'));
                }
                session()->flash('error', 'إنتهت صلاحيه الكود , برجاء المحاوله ثانيه');
                return redirect()->route('site-forget');
            }


        }


    }

    public function pass_change(Request $request)

    {
        $email = $request->email;
        $v = validator($request->all(), [
            'password' => 'required|min:6|max:15',
            'password_confirmation' => 'required|same:password',
        ],

            [
                'password.required' => 'برجاء إدخال كلمه المرور',
                'password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور',
                'password_confirmation.same' => 'كلمه المرور وتأكيدها لا تتطابق',
                'password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                'password.max' => 'كلمه المرور لا تزيد عن 15 حوف',

            ]);

        if ($v->fails()) {
            return view('site.change-password', compact('email'))->withErrors($v);
        }

        if ($user = User::where('email', $request->email)->first()) {

            $user->update(['password' => bcrypt($request->password)]);
            Auth::login($user);
            return redirect()->intended('');

        }

        session()->flash('error', 'حدث خطأ , برجاء المحاوله ثانيه');
        return redirect()->route('site-forget');
    }


}
