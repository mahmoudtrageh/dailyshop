<?php

namespace App\Http\Controllers\Site;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        if (auth()->user()) {

            $carts = auth()->user()->cart;
            return view('site.profile', compact('carts'));

        }
        return view('site.profile');
    }

    public function update_info(Request $request)
    {
        $checker = User::find(auth()->user()->id);
        $input = $request->all();

        $this->validate($request, [

            'name' => 'required|unique:users,name,' . $checker->id,
            'email' => 'required|unique:users,email,' . $checker->id,

        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => 'البريد الإلكترونى مسجل بالفعل',

            ]);

        if ($file = $request->file('img')) {
            @unlink(base_path('images/' . $checker->img));
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(base_path('/images'), $name);
            $input['img'] = $name;

        }
        if (!$file = $request->file('img')) {
            $input['img'] = $checker->img;
        }

        if ($checker->update($input)) {
            session()->flash('success', 'تم تعديل البيانات بنجاح');
            return redirect()->back();
        }

        return redirect()->back()->withInput($request->all());
    }
    public function edit_password(Request $request)

    {
        $this->validate($request, [

            'old_password' => 'required',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',

        ],

            [
                'old_password.required' => 'برجاء إدخال كلمه المرور القديمه',
                'new_password.unique' => 'برجاء إدخال كلمه المرور الجديده',
                'new_password.min' => ' كلمه المرور الجديده لا تقل عن 6 حروف',
                'new_password.max' => ' كلمه المرور الجديده لا تزيد عن 15 حروف',
                'new_password_confirmation.unique' => 'برجاء إدخال تأكيد كلمه المرور الجديده',
                'new_password_confirmation.same' => 'تأكيد كلمه المرور الجديده لا تتطابق',

            ]);

        $user = User::find(auth()->user()->id);
        $input = $request->all();
        $input['new_password'] = bcrypt($request->new_password);
        if (Hash::check($request->old_password, $user->password)) {
            $user->update(['password' => $input['new_password']]);
            session()->flash('success', 'تم تحديث كلمه المرور بنجاح');
            return redirect()->back();
        }

        session()->flash('error', 'كلمه المرور القديمه غير صحيحه');
        return redirect()->back();

    }
}
