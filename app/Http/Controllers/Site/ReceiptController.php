<?php

namespace App\Http\Controllers\Site;

use App\Invoice;
use App\InvoiceDetail;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ReceiptController extends Controller
{
    public function index ()
    {
        $receipts = Invoice::all();
        if (auth()->user()) {

            $carts = Auth::user()->cart;
            return view('site.receipt', compact('receipts', 'carts'));

        }
        return view('site.receipt', compact('receipts'));
    }
}
