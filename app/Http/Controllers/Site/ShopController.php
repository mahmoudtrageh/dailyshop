<?php

namespace App\Http\Controllers\Site;

use App\Color;
use App\ProducColor;
use App\Product;
use App\Size;
use App\Section;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;


class ShopController extends Controller
{
    public function index(Request $request)
    {

        $all_colors = Color::all();
        if (auth()->user()) {


            $wish = auth()->user()->wishlist->pluck('id')->toArray();
            $cart = auth()->user()->cart->pluck('id')->toArray();
        }

        $min = $request->min_price;
        $max = $request->max_price;

        $id = $request->id;

        if($request->id == null) {
            $all_products = Product::paginate(8);

            foreach($all_products as $all_product)
            {
                $min = $all_product->min('price_before');
                $max = $all_product->max('price_before');

            }

        } else{

            $dir_products = Product::where('section_id', $request->id)->paginate(8);

            foreach($dir_products as $dir_product)
            {
                $min = $dir_product->min('price_before');
                $max = $dir_product->max('price_before');

            }
        }


        $sections = Section::all();

        $new_products = Product::where('category', 'جديد')->where('is_active',1)->get();

        $special_Products=Product::where('category','مميز')->where('is_active',1)->get();

        if (auth()->user()) {

            $carts = auth()->user()->cart;
            return view('site.shop', compact( 'all_products', 'id', 'dir_products', 'all_colors', 'wish', 'cart', 'min', 'max', 'sections', 'new_products', 'special_Products', 'carts'));

        }
        return view('site.shop', compact( 'all_products', 'id', 'dir_products', 'all_colors', 'wish', 'cart', 'min', 'max', 'sections', 'new_products', 'special_Products'));
    }

    public function product_detail($id)
    {
        $product_details = Product::where('id', '=', $id)->get();
        foreach ($product_details as $product_detail) {

            $product_colors = $product_detail->color;

            $related_products = Product::where('section_id', $product_detail->section_id)->get();

        }

        $section = $product_detail->section;

        if (auth()->user()) {


            $wish = auth()->user()->wishlist->pluck('id')->toArray();
            $carts = auth()->user()->cart;
            $cart = auth()->user()->cart->pluck('id')->toArray();
            return view('site.product-detail', compact('product_details', 'cart', 'wish', 'product_colors', 'section', 'related_products', 'carts'));

        }


        return view('site.product-detail', compact('product_details', 'product_colors', 'section', 'related_products'));
    }

    public function filter(Request $request)

    {


        $sec_id = $request->section_id;
        $section_details = Product::all();


        $min = $request->min_price;
        $max = $request->max_price;

        $colors = $request->color_id;

        if($sec_id){
            $section_details = $section_details->where('section_id', $sec_id);
        }

        if ($min) {
            $section_details = $section_details->where('price_before', '>=', $min);
        }

        if ($max) {
            $section_details = $section_details->where('price_before', '<=', $max);
        }

        if ($colors) {
            $colors_products = [] ;
            foreach ($colors as $color){
                $co = Color::find($color);
                array_push($colors_products, $co->product->where('section_id', $sec_id));
            }
            $section_details = $colors_products[0];
        }



        if (auth()->user()) {


            $wish = auth()->user()->wishlist->pluck('id')->toArray();
            $cart = auth()->user()->cart->pluck('id')->toArray();
            $view = view('site.filtered-products', compact('section_details', 'cart', 'wish'))->render();
            return response(['response' => 'success', 'data' => $view]);
        }

        $view = view('site.filtered-products', compact('section_details'))->render();
        return response(['response' => 'success', 'data' => $view]);

    }

    public function get_num(Request $request)
    {

        if($request->num == 'all') {
            $section_details = Product::all();

        } else {
            $section_details = Product::paginate($request->num);
        }

        if (auth()->user()) {

            $wish = auth()->user()->wishlist->pluck('id')->toArray();
            $cart = auth()->user()->cart->pluck('id')->toArray();

            $view = view('site.filtered-products', compact('section_details','wish', 'cart'))->render();
            return response(['response' => 'success', 'data' => $view]);
        }

        $view = view('site.filtered-products', compact('section_details'))->render();
        return response(['response' => 'success', 'data' => $view]);
    }

    public function get_order(Request $request)
    {

        if($request->order == 1) {
            $section_details = Product::all();

            if (auth()->user()) {

                $wish = auth()->user()->wishlist->pluck('id')->toArray();
                $cart = auth()->user()->cart->pluck('id')->toArray();

                $view = view('site.filtered-products', compact('section_details', 'wish', 'cart'))->render();
                return response(['response' => 'success', 'data' => $view]);
            }

            $view = view('site.filtered-products', compact('section_details'))->render();
            return response(['response' => 'success', 'data' => $view]);
        }

        if($request->order == 2) {

            $section_details = Product::orderBy('name', 'DESC')->get();

            if (auth()->user()) {

                $wish = auth()->user()->wishlist->pluck('id')->toArray();
                $cart = auth()->user()->cart->pluck('id')->toArray();

                $view = view('site.filtered-products', compact('section_details', 'wish', 'cart'))->render();
                return response(['response' => 'success', 'data' => $view]);
            }

            $view = view('site.filtered-products', compact('section_details'))->render();
            return response(['response' => 'success', 'data' => $view]);
        }

        if($request->order == 3) {
            $section_details = Product::orderBy('price_before', 'DESC')->get();

            if (auth()->user()) {

                $wish = auth()->user()->wishlist->pluck('id')->toArray();
                $cart = auth()->user()->cart->pluck('id')->toArray();

                $view = view('site.filtered-products', compact('section_details', 'wish', 'cart'))->render();
                return response(['response' => 'success', 'data' => $view]);
            }

            $view = view('site.filtered-products', compact('section_details'))->render();
            return response(['response' => 'success', 'data' => $view]);
        }

        if($request->order == 4) {
            $section_details = Product::orderBy('created_at', 'DESC')->get();

            if (auth()->user()) {

                $wish = auth()->user()->wishlist->pluck('id')->toArray();
                $cart = auth()->user()->cart->pluck('id')->toArray();

                $view = view('site.filtered-products', compact('section_details', 'wish', 'cart'))->render();
                return response(['response' => 'success', 'data' => $view]);
            }

            $view = view('site.filtered-products', compact('section_details'))->render();
            return response(['response' => 'success', 'data' => $view]);
        }


    }

    public function add_size(Request $request)

    {


        $input = $request->all();

        if (Size::create($input)) {
            {
                session()->flash('success', 'تم إرسال المقاسات بنجاح');
                return redirect()->back();
            }
        }

    }
}
