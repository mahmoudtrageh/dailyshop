<?php

namespace App\Http\Controllers\Site;

use App\Cart;
use App\WishList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{

    public function index()

    {
        if (auth()->user()) {

            $carts = auth()->user()->cart;
            $wishs = auth()->user()->wishlist;
            $cart = auth()->user()->cart->pluck('id')->toArray();

            return view('site.wishlist', compact( 'carts', 'wishs', 'cart'));
        }

    }

    public function wish_change(Request $request)
    {

        if (auth()->user()) {
            $user = Auth::user()->id;
        $product = $request->id;
            $checker = WishList::where('user_id', $user)->where('product_id', $product)->first();
        if ($checker) {
            $checker->delete();
            return response([
                'data' => [
                    'message' => 'wish_deleted'
                ]
            ]);
        } else {
            $cart = WishList::create([
                'user_id' => $user,
                'product_id' => $product
            ]);

            if ($cart) {
                return response([
                    'data' => [
                        'message' => 'wish_added'
                    ]
                ]);
            } else {
                return response([
                    'data' => [
                        'message' => 'wish_error'
                    ]
                ]);
            }
        }

        } else {
            return response([
                'data' => [
                    'message' => 'user_not_logged_in',
                    'url' => route('site.get.login')
                ]
            ]);
        }

    }

    public function Cart_add(Request $request)
    {
        if (auth()->user()) {
            $user = Auth::user()->id;
            $product = $request->id;
            $cart = auth()->user()->cart->pluck('id')->toArray();
            if (in_array($product, $cart)) {

                return response([
                    'data' => [
                        'message' => 'order_exist'
                    ]
                ]);
            } else {
                $cart = Cart::create([
                    'user_id' => $user,
                    'product_id' => $product
                ]);
                WishList::where('user_id', $user)->where('product_id', $product)->first()->delete();
                if ($cart) {
                    return response([
                        'data' => [
                            'message' => 'order_added'
                        ]
                    ]);
                } else {
                    return response([
                        'data' => [
                            'message' => 'order_error'
                        ]
                    ]);
                }

            }

        } else{
            return response([
                'data' => [
                    'message' => 'user_not_logged_in',
                    'url' => route('site.get.login')
                ]
            ]);
        }


    }

    public function wish_delete(Request $request)
    {

        $user = Auth::user()->id;
        $product = $request->id;

        if (Auth::guest()) {
            return response([
                'data' => [
                    'message' => 'user_not_logged_in',
                    'url' => route('site.get.login')
                ]
            ]);
        }

        if(WishList::where('user_id', $user)->where('product_id', $product)->first()->delete()) {
            return response([
                'data' => [
                    'message' => 'wishlist_delete',
                ]
            ]);
        }

    }


}
