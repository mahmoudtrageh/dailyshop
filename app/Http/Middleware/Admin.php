<?php

namespace App\Http\Middleware;

use Closure;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = auth()->user()) {
            if ($user->user_type == 'admin') {

                return $next($request);
            }
        }
        session()->flash('error','برجاء التأكد من إسم المستخدم وكلمه المرور');
        return redirect()->route('login');
    }
}
