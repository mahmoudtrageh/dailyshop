<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Api
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('token');
        if (!$token) {
            return response()->json(
                [
                    'status' => [
                        'type' => '0',
                        'title' => 'no token'
                    ]
                ]
            );
        } elseif ($user = User::where('user_type','user')->where('api_token', $token)->first()) {
            if ($user->is_active == 1) {
                return $next($request);
            }
            return response()->json(
                [
                    'status' => [
                        'type' => '0',
                        'title' => 'user not active'
                    ]
                ]
            );
        }
        return response()->json(
            [
                'status' => [
                    'type' => '0',
                    'title' => 'invalid token'
                ]
            ]
        );
    }

}
