<?php

namespace App\Http\Middleware;

use Closure;

class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role_id)
    {
               $user = $request->user();
        if (in_array($role_id, $user->role->pluck('id')->toArray())
            || $user->id == 1) {
            return $next($request);
        }
        return redirect()->route('admin-index')->withErrors('ليس لديك صلاحيات لهذه الصفحه');

    }
}
