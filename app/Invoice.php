<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
     protected  $fillable=[

             'name',
             'phone',
             'country',
             'city',
             'address',
             'delivery',
             'bbox',
             'grand_total',
             'user_id',
                'coupon',
         ];
         
         public function details()
    {
        return $this->hasMany('App\InvoiceDetail');
    }
}
