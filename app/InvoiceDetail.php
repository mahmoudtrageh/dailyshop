<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $fillable = [

        'invoice_id',
        'quantity',
        'product_name',
        'total',
    ];

    public function detail()
    {
        return $this->belongsTo('App\Product','product_id');
    }

}
