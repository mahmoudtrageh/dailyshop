<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Markat extends Model
{
    protected  $fillable = [

        'img',
    ];

    protected $table = 'markat';

}
