<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProducColor extends Model
{
    protected $fillable = [

        'color_id',
        'product_id',

        ];

    protected $table = 'color_product';
}
