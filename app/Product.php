<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'code',
        'price_before',
        'price_after',
        'quantity',
        'section_id',
        'category',
        'description',
        'img',
        'img2',
    ];

    public function color()
    {
        return $this->belongsToMany('App\Color');
    }

    public function section()

    {
        return $this->belongsTo('App\Section');
    }

}
