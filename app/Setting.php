<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
     protected  $fillable=[

             'facebook',
             'twitter',
             'instagram',
             'logo',
             'icon',
             'water_mark',
             'site_name',
             'site_description',

         ];
}
