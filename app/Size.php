<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = [

            'chest_volume',
        'kom_height',
        'center_volume',
        'back_length',
        'highest_volume',
        'chest_to_center',
        'back_width',
        'center_to_leg',
        'shoulder_length',
        'eswra_volume',
        'kom_length',
        'total_length',
        'user_id',

    ];

    protected $table = 'sizes';
}
