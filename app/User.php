<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_type','phone','img','api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()

    {
        return $this->belongsToMany('App\Role');
    }

    public function size()

    {
        return $this->belongsTo('App\Size');
    }

    public function code()

    {
        return $this->hasOne('App\Code');
    }

    public function cart()
    {
        return $this->belongsToMany('App\Product','carts','user_id','product_id');
    }

    public function wishlist()
    {
        return $this->belongsToMany('App\Product','wish_lists','user_id','product_id');
    }
    public function invoices()
    {
        return $this->hasMany('App\Invoice','user_id');
    }
    
    
}
