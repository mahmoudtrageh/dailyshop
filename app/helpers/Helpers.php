<?php
/**
 * Created by PhpStorm.
 * User: iBrahem
 * Date: 9/26/2018
 * Time: 1:32 PM
 */

if (! function_exists('logo')) {
    function settings() {

        $logo=\App\Setting::first();

        return $logo;
    }


}

if (! function_exists('cart_products')) {
    function cart_products() {

        $carts = auth()->user()->cart;

        return $carts;
    }


}


if (! function_exists('postNotifications')) {
    function postNotifications() {

        $notifications = auth()->user()->unreadNotifications;
        return $notifications;
    }


}

if (!function_exists('Has_permission')) {
    function Has_permission($role_id)
    {
        if (in_array($role_id, auth()->user()->role->pluck('id')->toArray())
            ||  auth()->user()->id == 1) {
            return true;
        }
        return false;
    }

}
