<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_sub', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title1');
            $table->string('content1');
            $table->string('title2');
            $table->string('content2');
            $table->string('title3');
            $table->string('content3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about__subs');
    }
}
