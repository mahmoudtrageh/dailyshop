<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->float('chest_volume')->nullable();
            $table->float('kom_height')->nullable();
            $table->float('center_volume')->nullable();
            $table->float('back_length')->nullable();
            $table->float('highest_volume')->nullable();
            $table->float('chest_to_center')->nullable();
            $table->float('back_width')->nullable();
            $table->float('center_to_leg')->nullable();
            $table->float('shoulder_length')->nullable();
            $table->float('eswra_volume')->nullable();
            $table->float('kom_length')->nullable();
            $table->float('total_length')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sizes');
    }
}
