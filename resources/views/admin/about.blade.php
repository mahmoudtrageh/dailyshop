@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">من نحن</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">من نحن
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Button trigger modal -->
            <!-- Zero configuration table -->
             @include('errors.erros')
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><i class="la la-info mr-1"></i> من نحن</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration tab-content">
                                        <thead>
                                        <tr>
                                            <th>العنوان</th>
                                            <th>المحتوي</th>
                                            <th>التاريخ </th>
                                            <th>الصورة</th>
                                            <th>الاجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{$main->title}}</td>
                                            <td>{{$main->content}}</td>
                                            <td>{{$main->date}}</td>
                                            <td><img height="100px" src="{{asset('images/'.$main->img)}}" alt=""></td>
                                            <td>
                                                <button class="btn btn-sm btn-outline-info " data-toggle="modal" data-target="#edit"><i class="fas fa-edit"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
            <!-- Zero configuration table -->
            <section id="about">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <form action="{{route('edit-about-sub')}}" method="POST">
                                {{csrf_field()}}
                                <div class="row my-2">
                                    <div class="col-sm-4">
                                        <div class="card-content border collapse show">
                                            <div class="card-body card-dashboard">
                                                <div class="card text-center">
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <input type="text" name="title1" class="form-control" value="{{old('title1') ? old('title1') : $sub->title1}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="content1" class="form-control" rows="4">{{old('content1') ? old('content1') : $sub->content1}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="card-content border collapse show">
                                            <div class="card-body card-dashboard">
                                                <div class="card text-center">
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <input type="text" name="title2" class="form-control" value="{{old('title2') ? old('title2') : $sub->title2}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea class="form-control" name="content2" rows="4">{{old('content2') ? old('content2') : $sub->content2}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="card-content border collapse show">
                                            <div class="card-body card-dashboard">
                                                <div class="card text-center">
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <input type="text" name="title3" class="form-control" value="{{old('title3') ? old('title3') : $sub->title3}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea class="form-control" name="content3" rows="4">{{old('content3') ? old('content3') : $sub->content3}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-info m-1">تعديل</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
    <!-- edit  -->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">تعديل </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('edit-about-main')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="namee">العنوان</label>
                            <input type="text" name="title" value="{{old('title') ? old('title') : $main->title}}" class="form-control" id="namee"  placeholder="العنوان">
                        </div>
                        <div class="form-group">
                            <label for="infoe">المحتوي</label>
                            <textarea class="form-control" name="content"  rows="8" id="infoe">{{old('content') ? old('content') : $main->content}}</textarea>
                        </div>
                        <div class="form-group">
                            <div class="wrap-custom-file inline-block mb-10">
                                <input type="file"
                                       name="img"
                                       id="image321"
                                       accept=".gif, .jpg, .png"/>
                                <label for="image321"
                                       class="file-ok"
                                       style="background-image: url({{url('images/'.$main->img)}});"
                                >
                                    <span>تعديل الصوره  </span>
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info">حفظ</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">غلق</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    @stop