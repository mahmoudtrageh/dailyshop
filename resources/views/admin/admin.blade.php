@extends('layouts.Admin-Layout')


@section('content')

    <!--الاطباء-->
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">المشرفين
                </h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item active"> المشرفين

                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
        @include('errors.erros')
        <!-- Basic Initialisation table -->
            <section id="initialisation " class="box-shadow">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-user-secret ml-1"></i>المشرفين
                                </h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="col-md-12 col-sml-12">
                                        <div class="form-group">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                                    data-target="#zoomInLeft">
                                                أضف
                                            </button>
                                        </div>
                                        <table class="table table-bordered table-striped ">
                                            <thead>
                                            <tr>
                                                <th>الاسم</th>
                                                <th>البريد الالكتروني</th>
                                                <th>الصلاحيات</th>
                                                <th>الاجراء</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($admins as $admin)
                                                <tr>
                                                    <td>{{$admin->name}}</td>
                                                    <td>{{$admin->email}}</td>
                                                    <td>
                                                        @foreach($admin->role as $access)
                                                            <span class="badge badge-pill badge-success">{{$access->role}}</span>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <button type="button"
                                                                        class="btn btn-outline-danger"
                                                                        data-toggle="modal"
                                                                        data-target="#dele{{$admin->id}}"><i
                                                                            class="fas fa-trash-alt"></i></button>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <button type="button" class="btn btn-outline-info"
                                                                        data-toggle="modal"
                                                                        data-target="#edit{{$admin->id}}"
                                                                        class="btn btn-outline-info mr-1"><i
                                                                            class="fas fa-edit"></i></button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- Modal  edit-->
                                                <div class="modal animated zoomInLeft text-left" id="edit{{$admin->id}}"
                                                     tabindex="-1" role="dialog"
                                                     aria-labelledby="myModalLabel71" style="display: none;"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title text-right">تعديل </h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close"><span
                                                                            aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12 ">
                                                                        <div class="border-1px p-25">

                                                                            <!-- Contact Form -->
                                                                            <form action="{{route('edit-admins',['admin_id'=>$admin->id])}}"
                                                                                  method="POST">
                                                                                {{csrf_field()}}
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <input class="form-control"
                                                                                                   value="{{$admin->name}}"
                                                                                                   name="name"
                                                                                                   type="text"
                                                                                                   placeholder="الاسم">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <input class="form-control"
                                                                                                   type="email"
                                                                                                   name="email"
                                                                                                   value="{{$admin->email}}"
                                                                                                   placeholder="البريد الالكتروني">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="form-group">
                                                                                            <input class="form-control"
                                                                                                   type="password"
                                                                                                   name="password"
                                                                                                   placeholder="كلمة المرور">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    @foreach($roles as $i=>$role)
                                                                                        <div class="col-sm-4">
                                                                                            <div class="form-group">
                                                                                                <div class="d-inline-block custom-control custom-checkbox mr-1 ml-1">
                                                                                                    <input type="checkbox"
                                                                                                           class="custom-control-input"
                                                                                                           name="role_id[]"
                                                                                                           id="check{{$admin->id}}ox25{{$role->id}}"
                                                                                                           value="{{$role->id}}"

                                                                                                           @foreach($admin->role as $rolee)
                                                                                                           @if($rolee->id == $role->id) checked @endif
                                                                                                            @endforeach
                                                                                                    >
                                                                                                    <label class="custom-control-label"
                                                                                                           for="check{{$admin->id}}ox25{{$role->id}}">{{$role->role}}</label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="submit"
                                                                                            class="btn btn-outline-info">
                                                                                        حفظ
                                                                                    </button>
                                                                                    <button type="button"
                                                                                            class="btn grey btn-outline-danger"
                                                                                            data-dismiss="modal">غلق
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- delete Admin -->
                                                <div class="modal animated zoomInLeft text-left" id="dele{{$admin->id}}"
                                                     tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                                     style="display: none;" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title text-right">حذف</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close"><span
                                                                            aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12 ">
                                                                        <div class="border-1px p-25">

                                                                            <!-- Delete Form -->
                                                                            <form action="{{route('delete-admins')}}"
                                                                                  method="get">
                                                                                <input type="hidden" name="admin_id"
                                                                                       value="{{$admin->id}}">
                                                                                {{csrf_field()}}
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <h4> هل أنت متأكد من حذف <span
                                                                                                    style="color: #e11d8e">{{$admin->name}}</span>
                                                                                            ؟</h4>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="submit"
                                                                                            class="btn btn-outline-primary">
                                                                                        نعم
                                                                                    </button>
                                                                                    <button type="button"
                                                                                            class="btn grey btn-outline-danger"
                                                                                            data-dismiss="modal">غلق
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Basic Initialisation table -->
        </div>
    </div>

    <!-- Modal add -->
    <div class="modal animated zoomInLeft text-left" id="zoomInLeft" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-right">أضف</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="border-1px p-25">

                                <!-- Contact Form -->
                                <form action="{{route('add-admins')}}" method="POST">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input class="form-control" value="{{old('name')}}" name="name"
                                                       type="text" placeholder="الاسم">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input class="form-control" type="email" name="email"
                                                       value="{{old('email')}}"
                                                       placeholder="البريد الالكتروني">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input class="form-control" type="password" name="password"
                                                       placeholder="كلمة المرور">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        @foreach($roles as $role)
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="d-inline-block custom-control custom-checkbox mr-1 ml-1">
                                                        <input type="checkbox" class="custom-control-input"
                                                               name="role_id[]" id="checkbox{{$role->id}}25{{$role->id}}"
                                                               value="{{$role->id}}"
                                                               @if(old('role_id'))
                                                               @foreach(old('role_id') as $rolee)
                                                               @if($rolee == $role->id) checked @endif
                                                                @endforeach
                                                                @endif
                                                        >
                                                        <label class="custom-control-label"
                                                               for="checkbox{{$role->id}}25{{$role->id}}">{{$role->role}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-outline-info">حفظ</button>
                                        <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">
                                            غلق
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    </div>
    </div>


@stop