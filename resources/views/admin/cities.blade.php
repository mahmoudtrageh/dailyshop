@extends('layouts.Admin-Layout')


@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title"> الأقسام</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item active"> المدن
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
        @include('errors.erros')
        <!-- Basic Initialisation table -->
            <section id="initialisation " class="box-shadow">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-plus m-1"></i>المدن </h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="form-group">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                                data-target="#zoomInLeft">
                                            أضف مدينة
                                        </button>
                                        <!-- filters-->

                                        <!-- #filters-->
                                    </div>
                                    <table class="table table-striped table-bordered order-column dataex-basic-initialisation">
                                        <thead>
                                        <tr>
                                            <th> اسم الدولة</th>
                                            <th> اسم المدينة</th>
                                            <th> سعر الشحن</th>
                                            <th>الاجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cities as $city)
                                            <tr>
                                                <td>{{$city->country->name}}</td>
                                                <th>{{$city->name}}</th>
                                                <td>{{$city->delivery}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-danger mt-1 btn-sm"
                                                            data-toggle="modal"
                                                            data-target="#dele{{$city->id}}"><i
                                                                class="fas fa-trash-alt"></i></button>
                                                    <button type="button" class="btn btn-outline-info mt-1 btn-sm"
                                                            data-toggle="modal" data-target="#edit{{$city->id}}"
                                                            class="btn btn-outline-info mr-1"><i
                                                                class="fas fa-edit"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <!-- Edit City -->
                                            <div class="modal animated zoomInLeft text-left" id="edit{{$city->id}}"
                                                 tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel71" style="display: none;"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">تعديل المدينة </h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Contact Form -->
                                                                        <form action="{{route('edit-cities')}}" method="POST">
                                                                            {{csrf_field()}}
                                                                            <input type="hidden" name="city_id" value="{{$city->id}}">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-group">
                                                                                        <input name="name" class="form-control" type="text"
                                                                                               placeholder="اسم المدينة" value="{{old('name') ? old('name') : $city->name}}">
                                                                                        <br>
                                                                                        <input name="delivery" class="form-control" value="{{old('delivery') ? old('delivery') : $city->delivery}}"
                                                                                               type="text"
                                                                                               placeholder="سعر الشحن">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                                                                <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">
                                                                                    غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- delete City -->
                                            <div class="modal animated zoomInLeft text-left" id="dele{{$city->id}}"
                                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                                 style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">حذف</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Delete Form -->
                                                                        <form action="{{route('delete-cities')}}"
                                                                              method="get">
                                                                            <input type="hidden" name="city_id"
                                                                                   value="{{$city->id}}">
                                                                            {{csrf_field()}}
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <h4> هل أنت متأكد من حذف <span
                                                                                                style="color: #e11d8e">{{$city->name}}</span>
                                                                                        ؟</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn btn-outline-primary">
                                                                                    نعم
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--/ Basic Initialisation table -->
    </div>
    <!-- Modal add -->
    <div class="modal animated zoomInLeft text-left" id="zoomInLeft" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-right">أضف مدينة </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="border-1px p-25">
                                <form action="{{route('add-cities')}}" method="POST">
                                    {{csrf_field()}}
                                    <input type="hidden" name="country_id" value="{{$country_id}}">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input name="name" class="form-control" type="text"
                                                       placeholder="اسم المدينة" value="{{old('name')}}">
                                                <br>
                                                <input name="delivery" class="form-control" value="{{old('delivery')}}"
                                                       type="text"
                                                       placeholder="سعر الشحن">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                        <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">
                                            غلق
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@stop