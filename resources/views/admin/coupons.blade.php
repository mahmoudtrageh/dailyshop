@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">الكوبونات</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">الكوبونات
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @include('errors.erros')
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><i class="fab fa-product-hunt ml-1"></i> الكوبونات</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="row">
                                    <div class="col-xl-2">
                                        <button type="button" class="btn btn-outline-info mx-2" data-toggle="modal"
                                                data-target="#Add">اضافة كوبون
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration table-responsive">
                                        <thead>
                                        <tr>
                                            <th>الكوبون</th>
                                            <th>الحد الأقصى</th>
                                            <th>نسبة الخصم</th>
                                            <th>عدد المستخدمين</th>
                                            <th>الاجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($coupons as $coupon)
                                            <tr>
                                                <td>{{$coupon->code}}</td>
                                                <td>{{$coupon->limit}}</td>
                                                <td>{{$coupon->discount}}</td>
                                                <td>{{count($invoices)}}</td>
                                                <td>
                                                    <button class="btn btn-sm btn-outline-danger"
                                                            data-toggle="modal"
                                                            data-target="#del{{$coupon->id}}"><i
                                                                class="fas fa-trash-alt"></i></button>
                                                    <button class="btn btn-sm btn-outline-primary my-1"
                                                            data-toggle="modal"
                                                            data-target="#edit{{$coupon->id}}"><i class="fas fa-edit"></i></button>
                                                </td>
                                            </tr>
                                            <!-- Edit products -->
                                            <div class="modal fade text-left" id="edit{{$coupon->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel11"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel11">تعديل</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{route('edit-coupons')}}" method="post" enctype="multipart/form-data">
                                                                <input type="hidden" name="coupon_id" value="{{$coupon->id}}">
                                                                {{csrf_field()}}
                                                                <div class="row">

                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="code">الحد الأقصى</label>
                                                                            <input type="text" name="limit" value="{{$coupon->limit}}" class="form-control"
                                                                                   id="code" placeholder="الحد الأقصى">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="pricebefore">نسبة الخصم</label>
                                                                            <input type="text" name="discount" value="{{$coupon->discount}}"
                                                                                   class="form-control"
                                                                                   placeholder="نسبة الخصم">
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">غلق
                                                                    </button>
                                                                    <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- delete products -->
                                            <div class="modal animated zoomInLeft text-left" id="del{{$coupon->id}}"
                                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                                 style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">حذف</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Delete Form -->
                                                                        <form action="{{route('delete-coupons')}}"
                                                                              method="get">
                                                                            <input type="hidden" name="product_id"
                                                                                   value="{{$coupon->id}}">
                                                                            {{csrf_field()}}
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <h4> هل أنت متأكد من حذف <span
                                                                                                style="color: #e11d8e">{{$coupon->code}}</span>
                                                                                        ؟</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn btn-outline-primary">
                                                                                    نعم
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>
    <!-- add products -->
    <div class="modal fade text-left" id="Add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">اضافة</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('add-coupons')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="code">الحد الأقصى</label>
                                    <input type="text" name="limit" value="{{old('limit')}}" class="form-control"
                                           id="code" placeholder="الحد الأقصى">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pricebefore">نسبة الخصم</label>
                                    <input type="text" name="discount" value="{{old('discount')}}"
                                           class="form-control"
                                           placeholder="نسبة الخصم">
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">غلق
                            </button>
                            <button type="submit" class="btn btn-outline-primary">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')

@stop