@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">بروفايل المستخدم</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item active">بروفايل<
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
             @include('errors.erros')
            <!-- Row separator layout section start -->
            <section id="row-separator-form-layouts">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="row-separator-icons">تعديل كلمة المرور  </h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form action="{{route('edit-password')}}" method="POST" class="form form-horizontal row-separator">
                                        {{csrf_field()}}
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="user">كلمة المرور القديمة</label>
                                                <div class="col-md-9">
                                                    <div class="position-relative has-icon-right">
                                                        <input type="password" name="old_password" id="user" class="form-control" placeholder="كلمة المرور القديمة">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="user-profile">كلمة المرور الجديدة </label>
                                                <div class="col-md-9">
                                                    <div class="position-relative has-icon-right">
                                                        <input type="password" name="new_password" id="user-profile" class="form-control" placeholder="كلمة المرور الجديدة">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="email">تأكيد كلمة المرور الجديدة</label>
                                                <div class="col-md-9">
                                                    <div class="position-relative has-icon-right">
                                                        <input type="password" name="new_password_confirmation" id="email" class="form-control" placeholder="تأكيد كلمة المرور الجديدة ">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button type="submit" class="btn btn-primary"><i class="la la-check"></i>تعديل</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // Row separator layout section end -->
        </div>
    </div>

    @stop