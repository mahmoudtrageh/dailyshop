@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row my-2">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">تفاصيل الفاتورة</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">الفاتورة
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="card">
                <div id="invoice-template" class="card-body">
                    <!-- Invoice Company Details -->
                    <div id="invoice-company-details" class="row">
                        <div class="col-md-6 col-sm-12 text-center text-md-left">
                            <div class="media">
                                <img src="{{ asset('images/'.$signature->logo) }}" alt="company logo" class="w-50"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 text-center text-md-right">
                            <h2>فاتورة</h2>
                            <p class="pb-3"># رقم {{$invoice->id}}</p>
                            <ul class="px-0 list-unstyled">
                                <li>التكلفة </li>
                                <li class="lead text-bold-800"> {{$invoice->grand_total}}</li>
                            </ul>
                        </div>
                    </div>
                    <!--/ Invoice Company Details -->
                    <!-- Invoice Customer Details -->
                    <div id="invoice-customer-details" class="row pt-2">
                        <div class="col-sm-12 text-center text-md-left">
                            <p class="text-muted">فاتورة الى</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-center text-md-left">
                            <ul class="px-0 list-unstyled">
                                <li class="text-bold-800">{{$invoice->name}}</li>
                                <li>{{$invoice->country}}</li>
                                <li>{{$invoice->city}}</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-12 text-center text-md-right">
                            <p>
                                <span class="text-muted">تاريخ الفاتورة :</span> {{$invoice->created_at->toDateString()}}</p>
                            <p>
                                <span class="text-muted">الشروط :</span> مستحق عند الاستلام</p>
                        </div>
                    </div>
                    <!--/ Invoice Customer Details -->
                    <!-- Invoice Items Details -->
                    <div id="invoice-items-details" class="pt-2">
                        <div class="row">
                            <div class="table-responsive col-sm-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم المنتج</th>
                                        <th class="text-right">التكليفة</th>
                                        <th class="text-right">الكمية</th>
                                        <th class="text-right">الاجمالي</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($invoices_details as $i=>$invoice)
                                    <tr>
                                        <th scope="row">{{$i+1}}</th>
                                        <td>
                                            <p>{{$invoice->product_name }}</p>
                                        </td>
                                        <td class="text-right">{{$invoice->quantity}}</td>
                                        <td class="text-right">{{$invoice->quantity}}</td>
                                        <td class="text-right">$ {{$invoice->total}}</td>
                                    </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-md-6 col-sm-12">
                                <p class="lead">الاجمالي </p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td>المجموع الفرعى</td>
                                            <td class="text-right">$ {{$sub}}</td>
                                        </tr>
                                        <tr>
                                            <td>الشحن</td>
                                            <td class="text-right">$ {{$delivery->delivery}}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold-800">الاجمالي</td>
                                            <td class="text-bold-800 text-right"> $ {{$delivery->delivery + $sub}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center">
                                    <img src="{{ asset('images/'.$signature->logo) }}" alt="signature" class="height-100"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    @stop