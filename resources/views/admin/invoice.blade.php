@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">الفواتير</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">فواتير
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            @include('errors.erros')
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><i class="fas fa-money-bill-wave ml-1"></i> فواتير</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration table-responsive">
                                        <thead>
                                        <tr>
                                            <th>الاسم</th>
                                            <th>رقم الجوال</th>
                                            <th>الدولة</th>
                                            <th>المدينة</th>
                                            <th>العنوان</th>
                                            <th>الشحن</th>
                                            <th>الاجمالي</th>
                                            <th>التفاصيل</th>
                                            <th>الاجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($invoices as $invoice)
                                            <tr>
                                                <td>{{$invoice->name}}</td>
                                                <td>{{$invoice->phone}}</td>
                                                <td>{{$invoice->country}}</td>
                                                <td>{{$invoice->city}}</td>
                                                <td>{{$invoice->address}}</td>
                                                <td>{{$invoice->delivery}}</td>
                                                <td>{{$invoice->grand_total}}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-outline-danger"
                                                       href="{{route('Invoice-Details',['id'=>$invoice->id])}}"><i
                                                                class="fas fa-info"></i></a>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-outline-danger"
                                                            data-toggle="modal"
                                                            data-target="#dele{{$invoice->id}}"><i
                                                                class="fas fa-trash-alt"></i></button>
                                                </td>
                                            </tr>
                                            <!-- delete Invoice -->
                                            <div class="modal animated zoomInLeft text-left" id="dele{{$invoice->id}}"
                                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                                 style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">حذف</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Delete Form -->
                                                                        <form action="{{route('Invoice-Delete')}}"
                                                                              method="post">
                                                                            <input type="hidden" name="invoice_id"
                                                                                   value="{{$invoice->id}}">
                                                                            {{csrf_field()}}
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <h4> هل أنت متأكد من حذف فاتوره<span
                                                                                                style="color: #e11d8e">{{$invoice->name}}</span>
                                                                                        ؟</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn btn-outline-primary">
                                                                                    نعم
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>
@stop