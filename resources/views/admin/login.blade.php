<!DOCTYPE html>
<html lang="en">

<head>
    @include('assets.Admin-CSS')
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">

        </div>

        <div class="clearfix"></div>
    </header>

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row" style="margin-top: 200px">
                            <div class="col-sm-3 col-xs-3">
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="mb-30">
                                    <h3 class="text-center txt-dark mb-10">تسجيل الدخول tohfa</h3>
                                    <h6 class="text-center nonecase-font txt-grey">أدخل البيانات </h6>
                                </div>
                                <div class="form-wrap">
                                    <form  action="{{route('logincheck')}}" method="POST">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_2"> اسم المستخدم
                                                او البريد الإلكتروني</label>
                                            <input type="text" value="{{old('email')}}" name="email"
                                                   class="form-control" id="exampleInputEmail_2" placeholder=" ">
                                        </div>
                                        <div class="form-group">
                                            <label class="pull-right control-label mb-10" for="exampleInputpwd_2">كلمة
                                                المرور</label>

                                            <div class="clearfix"></div>
                                            <input type="password" name="password" class="form-control"
                                                   id="exampleInputpwd_2" placeholder=" ">
                                        </div>


                                        <div class="form-group text-center">
                                            <button  type="submit" class="btn btn-primary  btn-rounded"> دخول</button>
                                        </div>
                                    </form>
                                      @include('errors.erros')
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

@include('assets.Admin-JS')


<body/>

<html/>
