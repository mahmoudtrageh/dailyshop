@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">الرسائل</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">الرسائل
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body" style="width: 970px">
             @include('errors.erros')
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><i class="la la-envelope ml-1"></i> الرسائل</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div><br>


                                <button class="btn btn-outline-info mx-2"
                                        data-toggle="modal"
                                        data-target="#dele"
                                style="float: left">مسح الكل</button>

                                <!-- delete Invoice -->
                                <div class="modal animated zoomInLeft text-left" id="dele"
                                     tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                     style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title text-right">حذف</h4>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span
                                                            aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <div class="border-1px p-25">

                                                            <!-- Delete Form -->
                                                            <form action="{{route('delete-all-message')}}"
                                                                  method="post">
                                                                {{csrf_field()}}

                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <h4> هل أنت متأكد من حذف الرسائل
                                                                            ؟</h4>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit"
                                                                            class="btn btn-outline-primary">
                                                                        نعم
                                                                    </button>
                                                                    <button type="submit"
                                                                            class="btn grey btn-outline-danger"
                                                                            data-dismiss="modal">غلق
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered tab-content " >
                                        <thead>
                                        <tr>
                                            <th>الاسم </th>
                                            <th>البريد الالكتروني</th>
                                            <th>رقم الجوال</th>
                                            <th>موضوع الرسالة</th>
                                            <th>محتوي الرسالة</th>
                                            <th>الاجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($messages as $message)
                                        <tr>
                                            <td>{{$message->name}}</td>
                                            <td>{{$message->email}}</td>
                                            <td>{{$message->phone}}</td>
                                            <td>
                                                <!-- Button trigger modal -->
                                                <button  style="background-color: #1e9ff2 !important;border-color:#0f70af !important"  type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                                   عرض
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">موضوع الرسالة	</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {{$message->subject}}
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button   style="background-color: #1e9ff2 !important;border-color:#0f70af !important"  type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <!-- Button trigger modal -->
                                                <button style="background-color: #1e9ff2 !important;border-color:#0f70af !important" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                                    عرض
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">محتوي الرسالة	</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {{$message->message}}
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button  style="background-color: #1e9ff2 !important;border-color:#0f70af !important"  type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>




                                            <td>
                                                <button class="btn btn-sm btn-outline-danger"
                                                        data-toggle="modal"
                                                        data-target="#dele{{$message->id}}"><i
                                                            class="fas fa-trash-alt"></i></button>                                            </td>
                                        </tr>
                                        <!-- delete Message -->
                                        <div class="modal animated zoomInLeft text-left" id="dele{{$message->id}}"
                                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                             style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title text-right">حذف</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="border-1px p-25">
                                                                    <!-- Delete Form -->
                                                                    <form action="{{route('delete-message',['id'=>$message->id])}}"
                                                                          method="get">
                                                                        {{csrf_field()}}
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <h4> هل أنت متأكد من حذف <span
                                                                                            style="color: #e11d8e">{{$message->subject}}</span>
                                                                                    ؟</h4>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit"
                                                                                    class="btn btn-outline-primary">
                                                                                نعم
                                                                            </button>
                                                                            <button type="button"
                                                                                    class="btn grey btn-outline-danger"
                                                                                    data-dismiss="modal">غلق
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>
    @stop