@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">المنتجات</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">المنتجات
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @include('errors.erros')
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><i class="fab fa-product-hunt ml-1"></i> المنتجات</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="row">
                                    <div class="col-xl-2">
                                        <button type="button" class="btn btn-outline-info mx-2" data-toggle="modal"
                                                data-target="#Add">اضافة منتج
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration table-responsive">
                                        <thead>
                                        <tr>
                                            <th>اسم المنتج</th>
                                            <th>الكود</th>
                                            <th>الفئة</th>
                                            <th>السعر قبل الخصم</th>
                                            <th>السعر بعد الخصم</th>
                                            <th>اللون</th>
                                            <th>الصورة</th>
                                            <th>الصورة</th>
                                            <th>الوصف</th>
                                            <th>الكمية</th>
                                            <th class="sel">الحالة</th>
                                            <th>الاجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)
                                            <tr>
                                                <td>{{$product->name}}</td>
                                                <td>{{$product->code}}</td>
                                                <td>{{$product->category}}</td>
                                                <td>{{$product->price_before}}</td>
                                                <td>{{$product->price_after}}</td>
                                                <td>
                                                    @foreach($product->color as $value)
                                                        <span class="badge badge-pill badge-info">{{$value->name}}</span>
                                                        @endforeach
                                                </td>
                                                <td><img height="100px" src="{{asset('images/'.$product->img) }}"></td>
                                                <td><img height="100px" src="{{asset('images/'.$product->img2) }}"></td>
                                                <td>
                                                    <button class="btn btn-sm btn-outline-primary my-1"
                                                            data-toggle="modal"
                                                            data-target="#wsf{{$product->id}}">عرض
                                                    </button>
                                                </td>

                                                <td>{{$product->quantity}}</td>
                                                <td>
                                                    <div class="form-group sel">
                                                        <select class="form-control stutus"  pid="{{ $product->id }}">
                                                            <option @if($product->is_active) selected
                                                                    @endif value="1">
                                                                نشط
                                                            </option>
                                                            <option @if(!$product->is_active) selected
                                                                    @endif value="0">
                                                                معلق
                                                            </option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-outline-danger"
                                                            data-toggle="modal"
                                                            data-target="#del{{$product->id}}"><i
                                                                class="fas fa-trash-alt"></i></button>
                                                    <button class="btn btn-sm btn-outline-primary my-1"
                                                            data-toggle="modal"
                                                            data-target="#edit{{$product->id}}"><i class="fas fa-edit"></i></button>
                                                </td>
                                            </tr>
                                            <!-- Edit products -->
                                            <div class="modal fade text-left" id="edit{{$product->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel11"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel11">تعديل</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{route('edit-products')}}" method="post" enctype="multipart/form-data">
                                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                                {{csrf_field()}}
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="name">اسم المنتج</label>
                                                                            <input type="text" name="name" value="{{$product->name}}" class="form-control"
                                                                                   id="name" placeholder="اسم المنتج">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="code">كود المنتج</label>
                                                                            <input type="text" name="code" value="{{$product->code}}" class="form-control"
                                                                                   id="code" placeholder="كود المنتج">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="pricebefore">السعر قبل الخصم</label>
                                                                            <input type="text" name="price_before" value="{{$product->price_before}}"
                                                                                   class="form-control" id="pricebefore"
                                                                                   placeholder="السعر قبل الخصم">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="priceafter">السعر بعد الخصم</label>
                                                                            <input type="text" name="price_after" value="{{$product->price_after}}"
                                                                                   class="form-control" id="priceafter"
                                                                                   placeholder="السعر بعد الخصم">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="quantity">الكمية</label>
                                                                            <input type="text" name="quantity" value="{{$product->quantity}}" class="form-control"
                                                                                   id="quantity" placeholder="الكمية">
                                                                        </div>
                                                                    </div>


                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="department" style="margin-right:20px">الاقسام</label>
                                                                                <select name="section_id" class="form-control" id="department" style="margin-right:20px">
                                                                                    <option disabled="" selected>إختر القسم</option>
                                                                                    @foreach($sections as $section)
                                                                                        <option @if($product->section_id == $section->id) selected
                                                                                                @endif value="{{$section->id}}">{{$section->name}}</option>
                                                                                    @endforeach

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="category">الفئة</label>
                                                                                <select name="category" class="form-control" id="category">
                                                                                    <option disabled="" selected>إختر الفئة</option>
                                                                                    <option @if($product->category == 'مميز') selected @endif>مميز</option>
                                                                                    <option @if($product->category == 'جديد') selected @endif>جديد</option>
                                                                                    <option @if($product->category == 'شائع') selected @endif>تخفيض</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="wrap-custom-file inline-block mb-10">
                                                                            <input type="file"
                                                                                   name="img"
                                                                                   id="imagesd2321{{$product->id}}"
                                                                                   accept=".gif, .jpg, .png"/>
                                                                            <label for="imagesd2321{{$product->id}}"
                                                                                    class="file-ok"
                                                                                    style="background-image: url({{url('images/'.$product->img)}});"
                                                                            >
                                                                                <span>تعديل الصورة </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="wrap-custom-file inline-block mb-10">
                                                                            <input type="file"
                                                                                   name="img2"
                                                                                   id="imagesd232{{$product->id}}"
                                                                                   accept=".gif, .jpg, .png"/>
                                                                            <label for="imagesd232{{$product->id}}"
                                                                                   class="file-ok"
                                                                                   style="background-image: url({{url('images/'.$product->img2)}});"
                                                                            >
                                                                                <span>تعديل الصورة </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>


                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <label for="quantity" style="margin-right: 10px;">اللون</label>
                                                                        </div>
                                                                        @foreach($colors as $color)
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <div class="d-inline-block custom-control custom-checkbox mr-1 ml-1">
                                                                                        <input type="checkbox"
                                                                                               class="custom-control-input"
                                                                                               name="color_id[]"
                                                                                               id="check{{$color->id}}ox25{{$product->id}}"
                                                                                               value="{{$color->id}}"
                                                                                               @foreach($product->color as $item)
                                                                                               @if($item->id == $color->id) checked @endif
                                                                                                @endforeach
                                                                                        >
                                                                                        <label class="custom-control-label"
                                                                                               for="check{{$color->id}}ox25{{$product->id}}">{{$color->name}}</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="discrpation">الوصف</label>
                                                                            <textarea class="form-control" name="description" placeholder="الوصف"
                                                                                      id="discrpation"
                                                                                      rows="5">{{$product->description}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">غلق
                                                                    </button>
                                                                    <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  wasf products -->
                                            <div class="modal fade text-left" id="wsf{{$product->id}}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="myModalLabel11"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel11">الوصف</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form>
                                                                <p>{{$product->description}}</p>

                                                                <div class="modal-footer">
                                                                    <button type="button"
                                                                            class="btn grey btn-outline-secondary"
                                                                            data-dismiss="modal">غلق
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- ////////////////////////////////////////////////////////////////////////////-->
                                            <!-- delete products -->
                                            <div class="modal animated zoomInLeft text-left" id="del{{$product->id}}"
                                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                                 style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">حذف</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Delete Form -->
                                                                        <form action="{{route('delete-products')}}"
                                                                              method="get">
                                                                            <input type="hidden" name="product_id"
                                                                                   value="{{$product->id}}">
                                                                            {{csrf_field()}}
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <h4> هل أنت متأكد من حذف <span
                                                                                                style="color: #e11d8e">{{$product->name}}</span>
                                                                                        ؟</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn btn-outline-primary">
                                                                                    نعم
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>
    <!-- add products -->
    <div class="modal fade text-left" id="Add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">اضافة</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('add-products')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name">اسم المنتج</label>
                                    <input type="text" name="name" value="{{old('name')}}" class="form-control"
                                           id="name" placeholder="اسم المنتج">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="code">كود المنتج</label>
                                    <input type="text" name="code" value="{{old('code')}}" class="form-control"
                                           id="code" placeholder="كود المنتج">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pricebefore">السعر قبل الخصم</label>
                                    <input type="text" name="price_before" value="{{old('price_before')}}"
                                           class="form-control" id="pricebefore"
                                           placeholder="السعر قبل الخصم">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="priceafter">السعر بعد الخصم</label>
                                    <input type="text" name="price_after" value="{{old('price_after')}}"
                                           class="form-control" id="priceafter"
                                           placeholder="السعر بعد الخصم">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="quantity">الكمية</label>
                                    <input type="text" name="quantity" value="{{old('quantity')}}" class="form-control"
                                           id="quantity" placeholder="الكمية">
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="department" style="margin-right:20px">الاقسام</label>
                                        <select name="section_id" class="form-control" id="department" style="margin-right:20px">
                                            <option disabled="" selected>إختر القسم</option>
                                            @foreach($sections as $section)
                                                <option @if(old('section_id') == $section->id) selected
                                                        @endif value="{{$section->id}}">{{$section->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="category">الفئة</label>
                                        <select name="category" class="form-control" id="category">
                                            <option disabled="" selected>إختر الفئة</option>
                                            <option @if(old('category') == 'مميز') selected @endif>مميز</option>
                                            <option @if(old('category') == 'جديد') selected @endif>جديد</option>
                                            <option @if(old('category') == 'تخفيض') selected @endif>تخفيض</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="wrap-custom-file inline-block mb-10">
                                    <input type="file"
                                           name="img"
                                           id="image321"
                                           accept=".gif, .jpg, .png"/>
                                    <label for="image321"
                                            {{--class="file-ok"--}}
                                            {{--style="background-image: url({{url('images/'.$data->contactphoto)}});"--}}
                                    >
                                        <span>الصورة </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="wrap-custom-file inline-block mb-10">
                                    <input type="file"
                                           name="img2"
                                           id="image32"
                                           accept=".gif, .jpg, .png"/>
                                    <label for="image32"
                                            {{--class="file-ok"--}}
                                            {{--style="background-image: url({{url('images/'.$data->contactphoto)}});"--}}
                                    >
                                        <span>الصورة 2 </span>
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="quantity" style="margin-right: 10px;">اللون</label>
                                </div>
                                @foreach($colors as $color)
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="d-inline-block custom-control custom-checkbox mr-1 ml-1">
                                                <input type="checkbox"
                                                       class="custom-control-input"
                                                       name="color_id[]"
                                                       id="check{{$color->id}}ox25"
                                                       value="{{$color->id}}"
                                                       @if(old('color_id'))
                                                       @foreach(old('color_id') as $item)
                                                       @if($item == $color->id) checked @endif
                                                        @endforeach
                                                        @endif
                                                >
                                                <label class="custom-control-label"
                                                       for="check{{$color->id}}ox25">{{$color->name}}</label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="discrpation">الوصف</label>
                                    <textarea class="form-control" name="description" placeholder="الوصف"
                                              id="discrpation"
                                              rows="5">{{old('description')}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">غلق
                            </button>
                            <button type="submit" class="btn btn-outline-primary">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')


    <script>
        $(document).on("change", ".stutus", function () {
            var status = $(this).val();
            var id = $(this).attr("pid");
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ url('admin/update-status') }}",
                type: "post",
                dataType: "json",
                data: {status: status, id: id, _token: token},
                success: function (data) {
                    // console.log(data.check);
                    if (data.status !== "ok") {
                        alert("ERROR");
                    }

                },
                error: function () {
                    alert("ERROR");
                }
            })
        })
    </script>

    @stop