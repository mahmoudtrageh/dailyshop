@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">بروفايل المستخدم</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item active">بروفايل
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
         @include('errors.erros')
        <div class="content-body">
            <!-- Row separator layout section start -->
            <section id="row-separator-form-layouts">
                <div class="row">
                    <div class="col-md-3">
                        <form action="{{route('edit-profile')}}" method="POST" enctype="multipart/form-data"
                              class="form form-horizontal row-separator">
                            {{csrf_field()}}
                            <div class="card">
                                <div class="card-content text-center">
                                    <div class="wrap-custom-file inline-block mb-10">
                                        <input type="file"
                                               name="img"
                                               id="image321"
                                               accept=".gif, .jpg, .png"/>
                                        <label for="image321"
                                               class="file-ok"
                                               style="background-image: url({{url('images/'.auth()->user()->img)}});"
                                        >
                                            <span>تعديل</span>
                                        </label>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title"><i class="la la-user"></i> {{auth()->user()->name}}</h4>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="row-separator-icons">بيانات المستخدم</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">

                                    <div class="form-body">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="user-profile">اسم
                                                المستخدم</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" name="name"
                                                           value="{{old('name') ?old('name') : auth()->user()->name}}"
                                                           id="user-profile" class="form-control" placeholder="الاسم">
                                                    <div class="form-control-position">
                                                        <i class="la la-user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="email">البريد الالكتروني</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" name="email"
                                                           value="{{old('email') ?old('email') : auth()->user()->email}}"
                                                           id="email" class="form-control"
                                                           placeholder="البريد الالكتروني ">
                                                    <div class="form-control-position">
                                                        <i class="la la-envelope-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button type="submit" class="btn btn-primary"><i class="la la-check"></i>تعديل
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </section>
            <!-- // Row separator layout section end -->
        </div>
    </div>
@stop