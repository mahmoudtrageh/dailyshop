@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title"> الأقسام</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item active"> الأقسام
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @include('errors.erros')
        <div class="content-body">
            <!-- Basic Initialisation table -->
            <section id="initialisation " class="box-shadow">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-plus m-1"></i>الأقسام </h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="form-group">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                                data-target="#zoomInLeft">
                                            أضف قسم
                                        </button>
                                        <p class="text-left my-1">المقاس </p>

                                        <!-- filters-->

                                        <!-- #filters-->
                                    </div>
                                    <table class="table table-striped table-bordered order-column dataex-basic-initialisation">
                                        <thead>
                                        <tr>
                                            <th> اسم القسم</th>
                                                                                        <th>الصورة</th>

                                            <th>الاجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($sections as $section)
                                            <tr>
                                                <td>{{$section->name}}</td>
                                                                                                <td><img height="100px" src="{{asset('images/'.$section->img) }}"></td>

                                                <td>
                                                    <button type="button" class="btn btn-outline-danger mt-1 btn-sm"><i
                                                                class="fas fa-trash-alt"
                                                                data-toggle="modal"
                                                                data-target="#dele{{$section->id}}"></i></button>
                                                    <button type="button" class="btn btn-outline-info mt-1 btn-sm"
                                                            data-toggle="modal" data-target="#edit{{$section->id}}"
                                                            class="btn btn-outline-info mr-1"><i
                                                                class="fas fa-edit"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <!-- Edit Product -->
                                            <div class="modal animated zoomInLeft text-left" id="edit{{$section->id}}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="myModalLabel71" style="display: none;"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">تعديل القسم </h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">
                                                                        <!-- Contact Form -->
                                                                        <form action="{{route('edit-sections',['section_id'=>$section->id])}}"
                                                                              method="POST" enctype="multipart/form-data">
                                                                            {{csrf_field()}}
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-group">
                                                                                        <input name="name"
                                                                                               class="form-control"
                                                                                               type="text"
                                                                                               placeholder="اسم القسم="
                                                                                               value="{{$section->name}}"
                                                                                               aria-required="true">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                        <div class="wrap-custom-file inline-block mb-10">
                                                                            <input type="file"
                                                                                   name="img"
                                                                                   id="imagesd2321{{$section->id}}"
                                                                                   accept=".gif, .jpg, .png"/>
                                                                            <label for="imagesd2321{{$section->id}}"
                                                                                    class="file-ok"
                                                                                    style="background-image: url({{url('images/'.$section->img)}});"
                                                                            >
                                                                                <span>تعديل الصورة </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn btn-outline-primary">
                                                                                    حفظ
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">
                                                                                    غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- ////////////////////////////////////////////////////////////////////////////-->
                                            <!-- delete Admin -->
                                            <div class="modal animated zoomInLeft text-left" id="dele{{$section->id}}"
                                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                                 style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">حذف</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Delete Form -->
                                                                        <form action="{{route('delete-sections')}}"
                                                                              method="get">
                                                                            <input type="hidden" name="section_id"
                                                                                   value="{{$section->id}}">
                                                                            {{csrf_field()}}
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <h4> هل أنت متأكد من حذف <span
                                                                                                style="color: #e11d8e">{{$section->name}}</span>
                                                                                        ؟</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn btn-outline-primary">
                                                                                    نعم
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--/ Basic Initialisation table -->
    </div>
    <!-- Modal add -->
    <div class="modal animated zoomInLeft text-left" id="zoomInLeft" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-right">أضف قسم </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="border-1px p-25">
                                <form action="{{route('add-sections')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row" >
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input name="name" class="form-control" type="text"
                                                       placeholder="إسم القسم" value="{{old('name')}}"
                                                       aria-required="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                <div class="wrap-custom-file inline-block mb-10">
                                    <input type="file"
                                           name="img"
                                           id="image321"
                                           accept=".gif, .jpg, .png"/>
                                    <label for="image321"
                                            {{--class="file-ok"--}}
                                            {{--style="background-image: url({{url('images/'.$data->contactphoto)}});"--}}
                                    >
                                        <span>الصورة </span>
                                    </label>
                                </div>
                            </div>

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                        <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">
                                            غلق
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@stop