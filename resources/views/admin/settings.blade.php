@extends('layouts.Admin-Layout')


@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">الاعدادات</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">الاعدادات
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">


            <section class="quill-editor">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><i class="fas fa-cogs ml-1"></i> الاعدادات</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                             @include('errors.erros')
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form action="{{route('edit-settings')}}" method="POST" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <!-- social links-->
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="row">
                                                    <label class="col-sm-2"><i class="fab fa-facebook-square yaya"></i></label>
                                                    <input type="text" name="facebook" class="form-control col-sm-9" placeholder=""
                                                           value="{{old('facebook') ? old('facebook') : $settings->facebook }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="row">
                                                    <label class="col-sm-2"><i
                                                                class="fab fa-twitter-square yaya"></i></label>
                                                    <input type="text" name="twitter" class="form-control col-sm-9" placeholder=""
                                                           value="{{old('twitter') ? old('twitter') : $settings->twitter }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="row">
                                                    <label class="col-sm-2"><i
                                                                class="fab fa-instagram yaya"></i></label>
                                                    <input type="text" name="instagram" class="form-control col-sm-9" placeholder=""
                                                           value="{{old('instagram') ? old('instagram') : $settings->instagram }}">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end social links-->
                                        <!-- logo and sliders -->
                                        <div class="row my-4">
                                            <div class="media col-sm-4">
                                                <div class="wrap-custom-file inline-block mb-10">
                                                    <input type="file"
                                                           name="logo"
                                                           id="image321"
                                                           accept=".gif, .jpg, .png"/>
                                                    <label for="image321"
                                                           class="file-ok"
                                                           style="background-image: url({{url('images/'.$settings->logo)}});"
                                                    >
                                                        <span>لوجو </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="media col-sm-4">
                                                <div class="wrap-custom-file inline-block mb-10">
                                                    <input type="file"
                                                           name="icon"
                                                           id="image3212"
                                                           accept=".gif, .jpg, .png"/>
                                                    <label for="image3212"
                                                           class="file-ok"
                                                           style="background-image: url({{url('images/'.$settings->icon)}});"
                                                    >
                                                        <span>أيقون </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="media col-sm-4">
                                                <div class="wrap-custom-file inline-block mb-10">
                                                    <input type="file"
                                                           name="water_mark"
                                                           id="image321365"
                                                           accept=".gif, .jpg, .png"/>
                                                    <label for="image321365"
                                                           class="file-ok"
                                                           style="background-image: url({{url('images/'.$settings->water_mark)}});"
                                                    >
                                                        <span>Water Mark </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end logo and sliders -->
                                        <!-- Descripations website -->
                                        <div class="row my-4">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>اسم الموقع</label>
                                                    <input type="text" name="site_name" class="form-control " placeholder=" اسم الموقع"
                                                           value="{{old('site_name') ? old('site_name') : $settings->site_name }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>وصف الموقع</label>
                                                    <input type="text" name="site_description" class="form-control " placeholder=" وصف الموقع"
                                                           value="{{old('site_description') ? old('site_description') : $settings->site_description }}">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end Descripations website -->
                                        <div class="row my-1 justify-content-end">
                                            <div class="col-sm-4">
                                                <div class="row justify-content-end">
                                                    <div class="col-sm-4">
                                                        <button class="btn btn-info btn-block">حفظ</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- slider section start -->
            <section id="dropzone-examples">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-plus m-1"></i>السلايدر</h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="form-group">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                                data-target="#zoomInLeft">
                                            أضف سلايدر
                                        </button>


                                    </div>
                                    <table class="table table-striped table-bordered order-column dataex-basic-initialisation">
                                        <thead>
                                        <tr>
                                            <th> الصورة</th>
                                            <th>الاجراء</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        @foreach($sliders as $slider)
                                        <tr>
                                            <td><img height="100px" src="{{ asset('images/'.$slider->img) }}"></td>
                                            <td>
                                                <button type="button" class="btn btn-outline-danger mt-1 btn-sm"
                                                        data-toggle="modal" data-target="#dele{{$slider->id}}"><i
                                                            class="fas fa-trash-alt"></i></button>
                                                <button type="button" class="btn btn-outline-info mt-1 btn-sm"
                                                        data-toggle="modal" data-target="#edit{{$slider->id}}"><i
                                                            class="fas fa-edit"></i></button>
                                            </td>
                                        </tr>
                                        <!-- Edit Product -->
                                        <div class="modal animated zoomInLeft text-left" id="edit{{$slider->id}}" tabindex="-1" role="dialog"
                                             aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title text-right">تعديل السلايدر </h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                                    aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="border-1px p-25">

                                                                    <!-- Contact Form -->
                                                                    <form action="{{route('edit-sliders')}}" method="POST" enctype="multipart/form-data">
                                                                        {{csrf_field()}}
                                                                        <input type="hidden" name="slider_id" value="{{$slider->id}}">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="wrap-custom-file inline-block mb-10">
                                                                                    <input type="file"
                                                                                           name="img"
                                                                                           id="slider123654{{$slider->id}}"
                                                                                           accept=".gif, .jpg, .png"/>
                                                                                    <label for="slider123654{{$slider->id}}"
                                                                                            class="file-ok"
                                                                                            style="background-image: url({{url('images/'.$slider->img)}});"
                                                                                    >
                                                                                        <span>تعديل الصوره </span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                                                            <button type="button" class="btn grey btn-outline-danger"
                                                                                    data-dismiss="modal">غلق
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- delete Product -->
                                        <div class="modal animated zoomInLeft text-left" id="dele{{$slider->id}}" tabindex="-1" role="dialog"
                                             aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title text-right">حذف </h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                                    aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="border-1px p-25">

                                                                    <!-- Contact Form -->
                                                                    <form action="{{route('delete-sliders')}}" method="post">
                                                                        {{csrf_field()}}
                                                                        <input type="hidden" name="slider_id" value="{{$slider->id}}">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <h3>هل أنت متأكد من حذف <span>السلايدر</span></h3>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" class="btn btn-outline-primary">حذف</button>
                                                                            <button type="button" class="btn grey btn-outline-danger"
                                                                                    data-dismiss="modal">غلق
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            @endforeach
                                        </tbody>

                                    </table>
                                    <div class="row my-1 justify-content-end">
                                        <div class="col-sm-4">
                                            <div class="row justify-content-end">
                                                <div class="col-sm-4">
                                                    <button class="btn btn-info btn-block">حفظ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </section>
            <!-- // slider section end -->
            <!-- Modal add -->
            <div class="modal animated zoomInLeft text-left" id="zoomInLeft" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title text-right">أضف سلايدر </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="border-1px p-25">
                                        <form action="{{route('add-sliders')}}" method="POST" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="wrap-custom-file inline-block mb-10">
                                                        <input type="file"
                                                               name="img"
                                                               id="slider123654"
                                                               accept=".gif, .jpg, .png"/>
                                                        <label for="slider123654"
                                                               {{--class="file-ok"--}}
                                                               {{--style="background-image: url({{url('images/'.$data->contactphoto)}});"--}}
                                                        >
                                                            <span>أضف صوره </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                                <button type="button" class="btn grey btn-outline-danger"
                                                        data-dismiss="modal">غلق
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!-- contact section start -->
            <section id="services">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form action="{{route('edit-contact')}}" method="POST">
                                        {{csrf_field()}}
                                    <div class="form-body">

                                        <div class="form-group row">
                                            <label class="col-md-2 label-control" for="user-profile">الوصف</label>
                                            <div class="col-md-10">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" name="desc" value="{{old('desc') ? old('desc') : $contact->desc}}" id="user-profile" class="form-control"
                                                           placeholder="الوصف">
                                                    <div class="form-control-position">
                                                        <i class="la la-arrow-circle-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 label-control">التليفون</label>
                                            <div class="col-md-10">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" name="phone" value="{{old('phone') ? old('phone') : $contact->phone}}" class="form-control" placeholder="التليفون">
                                                    <div class="form-control-position">
                                                        <i class="la la-phone"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 label-control" for="user-profile">العنوان</label>
                                            <div class="col-md-10">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" name="address" value="{{old('address') ? old('address') : $contact->address}}" id="user-profile" class="form-control"
                                                           placeholder="العنوان">
                                                    <div class="form-control-position">
                                                        <i class="la la-arrow-circle-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 label-control" for="email">البريد الالكتروني</label>
                                            <div class="col-md-10">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" name="email" value="{{old('email') ? old('email') : $contact->email}}" id="email" class="form-control"
                                                           placeholder="البريد الالكتروني ">
                                                    <div class="form-control-position">
                                                        <i class="la la-envelope-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row my-1 justify-content-end">
                                        <div class="col-sm-4">
                                            <div class="row justify-content-end">
                                                <div class="col-sm-4">
                                                    <button type="submit" class="btn btn-info btn-block">حفظ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <!-- end info details-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // contact section end -->
            <!-- nabrat section start -->
            <section id="dropzone-examples">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-plus m-1"></i>البنرات</h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered order-column dataex-basic-initialisation">
                                        <thead>
                                        <tr>
                                            <th> الصورة</th>
                                            <th>الاجراء</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        @foreach($nabarats as $nabarat)
                                            <tr>
                                                <td><img height="100px" src="{{ asset('images/'.$nabarat->img1) }}">
                                                <td>
                                                    <button type="button" class="btn btn-outline-info mt-1 btn-sm"
                                                            data-toggle="modal" data-target="#dsdasdqwqwqwwe{{$nabarat->id}}"><i
                                                                class="fas fa-edit"></i></button>
                                                </td>
                                            </tr>
                                            <!-- Edit Product -->
                                            <div class="modal animated zoomInLeft text-left" id="dsdasdqwqwqwwe{{$nabarat->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">تعديل البنرات </h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Contact Form -->
                                                                        <form action="{{route('edit-banarat')}}" method="post" enctype="multipart/form-data">
                                                                            {{csrf_field()}}
                                                                            <input type="hidden" name="id" value="{{$nabarat->id}}">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="wrap-custom-file inline-block mb-10">
                                                                                        <input type="file"
                                                                                               name="img1"
                                                                                               id="img1{{$nabarat->id}}"
                                                                                               accept=".gif, .jpg, .png"/>
                                                                                        <label for="img1{{$nabarat->id}}"
                                                                                               {{--value="{{$nabarat->img1}}"--}}
                                                                                               name="img1"
                                                                                               id="img1"
                                                                                               class="file-ok"
                                                                                               style=" background-image: url({{url('images/'.$nabarat->img1)}}); "
                                                                                        >
                                                                                            <span>تعديل الصوره </span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                                                                <button type="button" class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </section>
            <!-- // nabrat section end -->

            <!-- slider section start -->
            <section id="dropzone-examples">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-plus m-1"></i>الماركات</h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="form-group">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                                data-target="#zoomInLeft-marka">
                                            أضف ماركة
                                        </button>


                                    </div>
                                    <table class="table table-striped table-bordered order-column dataex-basic-initialisation">
                                        <thead>
                                        <tr>
                                            <th> الصورة</th>
                                            <th>الاجراء</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        @foreach($markats as $markat)
                                            <tr>
                                                <td><img height="100px" src="{{ asset('images/'.$markat->img) }}"></td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-danger mt-1 btn-sm"
                                                            data-toggle="modal" data-target="#dele-marka{{$markat->id}}"><i
                                                                class="fas fa-trash-alt"></i></button>
                                                    <button type="button" class="btn btn-outline-info mt-1 btn-sm"
                                                            data-toggle="modal" data-target="#edit-marka{{$markat->id}}"><i
                                                                class="fas fa-edit"></i></button>
                                                </td>
                                            </tr>
                                            <!-- Edit Product -->
                                            <div class="modal animated zoomInLeft text-left" id="edit-marka{{$markat->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">تعديل الماركة </h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Contact Form -->
                                                                        <form action="{{route('edit-marka')}}" method="POST" enctype="multipart/form-data">
                                                                            {{csrf_field()}}
                                                                            <input type="hidden" name="marka_id" value="{{$markat->id}}">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="wrap-custom-file inline-block mb-10">
                                                                                        <input type="file"
                                                                                               name="img"
                                                                                               id="marka123654{{$markat->id}}"
                                                                                               accept=".gif, .jpg, .png"/>
                                                                                        <label for="marka123654{{$markat->id}}"
                                                                                               class="file-ok"
                                                                                               style="background-image: url({{url('images/'.$markat->img)}});"
                                                                                        >
                                                                                            <span>تعديل الصوره </span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                                                                <button type="button" class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- delete Product -->
                                            <div class="modal animated zoomInLeft text-left" id="dele-marka{{$markat->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">حذف </h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Contact Form -->
                                                                        <form action="{{route('delete-marka')}}" method="post">
                                                                            {{csrf_field()}}
                                                                            <input type="hidden" name="marka_id" value="{{$markat->id}}">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <h3>هل أنت متأكد من حذف <span>السلايدر</span></h3>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-outline-primary">حذف</button>
                                                                                <button type="button" class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>

                                    </table>
                                    <div class="row my-1 justify-content-end">
                                        <div class="col-sm-4">
                                            <div class="row justify-content-end">
                                                <div class="col-sm-4">
                                                    <button class="btn btn-info btn-block">حفظ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </section>
            <!-- // slider section end -->
            <!-- Modal add -->
            <div class="modal animated zoomInLeft text-left" id="zoomInLeft-marka" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title text-right">أضف ماركة </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="border-1px p-25">
                                        <form action="{{route('add-marka')}}" method="POST" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="wrap-custom-file inline-block mb-10">
                                                        <input type="file"
                                                               name="img"
                                                               id="marka123654"
                                                               accept=".gif, .jpg, .png"/>
                                                        <label for="marka123654"
                                                                {{--class="file-ok"--}}
                                                                {{--style="background-image: url({{url('images/'.$data->contactphoto)}});"--}}
                                                        >
                                                            <span>أضف صوره </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-outline-primary">حفظ</button>
                                                <button type="button" class="btn grey btn-outline-danger"
                                                        data-dismiss="modal">غلق
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@stop