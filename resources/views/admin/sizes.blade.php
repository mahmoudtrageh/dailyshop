@extends('layouts.Admin-Layout')


@section('content')


    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">المقاسات</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">المقاسات
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
        @include('errors.erros')
        <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><i class="la la-users ml-1"></i> المقاسات</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <span class="alert alert-success">أحدث مقاسات مُضافة</span>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    @foreach($sizes as $size)
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <tr>
                                            <th style="font-size:20px;font-weight: bold;">الاسم</th>
                                            <td style="font-size:20px;font-weight: bold;">المقاس</td>
                                        </tr>
                                        <tr>
                                            <th>محيط الصدر:</th>
                                            <td>{{$size->chest_volume}}</td>
                                        </tr>
                                        <tr>
                                            <th>محيط الكم من أعلي:</th>
                                            <td>{{$size->kom_height}}</td>
                                        </tr>
                                        <tr>
                                            <th>محيط الوسط:</th>
                                            <td>{{$size->center_volume}}</td>
                                        </tr>
                                        <tr>
                                            <th>طول الظهر:</th>
                                            <td>{{$size->back_length}}</td>
                                        </tr>
                                        <tr>
                                            <th>محيط أكبر حجم:</th>
                                            <td>{{$size->highest_volume}}</td>
                                        </tr>
                                        <tr>
                                            <th>طول الصدر إلى الوسط:</th>
                                            <td>{{$size->chest_to_center}}</td>
                                        </tr>
                                        <tr>
                                            <th>عرض الظهر:</th>
                                            <td>{{$size->back_width}}</td>
                                        </tr>
                                        <tr>
                                            <th>الطول من الوسط إلى الركبة:</th>
                                            <td>{{$size->center_to_leg}}</td>
                                        </tr>
                                        <tr>
                                            <th>طول الكتف:</th>
                                            <td>{{$size->shoulder_length}}</td>
                                        </tr>
                                        <tr>
                                            <th>محيط الأسورة:</th>
                                            <td>{{$size->eswra_volume}}</td>
                                        </tr>
                                        <tr>
                                            <th>طول الكم:</th>
                                            <td>{{$size->kom_length}}</td>
                                        </tr>
                                        <tr>
                                            <th>الطول الكلي للجسم:</th>
                                            <td>{{$size->total_length}}</td>
                                        </tr>
                                    </table>
                                        @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

@stop
@section('js')


@stop