@extends('layouts.Admin-Layout')


@section('content')


    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">المستخدمين</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin-index')}}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">المستخدمين
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
        @include('errors.erros')
        <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><i class="la la-users ml-1"></i> المستخدمين</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="row">
                                    <div class="col-xl-2">
                                        <button type="button" class="btn btn-outline-info mx-2" data-toggle="modal"
                                                data-target="#addusers">اضافة مستخدم
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                        <tr>
                                            <th>الاسم</th>
                                            <th>رقم الجوال</th>
                                            <th>البريد الالكتروني</th>
                                            <th>المقاسات</th>
                                            <th>الحالة</th>
                                            <th>الاجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->phone}}</td>
                                                <td>{{$user->email}}</td>
                                                <td><button class="btn btn-sm btn-outline-success"><a href="{{route('sizes',['id'=>$user->id])}}">المقاسات</a></button></td>
                                                <td>
                                                    <div class="form-group">
                                                        <select class="form-control stutus" uid="{{ $user->id }}">
                                                            <option @if($user->is_active) selected
                                                                    @endif value="1">
                                                                نشط
                                                            </option>
                                                            <option @if(!$user->is_active) selected
                                                                    @endif value="0">
                                                                معلق
                                                            </option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-outline-danger"
                                                            data-toggle="modal"
                                                            data-target="#dele{{$user->id}}"><i
                                                                class="fas fa-trash-alt"></i></button>
                                                    {{--<button class="btn btn-sm btn-outline-info" data-toggle="modal"--}}
                                                            {{--data-target="#edit{{$user->id}}"><i class="fas fa-edit"></i>--}}
                                                    {{--</button>--}}
                                                </td>
                                            </tr>
                                            <!-- delete Admin -->
                                            <div class="modal animated zoomInLeft text-left" id="dele{{$user->id}}"
                                                        tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
                                                        style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-right">حذف</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="border-1px p-25">

                                                                        <!-- Delete Form -->
                                                                        <form action="{{route('delete-users')}}"
                                                                              method="get">
                                                                            <input type="hidden" name="user_id"
                                                                                   value="{{$user->id}}">
                                                                            {{csrf_field()}}
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <h4> هل أنت متأكد من حذف <span
                                                                                                style="color: #e11d8e">{{$user->name}}</span>
                                                                                        ؟</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn btn-outline-primary">
                                                                                    نعم
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn grey btn-outline-danger"
                                                                                        data-dismiss="modal">غلق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>
    <!-- add user -->
    <div class="modal fade text-left" id="addusers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel11"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel11">اضافة مستخدم</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('add-users')}}" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name">الاسم</label>
                                    <input type="text" name="name" value="{{old('name')}}" id="name"
                                           class="form-control" placeholder="الاسم">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="email">البريد الالكتروني</label>
                                    <input type="email" id="email" name="email" value="{{old('email')}}"
                                           class="form-control" placeholder="البريد الالكتروني">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="tel">رقم الجوال</label>
                                    <input type="tel" id="tel" name="phone" value="{{old('phone')}}"
                                           class="form-control" placeholder="رقم الجوال">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="password">كلمة المرور</label>
                                    <input type="password" id="password" name="password" class="form-control"
                                           placeholder="كلمة المرور">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">غلق
                            </button>
                            <button type="submit" class="btn btn-outline-primary">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--<!-- edit user -->--}}
    {{--<div class="modal fade text-left" id="edit{{$user->id}}" tabindex="-1"--}}
         {{--role="dialog" aria-labelledby="myModalLabel1"--}}
         {{--aria-hidden="true">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h4 class="modal-title" id="myModalLabel1">تعديل مستخدم</h4>--}}
                    {{--<button type="button" class="close" data-dismiss="modal"--}}
                            {{--aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<form action="{{route('edit-users')}}" method="POST">--}}
                        {{--{{csrf_field()}}--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="name">الاسم</label>--}}
                                    {{--<input type="text" name="name"--}}
                                           {{--value="{{$user->name}}" id="name"--}}
                                           {{--class="form-control"--}}
                                           {{--placeholder="الاسم">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="email">البريد الالكتروني</label>--}}
                                    {{--<input type="email" id="email" name="email"--}}
                                           {{--value="{{$user->email}}"--}}
                                           {{--class="form-control"--}}
                                           {{--placeholder="البريد الالكتروني">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="tel">رقم الجوال</label>--}}
                                    {{--<input type="tel" id="tel" name="phone"--}}
                                           {{--value="{{$user->phone}}"--}}
                                           {{--class="form-control"--}}
                                           {{--placeholder="رقم الجوال">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="password">كلمة المرور</label>--}}
                                    {{--<input type="password" id="password"--}}
                                           {{--name="password" class="form-control"--}}
                                           {{--placeholder="كلمة المرور">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="modal-footer">--}}
                            {{--<button type="button"--}}
                                    {{--class="btn grey btn-outline-secondary"--}}
                                    {{--data-dismiss="modal">غلق--}}
                            {{--</button>--}}
                            {{--<button type="submit"--}}
                                    {{--class="btn btn-outline-primary">حفظ--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</form>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- ////////////////////////////////////////////////////////////////////////////-->--}}
@stop
@section('js')


    <script>
        $(document).on("change", ".stutus", function () {
            var status = $(this).val();
            var id = $(this).attr("uid");
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ url('admin/update-statuss') }}",
                type: "post",
                dataType: "json",
                data: {status: status, id: id, _token: token},
                success: function (data) {
                    // console.log(data.check);
                    if (data.status !== "ok") {
                        alert("ERROR");
                    }

                },
                error: function () {
                    alert("ERROR");
                }
            })
        })
    </script>

@stop