<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<title>Tohfa</title>
<meta name="yehia ali yehia" >
{{--<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/'.settings()->icon)}}">--}}
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
      rel="stylesheet">
<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
      rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
<!-- BEGIN VENDOR CSS-->



<link href="{{ asset('manager/app-assets/css-rtl/vendors.css')}}" media="all" rel="stylesheet" type="text/css" />
{{--<link href="{{ asset('manager/app-assets/vendors/css/tables/datatable/datatables.min.css')}}" media="all" rel="stylesheet" type="text/css" />--}}
{{--<!-- END VENDOR CSS-->--}}
{{--<!-- BEGIN MODERN CSS-->--}}
<link href="{{ asset('manager/app-assets/css-rtl/app.css')}}" media="all" rel="stylesheet" type="text/css" />
{{--<link href="{{ asset('manager/app-assets/css-rtl/custom-rtl.css')}}" media="all" rel="stylesheet" type="text/css" />--}}
{{--<!-- END MODERN CSS-->--}}
{{--<!-- BEGIN Page Level CSS-->--}}
<link href="{{ asset('manager/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css')}}" media="all" rel="stylesheet" type="text/css" />
{{--<link href="{{ asset('manager/app-assets/css-rtl/core/colors/palette-gradient.css')}}" media="all" rel="stylesheet" type="text/css" />--}}
{{--<!-- END Page Level CSS-->--}}
{{--<!-- BEGIN Custom CSS-->--}}
{{--<link href="{{ asset('manager/assets/css/style-rtl.css')}}" media="all" rel="stylesheet" type="text/css" />--}}

<link rel="stylesheet" href="{{asset('public/css/admin.css')}}">

<!-- END Custom CSS-->
<meta name="csrf-token" content="{{ csrf_token() }}">


<style>


    .wrap-custom-file {
        position: relative;
        display: inline-block;
        width: 150px;
        height: 150px;
        margin: 0 0.5rem 1rem;
        text-align: center;
    }

    .wrap-custom-file input[type="file"] {
        position: absolute;
        top: 0;
        left: 0;
        width: 2px;
        height: 2px;
        overflow: hidden;
        opacity: 0;
    }

    .wrap-custom-file label {
        z-index: 1;
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        width: 100%;
        overflow: hidden;
        padding: 0 0.5rem;
        cursor: pointer;
        background-color: #fff;
        border-radius: 4px;
        -webkit-transition: -webkit-transform 0.4s;
        transition: -webkit-transform 0.4s;
        transition: transform 0.4s;
        transition: transform 0.4s, -webkit-transform 0.4s;
    }

    .wrap-custom-file label span {
        display: block;
        margin-top: 2rem;
        font-size: 1.4rem;
        color: #777;
        -webkit-transition: color 0.4s;
        transition: color 0.4s;
    }

    .wrap-custom-file label:hover {
        -webkit-transform: translateY(-1rem);
        transform: translateY(-1rem);
    }

    .wrap-custom-file label:hover span { color: #333; }

    .wrap-custom-file label.file-ok {
        background-size: cover;
        background-position: center;
    }

    .wrap-custom-file label.file-ok span {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        padding: 0.3rem;
        font-size: 1.1rem;
        color: #000;
        background-color: rgba(255, 255, 255, 0.7);
    }

</style>