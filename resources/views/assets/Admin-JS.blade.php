
{{--<!-- BEGIN VENDOR JS-->--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/vendors/js/vendors.min.js')}}"></script>--}}
{{--<!-- BEGIN VENDOR JS-->--}}
{{--<!-- BEGIN PAGE VENDOR JS-->--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/vendors/js/charts/chart.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/vendors/js/charts/raphael-min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/vendors/js/charts/morris.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}"></script>--}}

{{--<script type="text/javascript" src="{{asset('manager/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/data/jvector/visitor-data.js')}}"></script>--}}
{{--<!-- END PAGE VENDOR JS-->--}}
{{--<!-- BEGIN MODERN JS-->--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/js/core/app-menu.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/js/core/app.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/js/scripts/customizer.js')}}"></script>--}}
{{--<!-- END MODERN JS-->--}}
{{--<!-- BEGIN PAGE LEVEL JS-->--}}
{{--<script type="text/javascript" src="{{asset('manager/app-assets/js/scripts/pages/dashboard-sales.js')}}"></script>--}}
{{--<!-- END PAGE LEVEL JS-->--}}

<script type="text/javascript" src="{{asset('public/js/admin.js')}}"></script>



<script>

    setTimeout(fade_out, 5000);

    function fade_out() {
        $("#checker").fadeOut().empty();
    }

</script>

<script>

    $('input[type="file"]').each(function(){

        var $file = $(this),
            $label = $file.next('label'),
            $labelText = $label.find('span'),
            labelDefault = $labelText.text();

        $file.on('change', function(event){
            var fileName = $file.val().split( '\\' ).pop(),
                tmppath = URL.createObjectURL(event.target.files[0]);
            if( fileName ){
                $label
                    .addClass('file-ok')
                    .css('background-image', 'url(' + tmppath + ')');
                $labelText.text(fileName);
            }else{
                $label.removeClass('file-ok');
                $labelText.text(labelDefault);
            }
        });

    });

</script>






