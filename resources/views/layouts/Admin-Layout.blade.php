<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    @include('assets.Admin-CSS')
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu" data-col="2-columns">
<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">

    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a
                            class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item">
                    <a class="navbar-brand" href="{{route('admin-index')}}">
                        <img class="brand-logo" alt="tohfa logo" src="{{ asset('images/'.settings()->logo) }}">
                        <h3 class="brand-text"> tohfa Admin</h3>
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i
                                class="la la-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs"
                                                              href="#"><i class="ft-menu"></i></a></li>
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i
                                    class="ficon ft-maximize"></i></a></li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"
                           aria-expanded="false">
                <span class="mr-1">Hello,
                  <span class="user-name text-bold-700">{{auth()->user()->name}}</span>
                </span>
                            <span class="avatar avatar-online">
                  <img src="{{ asset('images/'.auth()->user()->img) }}" alt="avatar"><i></i></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{route('profile')}}"><i
                                        class="ft-user"></i> Edit Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('password')}}"><i class="ft-user"></i> Edit Password</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('logout')}}"><i class="ft-power"></i> Logout</a>
                        </div>

                    </li>
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i>
                            <span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow">{{count(postNotifications())}}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header m-0">
                                    <span class="grey darken-2">Notifications</span>
                                </h6>
                                <span class="notification-tag badge badge-default badge-danger float-right m-0">{{count(postNotifications())}} New</span>
                            </li>

                            @foreach(postNotifications() as $not)


                                    <li class="scrollable-container media-list w-100">
                                        <div>
                                            <a href="{{$not->data['Url'] == 'message' ? route('read',['id'=>$not->id]) : ''}}">
                                                <div class="media">
                                                    <div class="media-left align-self-center"><i
                                                                class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                                                    <div class="media-body">
                                                        <h6 class="media-heading">{{$not->data['Message']}}</h6>
                                                        <small>
                                                            <time class="media-meta text-muted"
                                                                  datetime="2015-06-11T18:29:20+08:00"></time>
                                                        </small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                @endforeach

                        </ul>
                    </li>

                    {{--<notify :nots="nots" :url="urlRead"></notify>--}}
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item"><a href="{{route('admin-index')}}"><i class="la la-home"></i><span
                            class="menu-title">الرئيسية</span></a></li>
 @if(Has_permission(1))
            <li class=" nav-item"><a href="{{route('admins')}}"><i class="fas fa-user-secret"></i><span
                            class="menu-title">المشرفين</span></a>
            </li>
                        @endif

             @if(Has_permission(2))
            <li class=" nav-item"><a href="{{route('sections')}}"><i class="fas fa-plus"></i><span class="menu-title">الاقسام</span></a>
            </li>
                        @endif

             @if(Has_permission(3))
            <li class=" nav-item"><a href="{{route('products')}}"><i class="fab fa-product-hunt"></i><span
                            class="menu-title">المنتجات</span></a>
            </li>
                        @endif

             @if(Has_permission(4))
            <li class=" nav-item"><a href="{{route('invoice')}}"><i class="fas fa-money-bill-wave-alt"></i><span
                            class="menu-title">الفواتير</span></a></li>
                                    @endif
    
                             @if(Has_permission(5))
            <li class=" nav-item"><a href="{{route('users')}}"><i class="la la-users"></i><span
                            class="menu-title">المستخدمين</span></a></li>
                                     @endif
   
                             @if(Has_permission(9))
            <li class=" nav-item"><a href="{{route('countries')}}"><i class="la la-cc-visa"></i><span
                            class="menu-title">الدول</span></a></li>
                                      @endif
  
                             @if(Has_permission(6))
            <li class=" nav-item"><a href="{{route('messages')}}"><i class="la la-envelope"></i><span
                            class="menu-title">الرسائل</span></a></li>
                                    @endif
    
                             @if(Has_permission(7))
            <li class=" nav-item"><a href="{{route('About')}}"><i class="la la-info"></i><span
                            class="menu-title">من نحن</span></a>
            </li>
                        @endif

             @if(Has_permission(8))
            <li class=" nav-item"><a href="{{route('settings')}}"><i class="fas fa-cogs"></i><span
                            class="menu-title">الاعدادات</span></a></li>
                                        @endif

            @if(Has_permission(10))
                <li class=" nav-item"><a href="{{route('coupons')}}"><i class="fas fa-cogs"></i><span
                                class="menu-title">الكوبونات</span></a></li>
            @endif

        </ul>
    </div>
</div>
<div class="app-content content">
    @yield('content')
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2"
                                                                                     href="http://tagererp.com/"
                                                                                     target="_blank">Karim Helmy</a>, All rights reserved. </span>
        <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block"> <i
                    class="ft-heart pink"></i></span>
    </p>
</footer>
@include('assets.Admin-JS')
@yield('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
</body>
</html>