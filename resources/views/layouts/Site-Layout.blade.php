<!DOCTYPE html>
<html lang="ar" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('title')

    @include('assets.Site-CSS')

    {{--<link href="{{asset('site/css/bootstarpRTl.css')}}" rel="stylesheet">--}}

    <style>

        .card-color {
            color: red !important;
        }

        .card-background{
            background-color: black !important;
        }

        .new-quick{
            background-color: #fff;
            border-radius: 50%;
            padding: 10px;
            margin-top: 20px;
        }

    </style>

</head>

<body>

<!-- wpf loader Two -->
<div id="wpf-loader-two">
    <div class="wpf-loader-two-inner">
        <span>Loading</span>
    </div>
</div>
<!-- / wpf loader Two -->
<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
<!-- END SCROLL TOP BUTTON -->


<!-- Start header section -->
<header id="aa-header">
    <!-- start header top  -->
    <div class="aa-header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-header-top-area" style="direction: rtl;">

                        <div class="aa-header-top-right">
                            @if(auth()->user())

                            <ul class="aa-head-top-nav-right">
                                <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                                <li><a href="{{route('site-profile')}}">مرحبًا ، {{auth()->user()->name}}</a></li>
                                <li class="hidden-xs"><a  href="{{route('site.wishlist')}}">المفضلة</a></li>
                                <li class="hidden-xs"><a href="{{route('site.receipt')}}">الفواتير</a></li>
                                {{--<li class="hidden-xs"><a href="checkout.html">الدفع</a></li>--}}
                                <li>
                                    <a class="dropdown-item" href="{{route('logout')}}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('تسجيل الخروج') }}
                                    </a>
                                </li>

                                <form id="logout-form" action="{{route('site.logout')}}" method="get" style="display: none;">
                                    {{csrf_field()}}
                                </form>
                            </ul>

                                @else

                                <ul class="aa-head-top-nav-right">
                                    <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                                    <li><a href="" data-toggle="modal" data-target="#login-modal">تسجيل الدخول</a></li>
                                    <li class="hidden-xs"><a href="{{route('site.get.register')}}">إنشاء حساب</a></li>
                                </ul>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / header top  -->

    <!-- start header bottom  -->
    <div class="aa-header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-header-bottom-area">
                        <!-- logo  -->
                        <div class="aa-logo">
                            <!-- Text based logo -->
                            <a href="index.html">
                                <span class="fa fa-shopping-cart"></span>
                                <p><strong>التسوق</strong> اليومى<span>شريك التسوق الخاص بك</span></p>
                            </a>
                            <!-- img based logo -->
                            <!-- <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> -->
                        </div>
                        <!-- / logo  -->
                        <!-- cart box -->
                        <div class="aa-cartbox">
                            @if(auth()->user())
                            <a class="aa-cart-link" href="{{route('cart.index')}}">
                                <span class="fa fa-shopping-basket"></span>
                                <span class="aa-cart-title">عربة التسوق</span>
                                <span class="aa-cart-notify" id="CartCount">{{count(auth()->user()->cart)}}</span>
                            </a>
                            @else

                                <a class="aa-cart-link" href="{{route('cart.index')}}">
                                    <span class="fa fa-shopping-basket"></span>
                                    <span class="aa-cart-title">عربة التسوق</span>
                                    <span class="aa-cart-notify" id="CartCount"></span>
                                </a>

                            @endif
                            <div class="aa-cartbox-summary">

                                <ul>
                                    @if(auth()->user())
                                        @foreach($carts as $cart)
                                            <li id="row{{$cart->id}}">
                                                <a class="aa-cartbox-img" href="{{route('product.detail',['id'=>$cart->id])}}"><img src="{{asset('images/' . $cart->img)}}" alt="img"></a>
                                                <div class="aa-cartbox-info">
                                                    <h4><a href="{{route('product.detail',['id'=>$cart->id])}}">{{$cart->name}}</a></h4>
                                                </div>
                                                <a class="aa-remove-product del" cid="{{$cart->id}}"><span class="fa fa-times"></span></a>
                                            </li>
                                        @endforeach
                                    @endif

                                        <li>

                        <span class="aa-cartbox-total-title">
                         المجموع الكلى
                        </span>
                                        <span>
                                          @if(auth()->user())
                                            {{array_sum($carts->pluck('price_before')->toArray())}}
                                          @endif
                                         </span>
                                    </li>

                                </ul>
                                <a class="aa-cartbox-checkout aa-primary-btn" href="{{route('cart.index')}}">الدفع</a>
                            </div>
                        </div>
                        <!-- / cart box -->
                        <!-- search box -->
                        <div class="aa-search-box">
                            <form action="">
                                <input type="text" name="" id="" placeholder="إبحث هنا' ">
                                <button type="submit"><span class="fa fa-search"></span></button>
                            </form>
                        </div>
                        <!-- / search box -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / header bottom  -->
</header>
<!-- / header section -->
<!-- menu -->
<section id="menu">
    <div class="container">
        <div class="menu-area">
            <!-- Navbar -->
            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse">
                    <!-- Left nav -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{route('site.about.us')}}">من نحن</a></li>
                        <li><a href="{{route('site.contact.us')}}">تواصل معنا</a></li>
                        <li><a href="{{route('site-shop')}}">المتجر</a></li>
                        <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
</section>
<!-- / menu -->

@include('errors.erros')

@yield('content')

<!-- footer -->
<footer id="aa-footer">

    <!-- footer-bottom -->
    <div class="aa-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-footer-bottom-area">
                        <p>Designed by <a href="http://www.markups.io/">MarkUps.io</a></p>
                        <div class="aa-footer-social">
                            <a href="{{settings()->facebook}}"><span class="fa fa-facebook"></span></a>
                            <a href="{{settings()->twitter}}"><span class="fa fa-twitter"></span></a>
                            <a href="{{settings()->instagram}}"><span class="fa fa-instagram"></span></a>
                        </div>
                        <!-- <div class="aa-footer-payment">
                          <span class="fa fa-cc-mastercard"></span>
                          <span class="fa fa-cc-visa"></span>
                          <span class="fa fa-paypal"></span>
                          <span class="fa fa-cc-discover"></span>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- / footer -->

<!-- Login Modal -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" dir="rtl">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="float:left">&times;</button>
                <h4>دخول او إنشاء حساب</h4>
                <form class="aa-login-form" method="post" action="{{route('site-login-check')}}">
                    {{csrf_field()}}
                    <label for="">الاسم او البريد الالكترونى<span>*</span></label>
                    <input name="email" type="text" placeholder="الاسم او البريد الالكترونى">
                    <label for="">كلمة المرور<span>*</span></label>
                    <input name="password" type="password" placeholder="كلمة المرور">
                    <button class="aa-browse-btn" type="submit">تسجيل الدخول</button>
                    <label for="rememberme" class="rememberme"><input type="checkbox" id="rememberme"> ذكرنى </label>
                    <p class="aa-lost-password"><a href="#">نسيتى كلمة المرور؟</a></p>
                    <div class="aa-register-now">
                        لديكى حساب؟<a href="{{route('site.get.register')}}">سجلى الان!</a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

@include('assets.Site-JS')
@yield('js')

<script>

    setTimeout(fade_out, 5000);

    function fade_out() {
        $("#checker").fadeOut().empty();
    }

</script>


</body>

</html>