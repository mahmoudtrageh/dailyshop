@extends('layouts.Site-Layout')


@section('title')

    <title> About-Us</title>

@stop

@section('content')

    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="{{asset('site/images/fashion/contact_01_bg.jpg')}}" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>من نحن</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                        <li class="active">من نحن</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->
    <!-- start contact section -->
    <section id="aa-contact" dir="rtl" style="padding:50px 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="contact-us">
                        <div class="row">
                            @foreach($subs as $sub)
                            <div class="col-lg-4">
                                <div class="contact-us-details text-center" style="background:white;padding:10px 0;">
                                    <div class="card">
                                        <i class="fa fa-commenting-o fa-5x" style="color:#ff6666" aria-hidden="true"></i>

                                        <div class="card-body">
                                            <h3>{{$sub->title1}}</h3>
                                            <p class="card-text">{{$sub->content1}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="contact-us-details text-center" style="background:white;padding:10px 0;">
                                    <div class="card">
                                        <i class="fa fa-users fa-5x" style="color:#ff6666" aria-hidden="true"></i>

                                        <div class="card-body">
                                            <h3>{{$sub->title2}}</h3>
                                            <p class="card-text">{{$sub->content2}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="contact-us-details text-center" style="background:white;padding:10px 0;">
                                    <div class="card">
                                        <i class="fa fa-briefcase fa-5x" style="color:#ff6666" aria-hidden="true"></i>

                                        <div class="card-body">
                                            <h3>{{$sub->title3}}</h3>
                                            <p class="card-text">{{$sub->content3}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop