@extends('layouts.Site-Layout')

@section('title')



@stop

@section('content')

    <main class="main-content" style="height: 70vh;">
    <!--Breadcrumbs start-->
    <div class="breadcrumbs text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-title">
                        <h2>تأكيد الكود</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs end-->
    <!--Contact form start-->
    <div class="contact-form ptb-100">
        <div class="container">
            <div class="alert alert-success messagesub text-center">
                برجاء إدخال الكود المرسل للبريد الإلكترونى
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="contact-form">
                        <p class="form-messege"></p>
                        <form id="contact-form" action="{{route('code.confirmation')}}#confirm-code" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="email" value="{{$email}}"/>
                            <input class="form-control"  type="email" placeholder="البريد الالكتروني" value="{{$email}}" readonly >
                            <input class="form-control" style="margin-top: 20px" type="text" name="code" placeholder="ادخل الكود">

                            <button class="btn btn-primary" style="margin-top: 20px" type="submit">تاكيد</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--Contact form end-->
    </main>
@stop