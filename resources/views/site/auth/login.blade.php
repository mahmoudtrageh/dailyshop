@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Login </title>

@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

@section('content')

    <!-- catg header banner section -->
    <section id="aa-catg-head-banner" >
        <img src="{{asset('site/images/fashion/heading-bg.jpg')}}" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>حسابى</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                        <li class="active">حسابى</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <section id="aa-myaccount"  dir="rtl">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-myaccount-area">
                        <div class="row">
                            <div class="col-md-6">
                    <div id="login" class="aa-myaccount-login" >
                        <h4>دخول</h4>
                        <form method="post" action="{{route('site-login-check')}}" class="aa-login-form">
                            {{csrf_field()}}
                            <label for="">اسم المستخدم او البريد الالكنرونى<span>*</span></label>
                            <input name="email" type="text" placeholder="اسم المستخدم او البريد الالكترونى">
                            <label for="">كلمة المرور<span>*</span></label>
                            <input name="password" type="password" placeholder="كلمة المرور">
                            <button type="submit" class="aa-browse-btn">دخول</button>
                            <label class="rememberme" for="rememberme"><input type="checkbox" id="rememberme">ذكرنى</label>
                            <p id="go-to-reset" class="aa-lost-password">نسيت كلمة المرور؟</p>
                        </form>
                    </div>
                    </div>

                            <div class="col-md-6">
                    <div class="register-box">
                        <h3>إنشاء حساب</h3>
                        <div class="register_des">
                            <p>من خلال إنشاء حساب على موقعنا الإلكتروني ، ستتمكن من التسوق بشكل أسرع ، والاطلاع على حالة الطلبات وتتبع الطلبات التي قمت بها سابقًا</p>
                        </div>
                        <div class="submit">
                            <a class="btn btn-outline exclusive" href="{{route('site.get.register')}}">
                        <span>
                            <i class="fa fa-user"></i>&nbsp;
                            إنشاء حساب جديد
                        </span>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div id="reset" class="aa-myaccount-login" style="display:none;">
                        <h4>إعادة كلمة المرور</h4>
                        <form method="post" action="{{route('site-check')}}" class="aa-login-form">
                            {{csrf_field()}}
                            <label for="">سيتم ارسال رسالة الى الإيميل الخاص بك</label>
                            <input name="email" type="text" placeholder=" البريد الالكترونى">

                            <button type="submit" class="aa-browse-btn">إرسال</button>
                            <a id="back-to-login" class="aa-browse-btn" href="{{route('site.get.login')}}">العودة إلى تسجيل الدخول</a>

                        </form>
                    </div>
                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @stop


@section('js')

    <script>
        $(document).ready(function(){
            $("#go-to-reset").click(function(){
                $("#login").hide();
                $("#reset").show();
            });
            $("#go-to-login").click(function(){
                $("#reset").hide();
                $("#login").show();
            });
        });
    </script>

    @stop