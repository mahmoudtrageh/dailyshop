@extends('layouts.Site-Layout')

@section('title')



@stop

@section('content')

    <main class="main-content" style="height: 64vh;">
    <!--Breadcrumbs start-->
    <div class="breadcrumbs text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-title">
                        <h2>تغير كلمه المرور</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs end-->
    <!--Contact form start-->
    <div class="contact-form ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="contact-form">
                        <p class="form-messege"></p>
                        <form id="contact-form" action="{{route('site.pass.change')}}#change-password" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="email" value="{{$email}}"/>
                            <input class="form-control" type="password" name="password" placeholder=" كلمه المرور الجديدة">
                            <input class="form-control" style="margin-top: 20px" type="password" name="password_confirmation" placeholder=" تاكيد كلمه المرور الجديدة">
                            <button class="btn btn-primary" style="margin-top: 20px" type="submit">تاكيد التغير</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--Contact form end-->

    </main>
@stop