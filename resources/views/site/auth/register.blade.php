@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Register </title>

@stop

@section('content')

    <main class="main-content">

        <div id="register-page" class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="register-box">
                        <h1>إنشاء حساب</h1>
                        <form method="post" action="{{route('site.register')}}" id="create_customer" accept-charset="UTF-8"><input type="hidden" name="form_type" value="create_customer" /><input type="hidden" name="utf8" value="✓" />
                            {{csrf_field()}}

                            <h3 class="page-subheading">معلوماتك الشخصية</h3>

                            <label for="FirstName" class="label-register">الأسم</label>
                            <input type="text" name="name" id="FirstName" class="form-control" placeholder="الأسم"  autocapitalize="words" autofocus>
                            <label for="LastName" class="label-register">رقم التيليفون</label>
                            <input type="tel" name="phone" id="LastName" class="form-control" placeholder="رقم التيليفون"  autocapitalize="words">
                            <label for="Email" class="label-register">البريد الألكترونى</label>
                            <input type="email" name="email" id="Email" placeholder="البريد الألكترونى" class="form-control "  autocorrect="off" autocapitalize="off">
                            <label for="CreatePassword" class="label-register">كلمة المرور</label>
                            <input type="password" name="password" id="CreatePassword" placeholder="كلمة المرور" class="form-control ">
                            <p>
                                <input type="submit" value="إنشاء حساب" class="btn btn-outline">
                            </p>
                            <a class="link-back" href="../index.html">
                        <span>
                            <i class="fa fa-long-arrow-left"></i>&nbsp;
                           العودة إلى المتجر
                        </span>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    @stop