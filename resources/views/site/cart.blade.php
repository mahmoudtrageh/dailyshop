@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Cart </title>


@stop

@section('content')


    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="{{asset('site/images/fashion/cart-inr-bnr.jpg')}}" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>عربة التسوق</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                        <li class="active">العربة</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <!-- Cart view section -->
    <section id="cart-view">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cart-view-area" dir="rtl">
                        @if(count(auth()->user()->cart) == 0)
                            <div class="alert alert-warning " style="text-align: center" role="alert">there is no products in cart

                            </div>
                        @else

                            <div class="cart-view-table">
                            <form action="{{route('check-out')}}" method="post">
                                {{csrf_field()}}
                                <div class="table-responsive">
                                    @foreach($carts as $cart)

                                    <table class="table" id="row{{$cart->id}}">
                                        @endforeach
                                        <thead>
                                        <tr>
                                            <th>حذف</th>
                                            <th>الصورة</th>
                                            <th>المنتجات</th>
                                            <th>السعر</th>
                                            <th>الكمية</th>
                                            <th>المجموع</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($carts as $cart)

                                            <tr>
                                            <td><a class="remove del" cid="{{$cart->id}}" href="#"><fa class="fa fa-close"></fa></a></td>
                                            <td><a href="#"><img src="{{asset('images/' . $cart->img)}}" alt="img"></a></td>
                                            <td><a class="aa-cart-title" href="#">{{$cart->name}}</a></td>
                                            @if($cart->price_after == null)

                                                    <td class="u_price"
                                                        id="current_price_{{$cart->id}}">{{$cart->price_before}}</td>
                                            @else
                                                    <td class="u_price"
                                                        id="current_price_{{$cart->id}}">{{$cart->price_after}}</td>
                                            @endif
                                            <td class="p_quantity">
                                                <input id="counter_pro" value="1" product_id="{{$cart->id}}"

                                                       class="category-items quantity"></td>

                                            @if($cart->price_after == null)

                                                <td class="u_price ttl"
                                                    id="total_price_{{$cart->id}}">{{$cart->price_before}}</td>

                                            @else
                                                <td class="u_price ttl"
                                                    id="total_price_{{$cart->id}}">{{$cart->price_after}}</td>

                                            @endif

                                        </tr>
                                        @endforeach

                                        <tr>
                                            <td colspan="6" class="aa-cart-view-bottom">
                                                <div class="aa-cart-coupon">

                                                    <input class="aa-coupon-code" id="code" type="text" name="coupon" placeholder="كوبون">
                                                    <span style="display: none;" class="d-none valid alert alert-success">هذا الكوبون متاح</span>
                                                    <span style="display: none;" class="d-none finish alert alert-danger">هذا الكوبون انتهي</span>
                                                    <span style="display: none; "class="d-none invalid alert alert-danger">هذا الكوبون غير صحيح</span>

                                                    <button class="aa-cart-view-btn activate" type="button">تفعيل الكوبون</button>

                                                </div>
                                                <input class="aa-cart-view-btn" type="submit" value="تحديث العربة">

                                                @foreach($carts as $cart)
                                                    <input id="quant{{$cart->id}}" type="hidden" value="1" name="quantity[]">
                                                @endforeach

                                                <div class="form-group">
                                                    <select class="form-control country" name="country">
                                                        <option selected disabled> اختر الدولة</option>
                                                        @foreach($countries as $country)
                                                            <option  value="{{$country->id}}">{{$country->name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div class="form-group">
                                                    <select id="all_cities" class="form-control cities" name="city">
                                                        <option selected disabled> اختر الدولة اولاً</option>
                                                    </select>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="address"
                                                           placeholder="عنوان الشارع"
                                                           value="{{old('address')}}">
                                                </div>
                                                <div class="form-group">
                                                    <input type="phone" name="phone" class="form-control"
                                                           placeholder="رقم الهاتف"
                                                           value="{{old('phone')}}">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="bbox" class="form-control"
                                                           placeholder="الرمز البريدي"
                                                           value="{{old('bbox')}}">
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <!-- Cart Total view -->
                            <div class="cart-view-total">
                                <h4>الأجمالى</h4>
                                <table class="aa-totals-table">
                                    <tbody>
                                    <tr>

                                        <td>المجموع الفرعي</td>
                                        <td><span id="grandTotal"></span></td>
                                    </tr>
                                    <tr>

                                        <td>الشحن</td>
                                        <td><span id="delivery"></span></td>
                                    </tr>
                                    <tr>

                                        <td>المجموع الكلي</td>
                                        <td><span id="LastTotal"></span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <button type="submit"  class="aa-cart-view-btn">إرسال الفاتورة</button>
                            </form>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / Cart view section -->

@stop

@section('js')

    <script>

        $(document).on('change', '#counter_pro', function (e) {
            e.preventDefault();
            var counter_pro = $(this).val();
            var product_id = $(this).attr('product_id');
            var price = Number(document.getElementById('current_price_' + product_id).innerText);
            var total = document.getElementById('total_price_' + product_id);
            total.innerText = price * counter_pro;
            $('#quant' + product_id).val(counter_pro);

            function calcTotal() {
                output = 0;
                $('.ttl').each(function () {
                    output += parseInt($(this).text())
                });
                return output
            }

            $("#grandTotal").text(calcTotal());
            var LastTotal = parseInt($("#grandTotal").text()) + parseInt($('#delivery').text());
            $("#LastTotal").html(LastTotal)
        })

    </script>

    <script>

        $(document).on("change", ".country", function (e) {
            e.preventDefault();
            var id = $(this).val();
            var token = "{{csrf_token()}}";
            $.ajax({
                url: "{{route('get-city')}}",
                method: "post",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.response == 'countries_found') {
                        $('#all_cities').html('<option selected disabled> اختر المدينة</option>');
                        data.data.countries.forEach(function (city) {
                            $('#all_cities').append('<option value=' + city.id + '>' + city.name + '</option>');
                        });
                    }
                },
                error: function (error) {

                }
            });
        })

    </script>

    <script>
        $(document).on("change", ".cities", function (e) {
            e.preventDefault();
            var id = $(this).val();
            var token = "{{csrf_token()}}";
            $.ajax({
                url: "{{route('get-delivery')}}",
                method: "post",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.response == 'City_Found') {
                        $('#delivery').html(data.data.delivery);

                        var LastTotal = parseInt($("#grandTotal").text()) + parseInt($('#delivery').text());
                        $("#LastTotal").html(LastTotal)
                    }
                }
            });

        })
    </script>

    <script>
        $(document).ready(function () {
            function calcTotal() {
                output = 0;
                $('.ttl').each(function () {
                    output += parseInt($(this).text())
                })
                return output
            }

            $("#grandTotal").text(calcTotal())
        })
    </script>

    <script>

        $(document).ready(function () {
            var LastTotal = $("#grandTotal").text() + $('#delivery').text();
            $("#LastTotal").text(LastTotal)
        })

    </script>

    <script>
        $(document).on("click", ".del", function () {
            var id = $(this).attr("cid");
            var counter = document.getElementById('CartCount');
            var table = $('#row' + id);
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('Cart-Delete') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message == 'cart_delete') {
                        table.remove();
                        counter.innerText = Number(counter.innerText) - 1;
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })

        $(document).on("click", ".activate", function (e) {
            e.preventDefault();
            var code = document.getElementById('code').value;
            var token = "{{ csrf_token() }}";
            var field1 = $(".valid");
            var field2 = $(".finish");
            var field3 = $(".invalid");
            $.ajax({
                url: "{{ route('check-coupon') }}",
                type: "post",
                dataType: "json",
                data: {code: code, _token: token},
                success: function (data) {
                    if (data.data.message == 'coupon_valid') {
                        field1.show();
                    }

                    if (data.data.message == 'coupon_finish') {
                        field2.show();
                    }

                    if (data.data.message == 'coupon_invalid') {
                        field3.show();
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })

    </script>


@stop
