@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Password-Change</title>

@stop

@section('content')

    <!-- .breadcumb-area start -->
    <div class="breadcumb-area bg-img-1 black-opacity ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap text-center">
                        <h2>تعديل كلمة المرور </h2>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- checkout-area start -->
     @include('errors.erros')
    <div class="account-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
                    <div class="account-form form-style dir">
                        <form action="{{route('pass-change')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="email" value="{{$email}}">
                        <p></p>
                        <input type="password" name="password" placeholder=" كلمة المرور الجديدة "> <p></p>
                        <input type="password" name="password_confirmation" placeholder=" تأكيد كلمة المرور الجديدة ">
                        <div class="row">
                            <div class="col-lg-6">
                            </div>

                        </div>
                        <button type="submit" style="margin-bottom: 200px"> تأكيد </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@stop