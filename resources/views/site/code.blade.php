@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Code</title>

@stop

@section('content')

    <!-- .breadcumb-area start -->
    <div class="breadcumb-area bg-img-1 black-opacity ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap text-center">
                        <h2>تأكيد الكود </h2>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success text-center"> تم إرسال رساله إلى بريدك الإلكترونى </div>
    <!-- .breadcumb-area end -->
    <!-- checkout-area start -->
     @include('errors.erros')
    <div class="account-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
                    <div class="account-form form-style dir">
                        <form action="{{route('code-confirmation')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="email" value="{{$email}}">
                        <p></p>
                        <fieldset disabled>
                            <input type="text"  id="disabledTextInput" class="form-control" placeholder="{{$email}}">
                        </fieldset>

                        <input type="text" name="code" placeholder=" الكود ">

                        <button type="submit" style="margin-bottom: 200px"> تأكيد</button>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <!-- checkout-area end -->

    @stop