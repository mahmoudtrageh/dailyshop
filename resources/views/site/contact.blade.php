@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Contact Us </title>

@stop


@section('content')

    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="{{asset('site/images/fashion/contact_01_bg.jpg')}}" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>تواصل معنا</h2>
                    <ol class="breadcrumb">
                        <li><a href="index.html">الرئيسية</a></li>
                        <li class="active">تواصل معنا</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->
    <!-- start contact section -->
    <section id="aa-contact" dir="rtl">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-contact-area">
                        <div class="aa-contact-top">
                            <h2>We are wating to assist you..</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, quos.</p>
                        </div>
                        <!-- contact map -->
                        <div class="aa-contact-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3902.3714257064535!2d-86.7550931378034!3d34.66757706940219!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8862656f8475892d%3A0xf3b1aee5313c9d4d!2sHuntsville%2C+AL+35813%2C+USA!5e0!3m2!1sen!2sbd!4v1445253385137" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <!-- Contact address -->
                        <div class="aa-contact-address">
                            <div class="row">
                                <div class="col-md-8 col-lg-push-4">
                                    <div class="aa-contact-address-left">
                                        <form class="comments-form contact-form" action="{{route('send-contact')}}" method="post">
                                            {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" name="name" placeholder="الاسم" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="email" name="email" placeholder="البريد الألكترونى" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" name="subject" placeholder="موضوع الرسالة" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" name="phone" placeholder="رقم التليفون" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <textarea class="form-control" name="message" rows="3" placeholder="الرسالة..."></textarea>
                                            </div>
                                            <button type="submit" class="aa-secondary-btn">إرسال</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-pull-8">
                                    <div class="aa-contact-address-right">
                                        <address>
                                            @foreach($contacts as $contact)
                                                <h4>التسوق اليومى</h4>
                                                <p>{{$contact->desc}}</p>
                                                <p><span class="fa fa-home"></span>{{$contact->address}}</p>
                                                <p><span class="fa fa-phone"></span>{{$contact->phone}}</p>
                                                <p><span class="fa fa-envelope"></span>{{$contact->email}}</p>
                                            @endforeach
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop