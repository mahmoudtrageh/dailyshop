<div class="aa-product-catg-body">
            <ul class="aa-product-catg">
                <!-- start single product item -->
                @foreach($section_details as $section_detail)
                    <li>
                        <figure>
                            <a class="aa-product-img" href="{{route('product.detail',['id'=>$section_detail->id])}}"><img src="{{asset('images/'.$section_detail->img)}}" alt="polo shirt img"></a>
                            <a class="aa-add-card-btn card-change" cid="{{$section_detail->id}}"><span class="class-card{{$section_detail->id}} @if(auth()->user()) @if (in_array($section_detail->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                            <figcaption>
                                <h4 class="aa-product-title"><a href="{{route('product.detail',['id'=>$section_detail->id])}}">{{$section_detail->name}}</a></h4>
                                @if(!is_null($section_detail->price_after))

                                    <del class="aa-product-price">{{$section_detail->price_before}}</del>
                                    <span class="aa-product-price">{{$section_detail->price_after}}</span>

                                @else
                                    <span class="aa-product-price">{{$section_detail->price_before}}</span>

                                @endif
                                <p class="aa-product-descrip">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam accusamus facere iusto, autem soluta amet sapiente ratione inventore nesciunt a, maxime quasi consectetur, rerum illum.</p>
                            </figcaption>
                        </figure>
                        <div class="aa-product-hvr-content">
                            <a class="wishlist" pid="{{$section_detail->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="add-class{{$section_detail->id}} @if(auth()->user()) @if (in_array($section_detail->id, $wish)) fa fa-heart @else fa fa-heart-o @endif @else fa fa-heart-o @endif"></span></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                            <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal-sect{{$section_detail->id}}"><span class="fa fa-search"></span></a>
                        </div>
                        <!-- product badge -->
                        <span class="aa-badge aa-sale" href="#">SALE!</span>
                    </li>
                @endforeach
            </ul>

            @foreach($section_details as $section_detail)

                <!-- quick view modal -->
                    <div class="modal fade" id="quick-view-modal-sect{{$section_detail->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <div class="row">
                                        <!-- Modal view slider -->
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="aa-product-view-slider">
                                                <div class="simpleLens-gallery-container" id="demo-1-n{{$section_detail->id}}">
                                                    <div class="simpleLens-container">
                                                        <div class="simpleLens-big-image-container">
                                                            <a class="simpleLens-lens-image" data-lens-image="{{asset('images/' . $section_detail->img)}}">
                                                                <img src="{{asset('images/' . $section_detail->img)}}" class="simpleLens-big-image">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="simpleLens-thumbnails-container">
                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                           data-lens-image="{{asset('images/' . $section_detail->img2)}}"
                                                           data-big-image="{{asset('images/' . $section_detail->img2)}}">
                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $section_detail->img2)}}">
                                                        </a>
                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                           data-lens-image="{{asset('images/' . $section_detail->img)}}"
                                                           data-big-image="{{asset('images/' . $section_detail->img)}}">
                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $section_detail->img)}}">
                                                        </a>

                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                           data-lens-image="{{asset('images/' . $section_detail->img2)}}"
                                                           data-big-image="{{asset('images/' . $section_detail->img2)}}">
                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $section_detail->img2)}}">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal view content -->
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="aa-product-view-content">
                                                <h3>{{$section_detail->name}}</h3>
                                                <div class="aa-price-block">
                                                    @if(!is_null($section_detail->price_after))

                                                        <del class="aa-product-price">{{$section_detail->price_before}}</del>
                                                        <span class="aa-product-price">{{$section_detail->price_after}}</span>

                                                    @else
                                                        <span class="aa-product-price">{{$section_detail->price_before}}</span>

                                                    @endif
                                                    <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                                                </div>
                                                <p>{{$section_detail->description}}</p>
                                                <h4>Size</h4>
                                                <div class="aa-prod-view-size">
                                                    <a href="#">S</a>
                                                    <a href="#">M</a>
                                                    <a href="#">L</a>
                                                    <a href="#">XL</a>
                                                </div>
                                                <div class="aa-prod-quantity">
                                                    <form action="">
                                                        <select name="" id="">
                                                            <option value="0" selected="1">1</option>
                                                            <option value="1">2</option>
                                                            <option value="2">3</option>
                                                            <option value="3">4</option>
                                                            <option value="4">5</option>
                                                            <option value="5">6</option>
                                                        </select>
                                                    </form>
                                                    <p class="aa-prod-category">
                                                        القسم: <a href="#">{{$section_detail->section->name}}</a>
                                                    </p>
                                                </div>
                                                <div class="aa-prod-view-bottom">
                                                    {{--<a class="aa-add-card-btn card-change" cid="{{$section_detail->id}}"><span class="class-card{{$section_detail->id}} @if(auth()->user()) @if (in_array($section_detail->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>--}}
                                                    <a href="{{route('product.detail',['id'=>$section_detail->id])}}" class="aa-add-to-cart-btn">أظهر التفاصيل</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- / quick view modal -->

            @endforeach
        </div>


@section('js')

    <script>
        $(document).on("click", ".card-change", function (e) {
            e.preventDefault();
            var id = $(this).attr("cid");
            var field = $(".class-card" + id);
            var counter = document.getElementById('CartCount');
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('Cart-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message === 'user_not_logged_in') {
                        $("#login-modal").modal('show');
                    }
                    if (data.data.message === 'order_added') {
                        counter.innerText = Number(counter.innerText) + 1;
                        field.addClass('card-color');
                    }

                    if (data.data.message === 'order_deleted') {
                        counter.innerText = Number(counter.innerText) - 1;
                        field.removeClass('card-color');
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })


        $(document).on("click", ".wishlist", function (e) {
            e.preventDefault();
            var id = $(this).attr("pid");
            var field = $(".add-class" + id);
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('wish-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token,},

                success: function (data) {
                    if ((data.data.message === 'user_not_logged_in')) {
                        // console.log(data.data.message);
                        $("#login-modal").modal('show');
                    }
                    if (data.data.message === 'wish_added') {
                        field.removeClass('fa fa-heart-o');
                        field.addClass('fa fa-heart');
                    }
                    if (data.data.message === 'wish_deleted') {
                        field.removeClass('fa fa-heart');
                        field.addClass('fa fa-heart-o');

                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })

    @stop
