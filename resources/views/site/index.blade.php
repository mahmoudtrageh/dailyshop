@extends('layouts.Site-Layout')


@section('title')

    <title> Daily Shop | Home</title>

@stop

@section('content')

    <!-- Start slider -->
    <section id="aa-slider">
        <div class="aa-slider-area">
            <div id="sequence" class="seq">
                <div class="seq-screen">
                    <ul class="seq-canvas">
                        <!-- single slide item -->
                        @foreach($sliders as $slider)
                            <li>
                                <div class="seq-model">
                                    <img data-seq src="{{asset('images/' . $slider->img)}}" alt="Men slide img" />
                                </div>
                                <div class="seq-title">
                                    <span data-seq>Save Up to 75% Off</span>
                                    <h2 data-seq>Best Collection</h2>
                                    <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                                    <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- slider navigation btn -->
                <fieldset class="seq-nav" aria-controls="sequence" aria-label="Slider buttons">
                    <a type="button" class="seq-prev" aria-label="Previous"><span class="fa fa-angle-left"></span></a>
                    <a type="button" class="seq-next" aria-label="Next"><span class="fa fa-angle-right"></span></a>
                </fieldset>
            </div>
        </div>
    </section>
    <!-- / slider -->

    <!-- Products section -->
    <section id="aa-product">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="aa-product-area">
                            <div class="aa-product-inner">
                                <!-- start prduct navigation -->
                                <ul class="nav nav-tabs aa-products-tab">
                                    <!-- <li class="active"><a href="#men" data-toggle="tab">Men</a></li> -->
                                    @foreach($sections as $i=>$section)
                                    <li class="{{$i == 0 ? 'active' : ''}}"><a href="#women{{$section->id}}" id="section-tab" data-toggle="tab" href="#woman{{$section->id}}" role="tab"
                                                                               aria-controls="flg1" aria-selected="true">{{$section->name}}</a></li>
                                    @endforeach
                                    <!-- <li><a href="#sports" data-toggle="tab">Sports</a></li> -->

                                    <!-- <li><a href="#electronics" data-toggle="tab">Electronics</a></li> -->
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">

                                    <!-- start women product category -->
                                    @foreach($sections as $i=>$section)

                                    <div class="tab-pane fade in {{$i == 0 ? 'active' : ''}}" id="women{{$section->id}}" role="tabpanel" aria-labelledby="section-tab">
                                        <ul class="aa-product-catg">
                                            <!-- start single product item -->
                                                @foreach($section->product as $sec_product)
                                            <li>
                                                <figure>
                                                    <a class="aa-product-img" href="{{route('product.detail',['id'=>$sec_product->id])}}"><img src="{{asset('images/'.$sec_product->img)}}" alt="polo shirt img"></a>
                                                    <a class="aa-add-card-btn card-change" cid="{{$sec_product->id}}"><span class="class-card{{$sec_product->id}} @if(auth()->user()) @if (in_array($sec_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a href="{{route('product.detail',['id'=>$sec_product->id])}}">{{$sec_product->name}}</a></h4>
                                                        @if(!is_null($sec_product->price_after))

                                                            <del class="aa-product-price">{{$sec_product->price_before}}</del>
                                                            <span class="aa-product-price">{{$sec_product->price_after}}</span>

                                                        @else
                                                            <span class="aa-product-price">{{$sec_product->price_before}}</span>

                                                        @endif
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                    <a class="wishlist" pid="{{$sec_product->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="add-class{{$sec_product->id}} @if(auth()->user()) @if (in_array($sec_product->id, $wish)) fa fa-heart @else fa fa-heart-o @endif @else fa fa-heart-o @endif"></span></a>
                                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                                                    <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal-sec{{$sec_product->id}}"><span class="fa fa-search"></span></a>
                                                </div>
                                                <!-- product badge -->
                                                <span class="aa-badge aa-sale" href="#">SALE!</span>
                                            </li>

                                                <!-- quick view modal -->
                                                    <div class="modal fade" id="quick-view-modal-sec{{$sec_product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <div class="row">
                                                                        <!-- Modal view slider -->
                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                            <div class="aa-product-view-slider">
                                                                                <div class="simpleLens-gallery-container" id="demo-1-sec{{$sec_product->id}}">
                                                                                    <div class="simpleLens-container">
                                                                                        <div class="simpleLens-big-image-container">
                                                                                            <a class="simpleLens-lens-image" data-lens-image="{{asset('images/' . $sec_product->img)}}">
                                                                                                <img src="{{asset('images/' . $sec_product->img)}}" class="simpleLens-big-image">
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="simpleLens-thumbnails-container">
                                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                           data-lens-image="{{asset('images/' . $sec_product->img2)}}"
                                                                                           data-big-image="{{asset('images/' . $sec_product->img2)}}">
                                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $sec_product->img2)}}">
                                                                                        </a>
                                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                           data-lens-image="{{asset('images/' . $sec_product->img)}}"
                                                                                           data-big-image="{{asset('images/' . $sec_product->img)}}">
                                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $sec_product->img)}}">
                                                                                        </a>

                                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                           data-lens-image="{{asset('images/' . $sec_product->img2)}}"
                                                                                           data-big-image="{{asset('images/' . $sec_product->img2)}}">
                                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $sec_product->img2)}}">
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- Modal view content -->
                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                            <div class="aa-product-view-content">
                                                                                <h3>{{$sec_product->name}}</h3>
                                                                                <div class="aa-price-block">
                                                                                    @if(!is_null($sec_product->price_after))

                                                                                        <del class="aa-product-price">{{$sec_product->price_before}}</del>
                                                                                        <span class="aa-product-price">{{$sec_product->price_after}}</span>

                                                                                    @else
                                                                                        <span class="aa-product-price">{{$sec_product->price_before}}</span>

                                                                                    @endif
                                                                                    <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                                                                                </div>
                                                                                <p>{{$sec_product->description}}</p>
                                                                                <h4>Size</h4>
                                                                                <div class="aa-prod-view-size">
                                                                                    <a href="#">S</a>
                                                                                    <a href="#">M</a>
                                                                                    <a href="#">L</a>
                                                                                    <a href="#">XL</a>
                                                                                </div>
                                                                                <div class="aa-prod-quantity">
                                                                                    <form action="">
                                                                                        <select name="" id="">
                                                                                            <option value="0" selected="1">1</option>
                                                                                            <option value="1">2</option>
                                                                                            <option value="2">3</option>
                                                                                            <option value="3">4</option>
                                                                                            <option value="4">5</option>
                                                                                            <option value="5">6</option>
                                                                                        </select>
                                                                                    </form>
                                                                                    <p class="aa-prod-category">
                                                                                        القسم: <a href="#">{{$sec_product->section->name}}</a>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="aa-prod-view-bottom">
                                                                                    <a class="aa-add-card-btn card-change" cid="{{$sec_product->id}}"><span class="class-card{{$sec_product->id}} @if(auth()->user()) @if (in_array($sec_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                                                    <a href="{{route('product.detail',['id'=>$sec_product->id])}}" class="aa-add-to-cart-btn">أظهر التفاصيل</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- / quick view modal -->

                                            @endforeach
                                        </ul>
                                        <a class="aa-browse-btn"  href="{{route('site-shop',['id'=>$section->id])}}">{{$section->name}} | الجميع<span class="fa fa-long-arrow-left"></span></a>
                                    </div>
                                    <!-- / women product category -->
                                    @endforeach


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / Products section -->

    <!-- banner section -->
    <section id="aa-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="aa-banner-area">
                            @foreach($banarat as $key => $banar)
                                @if($key == 0)
                                    <a href="#"><img src="{{asset('images/' . $banar->img1)}}" alt="fashion banner img"></a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- popular section -->

    <!-- popular section -->
    <section id="aa-popular-category">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="aa-popular-category-area">
                            <!-- start prduct navigation -->
                            <ul class="nav nav-tabs aa-products-tab">
                                <li class="active"><a href="#popular" data-toggle="tab">الشائعة</a></li>
                                <li><a href="#featured" data-toggle="tab">المتميزة</a></li>
                                <li><a href="#latest" data-toggle="tab">الاخيرة</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Start men popular category -->
                                <div class="tab-pane fade in active" id="popular">
                                    <ul class="aa-product-catg aa-popular-slider">
                                        <!-- start single product item -->
                                        @foreach($popular_products as $popular_product)
                                        <li>
                                            <figure>
                                                <a class="aa-product-img" href="{{route('product.detail',['id'=>$popular_product->id])}}"><img src="{{asset('images/'.$popular_product->img)}}" alt="polo shirt img"></a>
                                                <a class="aa-add-card-btn card-change" cid="{{$popular_product->id}}"><span class="class-card{{$popular_product->id}} @if(auth()->user()) @if (in_array($popular_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                <figcaption>
                                                    <h4 class="aa-product-title"><a href="{{route('product.detail',['id'=>$popular_product->id])}}">{{$popular_product->name}}</a></h4>
                                                    @if(!is_null($popular_product->price_after))

                                                        <del class="aa-product-price">{{$popular_product->price_before}}</del>
                                                        <span class="aa-product-price">{{$popular_product->price_after}}</span>

                                                    @else
                                                        <span class="aa-product-price">{{$popular_product->price_before}}</span>

                                                    @endif
                                                </figcaption>
                                            </figure>
                                            <div class="aa-product-hvr-content">
                                                <a class="wishlist" pid="{{$popular_product->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="add-class{{$popular_product->id}} @if(auth()->user()) @if (in_array($popular_product->id, $wish)) fa fa-heart @else fa fa-heart-o @endif @else fa fa-heart-o @endif"></span></a>
                                                <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                                                <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal-p{{$popular_product->id}}"><span class="fa fa-search"></span></a>
                                            </div>
                                            <!-- product badge -->
                                            <span class="aa-badge aa-sale" href="#">SALE!</span>
                                        </li>


                                        @endforeach
                                    </ul>

                                    @foreach($popular_products as $popular_product)

                                    <!-- quick view modal -->
                                    <div class="modal fade" id="quick-view-modal-p{{$popular_product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <div class="row">
                                                        <!-- Modal view slider -->
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="aa-product-view-slider">
                                                                <div class="simpleLens-gallery-container" id="demo-1-p{{$popular_product->id}}">
                                                                    <div class="simpleLens-container">
                                                                        <div class="simpleLens-big-image-container">
                                                                            <a class="simpleLens-lens-image" data-lens-image="{{asset('images/' . $popular_product->img)}}">
                                                                                <img src="{{asset('images/' . $popular_product->img)}}" class="simpleLens-big-image">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="simpleLens-thumbnails-container">
                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                           data-lens-image="{{asset('images/' . $popular_product->img2)}}"
                                                                           data-big-image="{{asset('images/' . $popular_product->img2)}}">
                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $popular_product->img2)}}">
                                                                        </a>
                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                           data-lens-image="{{asset('images/' . $popular_product->img)}}"
                                                                           data-big-image="{{asset('images/' . $popular_product->img)}}">
                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $popular_product->img)}}">
                                                                        </a>

                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                           data-lens-image="{{asset('images/' . $popular_product->img2)}}"
                                                                           data-big-image="{{asset('images/' . $popular_product->img2)}}">
                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $popular_product->img2)}}">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modal view content -->
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="aa-product-view-content">
                                                                <h3>{{$popular_product->name}}</h3>
                                                                <div class="aa-price-block">
                                                                    @if(!is_null($popular_product->price_after))

                                                                        <del class="aa-product-price">{{$popular_product->price_before}}</del>
                                                                        <span class="aa-product-price">{{$popular_product->price_after}}</span>

                                                                    @else
                                                                        <span class="aa-product-price">{{$popular_product->price_before}}</span>

                                                                    @endif
                                                                    <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                                                                </div>
                                                                <p>{{$popular_product->description}}</p>
                                                                <h4>Size</h4>
                                                                <div class="aa-prod-view-size">
                                                                    <a href="#">S</a>
                                                                    <a href="#">M</a>
                                                                    <a href="#">L</a>
                                                                    <a href="#">XL</a>
                                                                </div>
                                                                <div class="aa-prod-quantity">
                                                                    <form action="">
                                                                        <select name="" id="">
                                                                            <option value="0" selected="1">1</option>
                                                                            <option value="1">2</option>
                                                                            <option value="2">3</option>
                                                                            <option value="3">4</option>
                                                                            <option value="4">5</option>
                                                                            <option value="5">6</option>
                                                                        </select>
                                                                    </form>
                                                                    <p class="aa-prod-category">
                                                                        القسم: <a href="#">{{$popular_product->section->name}}</a>
                                                                    </p>
                                                                </div>
                                                                <div class="aa-prod-view-bottom">
                                                                    <a class="aa-add-card-btn card-change" cid="{{$popular_product->id}}"><span class="class-card{{$popular_product->id}} @if(auth()->user()) @if (in_array($popular_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                                    <a href="{{route('product.detail',['id'=>$popular_product->id])}}" class="aa-add-to-cart-btn">أظهر التفاصيل</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- / quick view modal -->

                                    @endforeach
                                    <a class="aa-browse-btn" href="{{route('site-shop')}}">شاهدى الكل <span class="fa fa-long-arrow-left"></span></a>
                                </div>
                                <!-- / popular product category -->

                                <!-- start featured product category -->
                                <div class="tab-pane fade" id="featured">
                                    <ul class="aa-product-catg aa-featured-slider">
                                        <!-- start single product item -->
                                        @foreach($special_Products as $special_product)
                                            <li>
                                                <figure>
                                                    <a class="aa-product-img" href="{{route('product.detail',['id'=>$special_product->id])}}"><img src="{{asset('images/'.$special_product->img)}}" alt="polo shirt img"></a>
                                                    <a class="aa-add-card-btn card-change" cid="{{$special_product->id}}"><span class="class-card{{$special_product->id}} @if(auth()->user()) @if (in_array($special_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a href="{{route('product.detail',['id'=>$special_product->id])}}">{{$special_product->name}}</a></h4>
                                                        @if(!is_null($special_product->price_after))

                                                            <del class="aa-product-price">{{$special_product->price_before}}</del>
                                                            <span class="aa-product-price">{{$special_product->price_after}}</span>

                                                        @else
                                                            <span class="aa-product-price">{{$special_product->price_before}}</span>

                                                        @endif
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                    <a class="wishlist" pid="{{$special_product->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="add-class{{$special_product->id}} @if(auth()->user()) @if (in_array($special_product->id, $wish)) fa fa-heart @else fa fa-heart-o @endif @else fa fa-heart-o @endif"></span></a>
                                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                                                    <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal-s{{$special_product->id}}"><span class="fa fa-search"></span></a>
                                                </div>
                                                <!-- product badge -->
                                                <span class="aa-badge aa-sale" href="#">SALE!</span>
                                            </li>

                                        @endforeach
                                    </ul>

                                    @foreach($special_Products as $special_product)

                                    <!-- quick view modal -->
                                    <div class="modal fade" id="quick-view-modal-s{{$special_product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <div class="row">
                                                        <!-- Modal view slider -->
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="aa-product-view-slider">
                                                                <div class="simpleLens-gallery-container" id="demo-1-s{{$special_product->id}}">
                                                                    <div class="simpleLens-container">
                                                                        <div class="simpleLens-big-image-container">
                                                                            <a class="simpleLens-lens-image" data-lens-image="{{asset('images/' . $special_product->img)}}">
                                                                                <img src="{{asset('images/' . $special_product->img)}}" class="simpleLens-big-image">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="simpleLens-thumbnails-container">
                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                           data-lens-image="{{asset('images/' . $special_product->img2)}}"
                                                                           data-big-image="{{asset('images/' . $special_product->img2)}}">
                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $special_product->img2)}}">
                                                                        </a>
                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                           data-lens-image="{{asset('images/' . $special_product->img)}}"
                                                                           data-big-image="{{asset('images/' . $special_product->img)}}">
                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $special_product->img)}}">
                                                                        </a>

                                                                        <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                           data-lens-image="{{asset('images/' . $special_product->img2)}}"
                                                                           data-big-image="{{asset('images/' . $special_product->img2)}}">
                                                                            <img style="width:25px;height:25px;" src="{{asset('images/' . $special_product->img2)}}">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modal view content -->
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="aa-product-view-content">
                                                                <h3>{{$special_product->name}}</h3>
                                                                <div class="aa-price-block">
                                                                    @if(!is_null($special_product->price_after))

                                                                        <del class="aa-product-price">{{$special_product->price_before}}</del>
                                                                        <span class="aa-product-price">{{$special_product->price_after}}</span>

                                                                    @else
                                                                        <span class="aa-product-price">{{$special_product->price_before}}</span>

                                                                    @endif
                                                                    <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                                                                </div>
                                                                <p>{{$special_product->description}}</p>
                                                                <h4>Size</h4>
                                                                <div class="aa-prod-view-size">
                                                                    <a href="#">S</a>
                                                                    <a href="#">M</a>
                                                                    <a href="#">L</a>
                                                                    <a href="#">XL</a>
                                                                </div>
                                                                <div class="aa-prod-quantity">
                                                                    <form action="">
                                                                        <select name="" id="">
                                                                            <option value="0" selected="1">1</option>
                                                                            <option value="1">2</option>
                                                                            <option value="2">3</option>
                                                                            <option value="3">4</option>
                                                                            <option value="4">5</option>
                                                                            <option value="5">6</option>
                                                                        </select>
                                                                    </form>
                                                                    <p class="aa-prod-category">
                                                                        القسم: <a href="#">{{$special_product->section->name}}</a>
                                                                    </p>
                                                                </div>
                                                                <div class="aa-prod-view-bottom">
                                                                    <a class="aa-add-card-btn card-change" cid="{{$special_product->id}}"><span class="class-card{{$special_product->id}} @if(auth()->user()) @if (in_array($special_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                                    <a href="{{route('product.detail',['id'=>$special_product->id])}}" class="aa-add-to-cart-btn">أظهر التفاصيل</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- / quick view modal -->
                                    @endforeach
                                    <a class="aa-browse-btn" href="{{route('site-shop')}}">شاهدى الكل <span class="fa fa-long-arrow-left"></span></a>
                                </div>
                                <!-- / featured product category -->

                                <!-- start latest product category -->
                                <div class="tab-pane fade" id="latest">
                                    <ul class="aa-product-catg aa-latest-slider">
                                        <!-- start single product item -->
                                        @foreach($new_products as $new_product)
                                            <li>
                                                <figure>
                                                    <a class="aa-product-img" href="{{route('product.detail',['id'=>$new_product->id])}}"><img src="{{asset('images/'.$new_product->img)}}" alt="polo shirt img"></a>
                                                    <a class="aa-add-card-btn card-change" cid="{{$new_product->id}}"><span class="class-card{{$new_product->id}} @if(auth()->user()) @if (in_array($new_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a href="{{route('product.detail',['id'=>$new_product->id])}}">{{$new_product->name}}</a></h4>
                                                        @if(!is_null($new_product->price_after))

                                                            <del class="aa-product-price">{{$new_product->price_before}}</del>
                                                            <span class="aa-product-price">{{$new_product->price_after}}</span>

                                                        @else
                                                            <span class="aa-product-price">{{$new_product->price_before}}</span>

                                                        @endif
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                    <a class="wishlist" pid="{{$new_product->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="add-class{{$new_product->id}} @if(auth()->user()) @if (in_array($new_product->id, $wish)) fa fa-heart @else fa fa-heart-o @endif @else fa fa-heart-o @endif"></span></a>
                                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                                                    <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal-n{{$new_product->id}}"><span class="fa fa-search"></span></a>
                                                </div>
                                                <!-- product badge -->
                                                <span class="aa-badge aa-sale" href="#">SALE!</span>
                                            </li>

                                        @endforeach

                                    </ul>

                                @foreach($new_products as $new_product)

                                    <!-- quick view modal -->
                                        <div class="modal fade" id="quick-view-modal-n{{$new_product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <div class="row">
                                                            <!-- Modal view slider -->
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="aa-product-view-slider">
                                                                    <div class="simpleLens-gallery-container" id="demo-1-n{{$new_product->id}}">
                                                                        <div class="simpleLens-container">
                                                                            <div class="simpleLens-big-image-container">
                                                                                <a class="simpleLens-lens-image" data-lens-image="{{asset('images/' . $new_product->img)}}">
                                                                                    <img src="{{asset('images/' . $new_product->img)}}" class="simpleLens-big-image">
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="simpleLens-thumbnails-container">
                                                                            <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                               data-lens-image="{{asset('images/' . $new_product->img2)}}"
                                                                               data-big-image="{{asset('images/' . $new_product->img2)}}">
                                                                                <img style="width:25px;height:25px;" src="{{asset('images/' . $new_product->img2)}}">
                                                                            </a>
                                                                            <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                               data-lens-image="{{asset('images/' . $new_product->img)}}"
                                                                               data-big-image="{{asset('images/' . $new_product->img)}}">
                                                                                <img style="width:25px;height:25px;" src="{{asset('images/' . $new_product->img)}}">
                                                                            </a>

                                                                            <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                               data-lens-image="{{asset('images/' . $new_product->img2)}}"
                                                                               data-big-image="{{asset('images/' . $new_product->img2)}}">
                                                                                <img style="width:25px;height:25px;" src="{{asset('images/' . $new_product->img2)}}">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Modal view content -->
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="aa-product-view-content">
                                                                    <h3>{{$new_product->name}}</h3>
                                                                    <div class="aa-price-block">
                                                                        @if(!is_null($new_product->price_after))

                                                                            <del class="aa-product-price">{{$new_product->price_before}}</del>
                                                                            <span class="aa-product-price">{{$new_product->price_after}}</span>

                                                                        @else
                                                                            <span class="aa-product-price">{{$new_product->price_before}}</span>

                                                                        @endif
                                                                        <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                                                                    </div>
                                                                    <p>{{$new_product->description}}</p>
                                                                    <h4>Size</h4>
                                                                    <div class="aa-prod-view-size">
                                                                        <a href="#">S</a>
                                                                        <a href="#">M</a>
                                                                        <a href="#">L</a>
                                                                        <a href="#">XL</a>
                                                                    </div>
                                                                    <div class="aa-prod-quantity">
                                                                        <form action="">
                                                                            <select name="" id="">
                                                                                <option value="0" selected="1">1</option>
                                                                                <option value="1">2</option>
                                                                                <option value="2">3</option>
                                                                                <option value="3">4</option>
                                                                                <option value="4">5</option>
                                                                                <option value="5">6</option>
                                                                            </select>
                                                                        </form>
                                                                        <p class="aa-prod-category">
                                                                            القسم: <a href="#">{{$new_product->section->name}}</a>
                                                                        </p>
                                                                    </div>
                                                                    <div class="aa-prod-view-bottom">
                                                                        <a class="aa-add-card-btn card-change" cid="{{$new_product->id}}"><span class="class-card{{$new_product->id}} @if(auth()->user()) @if (in_array($new_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                                        <a href="{{route('product.detail',['id'=>$new_product->id])}}" class="aa-add-to-cart-btn">أظهر التفاصيل</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- / quick view modal -->

                                    @endforeach

                                    <a class="aa-browse-btn" href="{{route('site-shop')}}">شاهدى الكل <span class="fa fa-long-arrow-left"></span></a>
                                </div>
                                <!-- / latest product category -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / popular section -->
@stop

@section('js')

    <script>
        $(document).on("click", ".card-change", function (e) {
            e.preventDefault();
            var id = $(this).attr("cid");
            var field = $(".class-card" + id);
            var counter = document.getElementById('CartCount');
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('Cart-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message === 'user_not_logged_in') {
                        $("#login-modal").modal('show');
                    }
                    if (data.data.message === 'order_added') {
                        counter.innerText = Number(counter.innerText) + 1;
                        field.addClass('card-color');
                    }

                    if (data.data.message === 'order_deleted') {
                        counter.innerText = Number(counter.innerText) - 1;
                        field.removeClass('card-color');
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })


        $(document).on("click", ".wishlist", function (e) {
            e.preventDefault();
            var id = $(this).attr("pid");
            var field = $(".add-class" + id);
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('wish-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token,},

                success: function (data) {
                    if ((data.data.message === 'user_not_logged_in')) {
                        // console.log(data.data.message);
                        $("#login-modal").modal('show');
                    }
                    if (data.data.message === 'wish_added') {
                        field.removeClass('fa fa-heart-o');
                        field.addClass('fa fa-heart');
                    }
                    if (data.data.message === 'wish_deleted') {
                        field.removeClass('fa fa-heart');
                        field.addClass('fa fa-heart-o');

                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })

        $(document).on("click", ".del", function () {
            var id = $(this).attr("cid");
            var counter = document.getElementById('CartCount');
            var table = $('#row' + id);
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('Cart-Delete') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message == 'cart_delete') {
                        table.remove();
                        counter.innerText = Number(counter.innerText) - 1;
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })

    </script>

@stop