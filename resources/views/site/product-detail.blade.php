@extends('layouts.Site-Layout')


@section('title')

    <title> Tohfa | Home</title>

@stop

@section('content')

    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="{{asset('site/images/fashion/SHOP FULL COLLECTION@1x.jpg')}}" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>{{$section->name}}</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                        <li class="active">{{$section->name}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <!-- product category -->
    <section id="aa-product-details">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-product-details-area">
                        <div class="aa-product-details-content"  dir="rtl">
                            <div class="row">
                                <!-- Modal view slider -->
                                <div class="col-md-5 col-sm-5 col-xs-12 col-lg-push-7">
                                    <div class="aa-product-view-slider">
                                        <div id="demo-1" class="simpleLens-gallery-container">
                                            @foreach($product_details as $product_detail)
                                            <div class="simpleLens-container">
                                                <div class="simpleLens-big-image-container"><a data-lens-image="{{asset('images/' . $product_detail->img)}}" class="simpleLens-lens-image"><img src="{{asset('images/' . $product_detail->img)}}" class="simpleLens-big-image"></a></div>
                                            </div>
                                            <div class="simpleLens-thumbnails-container">
                                                <a data-big-image="{{asset('images/' . $product_detail->img2)}}" data-lens-image="{{asset('images/' . $product_detail->img2)}}" class="simpleLens-thumbnail-wrapper" href="#">
                                                    <img style="width: 25px;height:25px;" src="{{asset('images/' . $product_detail->img2)}}">
                                                </a>
                                                <a data-big-image="{{asset('images/' . $product_detail->img)}}" data-lens-image="{{asset('images/' . $product_detail->img)}}" class="simpleLens-thumbnail-wrapper" href="#">
                                                    <img style="width: 25px;height:25px;" src="{{asset('images/' . $product_detail->img)}}">
                                                </a>
                                                <a data-big-image="{{asset('images/' . $product_detail->img2)}}" data-lens-image="{{asset('images/' . $product_detail->img2)}}" class="simpleLens-thumbnail-wrapper" href="#">
                                                    <img style="width: 25px;height:25px;" src="{{asset('images/' . $product_detail->img2)}}">
                                                </a>
                                            </div>
                                              @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal view content -->
                                @foreach($product_details as $product_detail)
                                    <div class="col-md-7 col-sm-7 col-xs-12 col-lg-pull-5">
                                        <div class="aa-product-view-content">
                                            <h3>{{$product_detail->name}}</h3>
                                            <div class="aa-price-block">
                                                <span class="aa-product-view-price">{{$product_detail->price_before}}</span>
                                                <!-- <p class="aa-product-avilability">Avilability: <span>In stock</span></p> -->
                                            </div>
                                            <p>{{$product_detail->description}}</p>
                                            <h4>اللون</h4>
                                            <div class="aa-color-tag">
                                                    <ul id="ul_catalog_color" class="list-unstyled catalog_filter_ul color-group" style="display: flex;">
                                                    @foreach($product_colors as $i=>$product_color)
                                                        @if($i < 5)
                                                            <li class="advanced-filter" data-handle="white">
                                                                <a class="catalog_color">
                                                                    <div class="contain-color" style="background:{{$product_color->class}};width:30px;height:30px;"></div>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                                    <ul id="ul_catalog_color" class="list-unstyled catalog_filter_ul color-group" style="display: flex;">
                                                    @foreach($product_colors as $i=>$product_color)
                                                        @if($i >= 5)
                                                            <li class="advanced-filter" data-handle="white">
                                                                <a class="catalog_color">
                                                                    <div class="contain-color" style="background-color:{{$product_color->class}};width:30px;height:30px;"></div>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="aa-prod-quantity">
                                                <p class="aa-prod-category">
                                                    القسم: <a href="#">{{$section->name}}</a>
                                                </p>

                                            </div>
                                            <div class="aa-prod-view-bottom">
                                                <a  cid="{{$product_detail->id}}" class="aa-add-to-cart-btn card-change class-card{{$product_detail->id}} @if(auth()->user()) @if (in_array($product_detail->id, $cart)) card-color @endif @endif" href="#">أضف إلى العربة</a>
                                                <a  pid="{{$product_detail->id}}" class="aa-add-to-cart-btn wishlist add-class{{$product_detail->id}} @if(auth()->user()) @if (in_array($product_detail->id, $wish)) card-color @endif @endif" href="#">المفضلة</a>
                                                <a class="aa-add-to-cart-btn" href="#">قارنى</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="container">
                            <div class="row" dir="rtl">
                                @if(auth()->user())
                                <form action="{{route('add-size')}}" method="POST">
                                    {{csrf_field()}}
                                    <div class="take-size" style="padding:20px;">
                                    <h3 style="color:#ff6666;">المقاسات بالسنتيمتر</h3>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <img style="padding-top:25px;" src="{{asset('site/images/372331women.gif')}}" >
                                        <br>
                                        <!-- <button type="button" class="btn btn-outline-primary">Primary</button> -->
                                    </div>


                                        <input name="user_id" type="hidden" class="form-control" value="{{auth()->user()->id}}">


                                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">محيط الكم من اعلى</label>
                                            <input name="kom_height" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">طول الظهر</label>
                                            <input name="back_length" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">طول الصدر إلى الوسط</label>
                                            <input name="chest_to_center" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">الطول من الوسط إلى الركبة</label>
                                            <input name="center_to_leg" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>

                                        <div class="form-group">
                                            <label for="formGroupExampleInput">محيط الأسورة</label>
                                            <input name="eswra_volume" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">الطول الكلى للجسم</label>
                                            <input name="total_length" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>

                                    </div>

                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">محيط الصدر</label>
                                            <input name="chest_volume" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">محيط الوسط</label>
                                            <input name="center_volume" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">محيط أكبر حجم</label>
                                            <input name="highest_volume" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">عرض الظهر</label>
                                            <input name="back_width" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">طول الكتف</label>
                                            <input name="shoulder_length" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">طول الكم</label>
                                            <input name="kom_length" type="float" class="form-control" id="formGroupExampleInput">
                                        </div>
                                    </div>

                                </div>

                                    <button type="submit" style="margin-top:80px;width:10%;background:#ff6666;color: white;padding: 5px 0;border-radius: 15px; ">إرسال</button>

                                </form>

                                    @else

                                    <span class="alert alert-danger" style="display: block;">برجاء تسجيل الدخول لإرسال المقاسات الخاصة بكي</span>

                                @endif



                            </div>
                        </div>


                        <!-- Related product -->
                        <div class="aa-product-related-item">
                            <h3>المنتجات المتعلقة</h3>
                            <ul class="aa-product-catg aa-related-item-slider">
                                <!-- start single product item -->
                                @foreach($related_products as $related_product)
                                    <li>
                                        <figure>
                                            <a class="aa-product-img" href="{{route('product.detail',['id'=>$related_product->id])}}"><img src="{{asset('images/' . $related_product->img)}}" alt="polo shirt img"></a>
                                            <a class="aa-add-card-btn card-change" cid="{{$related_product->id}}"><span class="class-card{{$related_product->id}} @if(auth()->user()) @if (in_array($related_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                            <figcaption>
                                                <h4 class="aa-product-title"><a href="{{route('product.detail',['id'=>$related_product->id])}}">{{$related_product->name}}</a></h4>

                                                @if(!is_null($related_product->price_after))

                                                    <del class="aa-product-price">{{$related_product->price_before}}</del>
                                                    <span class="aa-product-price">{{$related_product->price_after}}</span>

                                                @else
                                                    <span class="aa-product-price">{{$related_product->price_before}}</span>

                                                @endif

                                            </figcaption>
                                        </figure>
                                        <div class="aa-product-hvr-content">
                                            <a class="wishlist-related" pid="{{$related_product->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="add-class{{$related_product->id}} @if(auth()->user()) @if (in_array($related_product->id, $wish)) fa fa-heart @else fa fa-heart-o @endif @else fa fa-heart-o @endif"></span></a>
                                            <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                                            <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>
                                        </div>
                                        <!-- product badge -->
                                        <span class="aa-badge aa-sale" href="#">SALE!</span>
                                    </li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / product category -->



@stop

@section('js')

    <script>

        $(document).on("click", ".card-change", function (e) {
            e.preventDefault();
            var id = $(this).attr("cid");
            var field = $(".class-card" + id);
            var counter = document.getElementById('CartCount');
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('Cart-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message === 'user_not_logged_in') {
                        $('#login-modal').modal('show');
                    }
                    if (data.data.message === 'order_added') {
                        counter.innerText = Number(counter.innerText) + 1;
                        field.addClass('card-color');
                    }

                    if (data.data.message === 'order_deleted') {
                        counter.innerText = Number(counter.innerText) - 1;
                        field.removeClass('card-color');
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })


        $(document).on("click", ".wishlist", function (e) {
            e.preventDefault();
            var id = $(this).attr("pid");
            var token = "{{ csrf_token() }}";
            var field = $(".add-class" + id);

            $.ajax({
                url: "{{ route('wish-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},

                success: function (data) {
                    if ((data.data.message === 'user_not_logged_in')) {
                        $('#login-modal').modal('show');
                    }
                    if (data.data.message === 'wish_added') {
                        field.addClass('card-color');
                    }
                    if (data.data.message === 'wish_deleted') {
                        field.removeClass('card-color');
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })

        $(document).on("click", ".wishlist-related", function (e) {
            e.preventDefault();
            var id = $(this).attr("pid");
            var field = $(".add-class" + id);
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('wish-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token,},

                success: function (data) {
                    if ((data.data.message === 'user_not_logged_in')) {
                        // console.log(data.data.message);
                        $("#login-modal").modal('show');
                    }
                    if (data.data.message === 'wish_added') {
                        field.removeClass('fa fa-heart-o');
                        field.addClass('fa fa-heart');
                    }
                    if (data.data.message === 'wish_deleted') {
                        field.removeClass('fa fa-heart');
                        field.addClass('fa fa-heart-o');

                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })
    </script>

@stop