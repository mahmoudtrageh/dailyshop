@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Profile </title>


@stop


@section('content')

    <main class="main-content" style="height: 66vh;">

        <div id="my-account-page" class="container">
            <div class="row" style="padding:20px 10px;">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card" style="width: 100%;height: 245px;box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);">
                        @if(auth()->user()->img)
                            <img class="card-img-top" style="height: 180px;width: 100%;" src="{{asset('images/'.auth()->user()->img)}}" alt="الصورة الشخصية">
                       @else
                            <img class="card-img-top" style="height: 180px;width: 100%;" src="{{asset('site/images/no-img.jpg')}}" alt="الصورة الشخصية">

                        @endif
                        @if(auth()->user())
                            <div class="card-block">
                                <h4 style="padding:10px" class="card-title">{{auth()->user()->name}}</h4>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <form action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input name="name" class="form-control" type="text" placeholder="الأسم">
                        </div>
                        <div class="form-group">
                            <input name="phone" class="form-control" type="tel" placeholder="رقم الجوال">
                        </div>
                        <div class="form-group">
                            <input name="email" class="form-control" type="email" placeholder="البريد الألكترونى">
                        </div>

                        <div class="col-sm-12 col-xs-12 form-group">
                            <div class="wrap-custom-file inline-block mb-10">
                                <input type="file" name="img" id="imagep{{auth()->user()->id}}" accept=".gif, .jpg, .png"/>
                                <label for="imagep{{auth()->user()->id}}">
                                    <span><i class="fa fa-image fa-2x"></i></span>
                                </label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-secondary btn-lg btn-block" style="margin-top:10px;background:#52b4f9;">تأكيد</button>
                    </form>
                    <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#exampleModalCenter">تعديل كلمة المرور</button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">تعديل كلمة المرور</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('password.edit')}}" method="post">
                                        {{csrf_field()}}
                                        <input class="form-control mb-2" style="margin-bottom: 10px;" type="password" name="old_password" placeholder="كلمه المرور القديمه">
                                        <input class="form-control mb-2" style="margin-bottom: 10px;" type="password" name="new_password" placeholder="كلمه المرور الجديده">
                                        <input class="form-control mb-2" style="margin-bottom: 10px;" type="password" name="new_password_confirmation" placeholder=" تأكيد كلمه المرور الجديده">
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-secondary" data-dismiss="modal" style="margin-bottom:0;">إغلاق</button>
                                            <button type="submit" class="btn btn-primary" style="background:#52b4f9;">تأكيد</button>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

@stop