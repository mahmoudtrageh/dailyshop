@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Contact Us </title>

@stop


@section('content')

    @if(is_null($receipts))

        <main class="main-content" style="height: 72vh;">
            @else {
            <main class="main-content">
                @endif
    <section class="breadcrumb-style-2">

        <section id="breadcrumbs" style="background:#f8f8f8;">
            <div class="container">
                <nav aria-label="breadcrumbs">
                    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="../index.html" title="Back to the frontpage" itemprop="item">
                                <span itemprop="name">الرئيسية</span>
                            </a>
                            <meta itemprop="position" content="1" />
                        </li>

                        <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <span itemprop="item"><span itemprop="name">الفواتير</span></span>
                            <meta itemprop="position" content="2" />
                        </li>

                    </ol>
                </nav>

            </div>
        </section>

        <!-- <h1 class="page-header bottom-indent">Wishlist</h1> -->
    </section>
    <section id="columns" class="columns-container" style="margin-top:50px;">
        <div class="container">
            <div class="row">
                <div id="center_column" class="center_column col-sm-12 col-md-12">
                    <div class="accordion" id="accordionExample">
                        @foreach($receipts as $receipt)
                            @foreach($receipt->details as $detail)
                        <div class="card" >
                            <div class="card-header" id="headingOne" >
                                <h2 class="mb-0">
                                    <button style="transition:all 0.5s ease;" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne{{$detail->id}}" aria-expanded="true" aria-controls="collapseOne">
                                        <p>رقم الفاتورة: <span> {{$detail->id}}</span> <span style="padding:0 5px;color:#52b4f9;">||</span> تاريخ الفاتورة: <span>{{$detail->created_at}}</span> </p>
                                    </button>
                                </h2>
                            </div>

                            <div id="collapseOne{{$detail->id}}" class="collapse animated zoomIn" aria-labelledby="headingOne" data-parent="#accordionExample" style="transition:all 0.5s ease;">
                                <div class="card-body">
                                    <table class="table table-bordered text-center cartt">
                                        <thead >
                                        <tr>
                                            <th class="bg-info text-center" scope="col">المنتج</th>
                                            <th class="bg-info text-center" scope="col">السعر</th>
                                            <th class="bg-info text-center" scope="col">الكمية</th>
                                            <th class="bg-info text-center" scope="col">الإجمالي</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                                    <tr>

                                                        <tr>
                                                         <td class="product"><a>{{$detail->product_name}}</a></td>
                                                            <td class="ptice"> {{$detail->total / $detail->quantity}}$</td>
                                                            <td>{{$detail->quantity}}</td>
                                                            <td> {{$detail->total}}$ </td>




                                                        </tr>

                                        </tbody>
                                    </table>

                                    <h4 style="color:#5aa7ce;padding-right: 10px;">اجمالي الفاتورة</h4>
                                    <table class="table">
                                        <tbody>
                                        <tr>

                                            <td>المجموع الفرعي</td>
                                            <td><span id="grandTotal"></span>{{$detail->total}}$</td>
                                        </tr>
                                        <tr>

                                            <td>الشحن</td>
                                            <td><span id="delivery"></span>{{$receipt->delivery}}$</td>
                                        </tr>
                                        <tr>

                                            <td>المجموع الكلي</td>
                                            <td><span id="LastTotal"></span>{{$detail->total + $receipt->delivery}}$</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                            @endforeach
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </section>
</main>

@stop

@section('js')

                    <script>

                        $(document).on("change", ".price", function () {
                            var total_id = $(this).attr('kid');
                            var count = $('#counter' + total_id).val();

                            console.log(count);
                            var price = $(this).attr('price_after');

                            console.log(price);

                            var total = price * count;

                            console.log(total);

                            $('#total' + total_id).html(total);
                            $('#quant' + total_id).val(count);

                            function calcTotal() {
                                output = 0;
                                $('.ttl').each(function () {
                                    output += parseInt($(this).text())
                                })
                                return output
                            }

                            $("#grandTotal").text(calcTotal());
                            var LastTotal = output + parseInt($('#delivery').text());
                            if ($('#delivery').text() != '') {
                                var LastTotal = output + parseInt($('#delivery').text());
                            }
                            else {
                                var LastTotal = output
                            }
                            $("#LastTotal").html(LastTotal)

                        })

                    </script>


@stop