@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Home</title>


@stop

<link rel="stylesheet"
      href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<style>

    .form-price-range-filter {
        text-align: center;
    }

    .tutorial-table {
        width: 100%;
        font-size: 13px;
        border-top: #e5e5e5 1px solid;
        border-spacing: initial;
        margin: 20px 0px;
        word-break: break-word;
    }

    .tutorial-table th {
        background-color: #f5f5f5;
        padding: 10px 20px;
        text-align: left;
    }

    .tutorial-table td {
        border-bottom: #f0f0f0 1px solid;
        background-color: #ffffff;
        padding: 10px 20px;
    }

    .text-right {
        text-align: right;
    }

    th.text-right {
        text-align: right;
    }

    .btn-submit {
        margin-top: 20px;
        padding: 10px 20px;
        background: #FFF;
        border: #aaa 1px solid;
        border-radius: 4px;
        margin: 20px 0px;
    }

    #min {
        float: left;
        width: 100px;
        padding: 5px 10px;
        margin-right: 14px;
    }

    #slider-range {
        width: 75%;
        float: left;
        margin: 5px 0px 5px 0px;
    }

    #max {
        float: right;
        width: 100px;
        padding: 5px 10px;
    }

    .no-result {
        padding: 20px;
        background: #ffdddd;
        text-align: center;
        border-top: #d2aeb0 1px solid;
        color: #6f6e6e;
    }

    .product-thumb {
        width: 50px;
        height: 50px;
        margin-right: 15px;
        border-radius: 50%;
        vertical-align: middle;
    }

    .catalog_filters {
        height: 200px;
    }

</style>

@section('content')


    <main class="main-content">

        <section class="breadcrumb-style">

            <section id="breadcrumbs">
                <div class="container">
                    <nav aria-label="breadcrumbs">
                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="../index.html" title="Back to the frontpage" itemprop="item">
                                    <span itemprop="name">الرئيسية</span>
                                </a>
                                <meta itemprop="position" content="1" />
                            </li>


                            <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <span itemprop="item"><span itemprop="name">المنتجات</span></span>
                                <meta itemprop="position" content="2" />
                            </li>


                        </ol>
                    </nav>

                </div>
            </section>
            <div class="bg-page-heading" style="background-image: url('{{asset('site/images/vector_shop.jpg')}}');">
                <div class="bg-dark">
                    <h1 class="page-heading product-listing cat-name text-center">المنتجات</h1>
                </div>
            </div>
        </section>
        <section id="columns" class="columns-container">
            <div class="container">
                <div class="row">
                    <aside>
                        <div id="left_column" class="left_column sidebar col-xs-12 col-sm-4 col-md-3">


                            <!-- <div id="categories_block" class="block sidebarCategories ">

                                    <h4 class="title_block">Categories</h4>

                                <div class="block_content">
                                    <ul class="listSidebar list-unstyled">


                                                <li >
                                                    <a href="best-seller.html" title="BEST SELLER">
                                                        BEST SELLER


                                                                <span id="apolloQty284524163" class="apolloQty pull-right">18</span>


                                                    </a>
                                                </li>

                                                <li  class="active" >
                                                    <a href="featured.html" title="FEATURED">
                                                        FEATURED


                                                                <span id="apolloQty284524035" class="apolloQty pull-right">18</span>


                                                    </a>
                                                </li>

                                                <li >
                                                    <a href="hot-product.html" title="HOT PRODUCT">
                                                        HOT PRODUCT


                                                                <span id="apolloQty284524675" class="apolloQty pull-right">18</span>


                                                    </a>
                                                </li>

                                                <li >
                                                    <a href="latest-arrivals.html" title="LATEST ARRIVALS">
                                                        LATEST ARRIVALS


                                                                <span id="apolloQty284523459" class="apolloQty pull-right">18</span>


                                                    </a>
                                                </li>

                                                <li >
                                                    <a href="special-product.html" title="SPECIAL PRODUCT">
                                                        SPECIAL PRODUCT


                                                                <span id="apolloQty284524099" class="apolloQty pull-right">18</span>


                                                    </a>
                                                </li>

                                                <li >
                                                    <a href="trending-product.html" title="TRENDING PRODUCT">
                                                        TRENDING PRODUCT


                                                                <span id="apolloQty279712259" class="apolloQty pull-right">22</span>


                                                    </a>
                                                </li>


                                    </ul>
                                </div>
                            </div> -->




                            <div id="catalog_block" class="block ">

                                <div style="" class="block_content">

                                <form id="filter_form" action="{{route('site-filter')}}" class="" method="get">
                                   @csrf

                                    <div class="catalog_filters">
                                        <div class="catalog_subtitle_heading">
                                            <span class="catalog_subtitle">Price</span>
                                        </div>

                                        <input type="" id="min" name="min_price"
                                               value="{{$min}}">
                                        <div id="slider-range"></div>
                                        <input type="" id="max" name="max_price"
                                               value="{{$max}}">
                                    </div>

                                    <input type="hidden" name="sec_id" value="{{$id}}">

                                    <div class="catalog_filters">

                                        <div class="catalog_subtitle_heading">
                                            <span class="catalog_subtitle">اللون</span>
                                        </div>

                                        <ul id="ul_catalog_color" class="list-unstyled catalog_filter_ul color-group">
                                            @foreach($all_colors as $i=>$all_color)
                                                @if($i < 5)
                                                    <li class="advanced-filter" data-handle="white">
                                                        <a href="javascript:void(0);" class="catalog_color">
                                                            <div class="contain-color" style="background:{{$all_color->class}};"></div>
                                                        </a>
                                                        <input style="display: block !important;" type="checkbox" name="color_id[]" value="{{$all_color->id}}">

                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>

                                        <ul id="ul_catalog_color" class="list-unstyled catalog_filter_ul color-group">
                                            @foreach($all_colors as $i=>$all_color)
                                                @if($i >= 5)
                                                    <li class="advanced-filter" data-handle="white">
                                                        <a href="javascript:void(0);" class="catalog_color">
                                                            <div class="contain-color" style="background:{{$all_color->class}};"></div>
                                                        </a>
                                                        <input style="display: block !important;" type="checkbox" name="color_id[]" value="{{$all_color->id}}">
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>

                                    <button class="btn btn-primary">بحث</button>
                                </form>
                                </div>

                            </div>


                            <div id="products_sider_block" class="block products_block nopadding ">

                                <h4 class="title_block">
                                    المنتجات المميزة
                                </h4>

                                <div class="block_content products-block">
                                    <ul class="products products-block list-unstyled">

                                        <li class="media clearfix">

                                            @foreach($special_Products as $special_product)

                                            <div class="product-block">
                                                <div class="product-container media">
                                                    <a class="products-block-image img pull-left" href="{{route('product.detail',['id'=>$special_product->id])}}" title="Aenean consequat">
                                                        <img style="height: 100px;" class="replace-2x img-responsive" src="{{asset('images/' . $special_product->img)}}" alt="Aenean consequat">
                                                    </a>
                                                    <div class="media-body">
                                                        <div class="product-content">
                                                            <h5 class="name media-heading">
                                                                <a class="product-name" href="{{route('product.detail',['id'=>$special_product->id])}}" title="Aenean consequat">{{$special_product->name}}</a>
                                                            </h5>

                                                            <div class="review">
                                                                <span class="shopify-product-reviews-badge" data-id="8068155587"></span>
                                                            </div>

                                                            <div class="content_price price">

                                                                <span class="old-price product-price"><span class='money'>{{$special_product->price_before}}$</span></span>

                                                                <span class="price product-price">
	                    <span class='money'>{{$special_product->price_after}}$</span>
	                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                           @endforeach

                                        </li>

                                    </ul>

                                </div>
                            </div>




                            <div id="products_viewed_block" class="block products_block nopadding " style="display: none;">

                                <h4 class="title_block">
                                    Product Reviewed
                                </h4>

                                <div class="block_content products-block">
                                    <ul id="recently-products" class="products products-block product_list_widget list-unstyled" style="display: none;">

                                    </ul>
                                </div>
                            </div>

                            <script id="recently-template"  type="text/x-jquery-tmpl">
        <li id="product-${handle}" class="media clearfix">
            <div class="product-block">
                <div class="product-container media">
                    <a class="products-block-image img pull-left" href="${url}" title="${title}">
                        <img class="replace-2x img-responsive" src="${Shopify.Products.resizeImage(featured_image, "small")}" alt="${title}">
                    </a>
                    <div class="media-body">
                        <div class="product-content">
                            <h5 class="name media-heading">
                                <a class="product-name" href="${url}" title="${title}">${title}</a>
                            </h5>
                            <div class="content_price price">

                                <span class="old-price product-price">
                                    <span>
{{--{{html Shopify.formatMoney(compare_at_price, window.money_format)}}--}}
                                </span>
                            </span>
{{--{{/if}}--}}
                                <span class="price product-price">
                                    <span>
                                    	{{--{{html Shopify.formatMoney(price, window.money_format)}}--}}
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
</script>

                            <script>
                                $(document).ready(function() {
                                    Shopify.Products.showRecentlyViewed( {
                                        howManyToShow:3,
                                        onComplete: function() {

                                            Currency.convertAll(window.shop_currency, $("#currencies a.selected").data("currency"), "span.money", "money_format")

                                        }
                                    } );
                                });
                            </script>


                        </div>
                    </aside>





                    <div id="center_column" class="center_column col-xs-12 col-sm-8 col-md-9">
                        <div class="content_scene_cat">
                            <div class="content_scene_cat_bg">

                                <div class="image">
                                    <img class="img-responsive" alt="FEATURED" src="{{asset('site/images/collections/stock-photo-10564837-min_1024x102451b1.jpg?v=1472466772')}}" />
                                </div>


                            </div>
                        </div>
                        <!--  <div class="category-heading clearfix">
                              <div class="sortPagiBar clearfix row">
                                  <div class="col-md-6 col-sm-6 col-xs-4">
                                      <div class="collection-view">
            <span class="hidden-xs">Display</span>
            <button type="button" title="Grid view" class="change-view change-view--active" data-view="grid">
              <span class="icon-fallback-text">
                <span class="fa fa-th-large" aria-hidden="true"></span>
                <span class="fallback-text">Grid view</span>
              </span>
            </button>
            <button type="button" title="List view" class="change-view" data-view="list">
              <span class="icon-fallback-text">
                <span class="fa fa-list-ul" aria-hidden="true"></span>
                <span class="fallback-text">List view</span>
              </span>
            </button>
          </div>
                                  </div>
                                  <div class="col-md-6 col-sm-6 col-xs-8">
                                        <div class="form-horizontal">
              <label for="SortBy" class="hidden-xs">Sort by</label>
              <select name="SortBy" id="SortBy">
                  <option value="manual">Featured</option>
                  <option value="best-selling">Best Selling</option>
                  <option value="title-ascending">Alphabetically, A-Z</option>
                  <option value="title-descending">Alphabetically, Z-A</option>
                  <option value="price-ascending">Price, low to high</option>
                  <option value="price-descending">Price, high to low</option>
                  <option value="created-descending">Date, new to old</option>
                  <option value="created-ascending">Date, old to new</option>
              </select>
          </div>
                                  </div>
                              </div>
                          </div>-->


                        <div id="product_list" class="product_list grid">

                            <div class="product-items">
                                <div class="row">

                                   <div id="my-products">
                                       @foreach($section_details as $section_detail)

                                       <div class="product_block  col-md-4 col-xs-6 col-sp-12" >
                                        <div class="product-container text-left product-block">
                                            <div class="product-image-container image">
                                                <a class="product_img_link" href="{{route('product.detail',['id'=>$section_detail->id])}}">

                                                    <img style="height:200px;" class="img-responsive" src="{{asset('images/'.$section_detail->img)}}" alt="Bibendum egestas">



                                                    <span class="product-additional" data-idproduct="8068158019">




                                    <img style="height:200px;" class="replace-2x img-responsive image-effect" alt="Bibendum egestas" src="{{asset('images/'.$section_detail->img2)}}">

</span>


                                                </a>



                                                <div class="show-product">

                                                    <div class="quickview">
                                                        <a class="new-quick btn" href="{{route('product.detail',['id'=>$section_detail->id])}}">
                                                            <i cid="{{$section_detail->id}}" class="card-change fa fa-eye @if(auth()->user()) @if (in_array($section_detail->id,$cart)) card-color @endif @endif"></i>
                                                        </a>
                                                    </div>


                                                    <div class="wishlist" pid="{{$section_detail->id}}">
                                                        <a class="btn btn-outline-inverse btn-wishlist" title="Add to Wishlist">
                                                            <i  class=" add-class fa fa-heart-o @if(auth()->user()) @if (in_array($section_detail->id,$wish)) fa fa-heart @endif @endif" aria-hidden="true">
                                                            </i>
                                                            <span>إضافة إلى المفضلة</span>
                                                        </a>
                                                    </div>




                                                    <!-- <div class="ap-cp-add-compare">
                                                      <input type="checkbox" data-img="//cdn.shopify.com/s/files/1/1413/8964/products/50eb8f24983235-min_large.jpg?v=1472007805" data-title="Bibendum egestas" data-id="bibendum-egestas-bibendum-lorem-commodo" class="ap-cp-add">
                                                      <i class="fa fa-retweet"></i><span>Add to compare</span>
                                                    </div> -->

                                                </div>
                                            </div>
                                            <div class="product-meta">
                                                <h5 class="name">
                                                    <a class="product-name" href="{{route('product.detail',['id'=>$section_detail->id])}}" title="Bibendum egestas">{{$section_detail->name}}</a>
                                                </h5>
                                                <div class="content_price">

        <span class="price product-price ">
          <span class='money'>{{$section_detail->price_after}}$</span>
        </span>
                                                </div>
                                                <div class="product-desc">{{$section_detail->description}}</div>

                                                <div class="review">
                                                    <span class="shopify-product-reviews-badge" data-id="8068158019"></span>
                                                </div>

                                                <div class="functional-buttons clearfix">
                                                    <div class="cart">

                                                        {{--<div class="action">--}}
                                                            {{--<form action="https://ap-cogen.myshopify.com/cart/add" method="post" enctype="multipart/form-data" class="form-ajaxtocart">--}}
                                                                {{--<input type="hidden" name="id" value="26088911939" />--}}

                                                        <div class="card-change" cid="{{$section_detail->id}}">
                                                            <a class="button ajax_addtocart" title="Add to Cart">
                                                                <i class="class-card{{$section_detail->id}} fa fa-shopping-cart @if(auth()->user()) @if (in_array($section_detail->id,$cart)) card-color @endif @endif"></i>
                                                            </a>

                                                        </div>

                                                            {{--</form>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                       @endforeach

                                   </div>

                                </div>

                            </div>


                        </div>


                        <div class="content_sortPagiBar">
                            <div id="pagination" class="clearfix">
                                <ul class="pagination">

                                    <li class="pagination_previous disabled">
                                        Previous
                                    </li>




                                    <li class="active"><span>1</span></li>




                                    <li>
                                        <a href="featured4658.html?page=2" title="">2</a>
                                    </li>



                                    <li class="pagination_next"><a href="featured4658.html?page=2" title="Next &raquo;">Next</a></li>

                                </ul>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </section>

    </main>

    <div class="modal fade login-form animated zoomInLeft text-right" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel71"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>تسجيل الدخول</h1><br>
                <form action="{{route('site-login-check')}}#login_form" method="post">
                    {{csrf_field()}}
                    <input type="text" name="email" value="{{old('email')}}" placeholder="البريد الإلكتروني *">
                    <input type="password" type="password" name="password"
                           placeholder="الرقم السري *">
                    <input type="submit" name="login" class="login loginmodal-submit" value="تسجيل الدخول">
                </form>

                <div class="login-help">
                    <a href="{{route('site.register')}}">Register</a> - <a href="{{route('site-forget')}}">
                        نسيت كلمة السر</a>
                </div>
            </div>
        </div>
    </div>


@stop

@section('js')

    <script>

        $(document).on("click", ".card-change", function (e) {
            e.preventDefault();
            var id = $(this).attr("cid");
            var field = $(".class-card" + id);
            var counter = document.getElementById('CartCount');
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('Cart-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message === 'user_not_logged_in') {
                        $('#login-modal').modal('show');
                    }
                    if (data.data.message === 'order_added') {
                        counter.innerText = Number(counter.innerText) + 1;
                        field.addClass('card-color');
                    }

                    if (data.data.message === 'order_deleted') {
                        counter.innerText = Number(counter.innerText) - 1;
                        field.removeClass('card-color');
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })


        $(document).on("click", ".wishlist", function (e) {
            e.preventDefault();
            var id = $(this).attr("pid");
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('wish-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},

                success: function (data) {
                    if ((data.data.message === 'user_not_logged_in')) {
                        $('#login-modal').modal('show');
                    }
                    if (data.data.message === 'wish_added') {
                        $('.add-class').removeClass('fa fa-heart-o');
                        $('.add-class').addClass('fa fa-heart');
                    }
                    if (data.data.message === 'wish_deleted') {
                        $('.add-class').removeClass('fa fa-heart');
                        $('.add-class').addClass('fa fa-heart-o');

                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })
    </script>

    {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script>

        $(function() {
            $( "#slider-range" ).slider({
                range: true,
                min: 0,
                max: 1000,
                values: [ {{$min}}, {{$max}} ],
                slide: function( event, ui ) {
                    $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                    $( "#min" ).val(ui.values[ 0 ]);
                    $( "#max" ).val(ui.values[ 1 ]);
                }
            });
            $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
                " - $" + $( "#slider-range" ).slider( "values", 1 ) );
        });
    </script>

    <script>
        $('#filter_form').on('submit', function (e) {
            e.preventDefault();
            let form = $('#filter_form');
            let data = form.serialize();
            console.log(data);
            console.log(form.attr('method'));
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: data,
            success: function (data) {
                if (data.response === 'success') {
                        $('#my-products').html(data.data);
                    }
                },
                error: function () {
                    console.log(data);
                    alert('Error')
                }
            });
        })
    </script>

@stop