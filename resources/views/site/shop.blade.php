@extends('layouts.Site-Layout')


@section('title')

    <title> Shop </title>

@stop

<style>

    .example-val {
        width: 50px;
        padding: 10px;
        text-align: center;
        margin: 5px;
    }

</style>

@section('content')

    <link rel="stylesheet"
          href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />


    <!-- catg header banner section -->
    <section id="aa-catg-head-banner" style="direction: ltr;">
        <img src="{{asset('site/images/heading-bg.jpg')}}" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <ol class="breadcrumb">
                        <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                        <li class="active">المتجر</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <!-- product category -->
    <section id="aa-product-category" >
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
                    <div class="aa-product-catg-content">
                        <div class="aa-product-catg-head" dir="rtl">
                            <div class="aa-product-catg-head-right">
                                <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                                <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
                            </div>
                            <div class="aa-product-catg-head-left">
                                    <label for="">رتبى حسب</label>
                                <select class="order-pro">
                                    <option value="1" selected>الافتراضى</option>
                                        <option value="2">الاسم</option>
                                        <option value="3">السعر</option>
                                        <option value="4">التاريخ</option>
                                    </select>

                                    <label for="">إعرضى</label>
                                    <select class="show-num">
                                        <option disabled selected>اختاري الرقم</option>
                                        <option value="all">الكل</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                            </div>
                        </div>

                        <div id="my-products">

                            <div class="aa-product-catg-body">
                                <ul class="aa-product-catg">
                                    <!-- start single product item -->

                                    @if(isset($id))
                                        @foreach($dir_products as $dir_product)
                                            <li>
                                                <figure>
                                                    <a class="aa-product-img" href="{{route('product.detail',['id'=>$dir_product->id])}}"><img src="{{asset('images/'.$dir_product->img)}}" alt="polo shirt img"></a>
                                                    <a class="aa-add-card-btn card-change" cid="{{$dir_product->id}}"><span class="class-card{{$dir_product->id}} @if(auth()->user()) @if (in_array($dir_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a href="{{route('product.detail',['id'=>$dir_product->id])}}">{{$dir_product->name}}</a></h4>
                                                        @if(!is_null($dir_product->price_after))

                                                            <del class="aa-product-price">{{$dir_product->price_before}}</del>
                                                            <span class="aa-product-price">{{$dir_product->price_after}}</span>

                                                        @else
                                                            <span class="aa-product-price">{{$dir_product->price_before}}</span>

                                                        @endif
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                    <a class="wishlist" pid="{{$dir_product->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="add-class{{$dir_product->id}} @if(auth()->user()) @if (in_array($dir_product->id, $wish)) fa fa-heart @else fa fa-heart-o @endif @else fa fa-heart-o @endif"></span></a>
                                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                                                    <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal-dir{{$dir_product->id}}"><span class="fa fa-search"></span></a>
                                                </div>
                                                <!-- product badge -->
                                                <span class="aa-badge aa-sale" href="#">SALE!</span>
                                            </li>
                                        @endforeach

                                            @foreach($dir_products as $dir_product)

                                            <!-- quick view modal -->
                                                <div class="modal fade" id="quick-view-modal-dir{{$dir_product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <div class="row">
                                                                    <!-- Modal view slider -->
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="aa-product-view-slider">
                                                                            <div class="simpleLens-gallery-container" id="demo-1-n{{$dir_product->id}}">
                                                                                <div class="simpleLens-container">
                                                                                    <div class="simpleLens-big-image-container">
                                                                                        <a class="simpleLens-lens-image" data-lens-image="{{asset('images/' . $dir_product->img)}}">
                                                                                            <img src="{{asset('images/' . $dir_product->img)}}" class="simpleLens-big-image">
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="simpleLens-thumbnails-container">
                                                                                    <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                       data-lens-image="{{asset('images/' . $dir_product->img2)}}"
                                                                                       data-big-image="{{asset('images/' . $dir_product->img2)}}">
                                                                                        <img style="width:25px;height:25px;" src="{{asset('images/' . $dir_product->img2)}}">
                                                                                    </a>
                                                                                    <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                       data-lens-image="{{asset('images/' . $dir_product->img)}}"
                                                                                       data-big-image="{{asset('images/' . $dir_product->img)}}">
                                                                                        <img style="width:25px;height:25px;" src="{{asset('images/' . $dir_product->img)}}">
                                                                                    </a>

                                                                                    <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                       data-lens-image="{{asset('images/' . $dir_product->img2)}}"
                                                                                       data-big-image="{{asset('images/' . $dir_product->img2)}}">
                                                                                        <img style="width:25px;height:25px;" src="{{asset('images/' . $dir_product->img2)}}">
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Modal view content -->
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="aa-product-view-content">
                                                                            <h3>{{$dir_product->name}}</h3>
                                                                            <div class="aa-price-block">
                                                                                @if(!is_null($dir_product->price_after))

                                                                                    <del class="aa-product-price">{{$dir_product->price_before}}</del>
                                                                                    <span class="aa-product-price">{{$dir_product->price_after}}</span>

                                                                                @else
                                                                                    <span class="aa-product-price">{{$dir_product->price_before}}</span>

                                                                                @endif
                                                                                <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                                                                            </div>
                                                                            <p>{{$dir_product->description}}</p>
                                                                            <h4>Size</h4>
                                                                            <div class="aa-prod-view-size">
                                                                                <a href="#">S</a>
                                                                                <a href="#">M</a>
                                                                                <a href="#">L</a>
                                                                                <a href="#">XL</a>
                                                                            </div>
                                                                            <div class="aa-prod-quantity">
                                                                                <form action="">
                                                                                    <select name="" id="">
                                                                                        <option value="0" selected="1">1</option>
                                                                                        <option value="1">2</option>
                                                                                        <option value="2">3</option>
                                                                                        <option value="3">4</option>
                                                                                        <option value="4">5</option>
                                                                                        <option value="5">6</option>
                                                                                    </select>
                                                                                </form>
                                                                                <p class="aa-prod-category">
                                                                                    القسم: <a href="#">{{$dir_product->section->name}}</a>
                                                                                </p>
                                                                            </div>
                                                                            <div class="aa-prod-view-bottom">
                                                                                <a class="aa-add-card-btn card-change" cid="{{$dir_product->id}}"><span class="class-card{{$dir_product->id}} @if(auth()->user()) @if (in_array($dir_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                                                <a href="{{route('product.detail',['id'=>$dir_product->id])}}" class="aa-add-to-cart-btn">أظهر التفاصيل</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- / quick view modal -->

                                            @endforeach

                                    @else

                                    @foreach($all_products as $all_product)
                                        <li>
                                            <figure>
                                                <a class="aa-product-img" href="{{route('product.detail',['id'=>$all_product->id])}}"><img src="{{asset('images/'.$all_product->img)}}" alt="polo shirt img"></a>
                                                <a class="aa-add-card-btn card-change" cid="{{$all_product->id}}"><span class="class-card{{$all_product->id}} @if(auth()->user()) @if (in_array($all_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                <figcaption>
                                                    <h4 class="aa-product-title"><a href="{{route('product.detail',['id'=>$all_product->id])}}">{{$all_product->name}}</a></h4>
                                                    @if(!is_null($all_product->price_after))

                                                        <del class="aa-product-price">{{$all_product->price_before}}</del>
                                                        <span class="aa-product-price">{{$all_product->price_after}}</span>

                                                    @else
                                                        <span class="aa-product-price">{{$all_product->price_before}}</span>

                                                    @endif
                                                </figcaption>
                                            </figure>
                                            <div class="aa-product-hvr-content">
                                                <a class="wishlist" pid="{{$all_product->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="add-class{{$all_product->id}} @if(auth()->user()) @if (in_array($all_product->id, $wish)) fa fa-heart @else fa fa-heart-o @endif @else fa fa-heart-o @endif"></span></a>
                                                <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                                                <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal-all{{$all_product->id}}"><span class="fa fa-search"></span></a>
                                            </div>
                                            <!-- product badge -->
                                            <span class="aa-badge aa-sale" href="#">SALE!</span>
                                        </li>
                                     @endforeach

                                        @foreach($all_products as $all_product)

                                        <!-- quick view modal -->
                                            <div class="modal fade" id="quick-view-modal-all{{$all_product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <div class="row">
                                                                <!-- Modal view slider -->
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="aa-product-view-slider">
                                                                        <div class="simpleLens-gallery-container" id="demo-1-n{{$all_product->id}}">
                                                                            <div class="simpleLens-container">
                                                                                <div class="simpleLens-big-image-container">
                                                                                    <a class="simpleLens-lens-image" data-lens-image="{{asset('images/' . $all_product->img)}}">
                                                                                        <img src="{{asset('images/' . $all_product->img)}}" class="simpleLens-big-image">
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="simpleLens-thumbnails-container">
                                                                                <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                   data-lens-image="{{asset('images/' . $all_product->img2)}}"
                                                                                   data-big-image="{{asset('images/' . $all_product->img2)}}">
                                                                                    <img style="width:25px;height:25px;" src="{{asset('images/' . $all_product->img2)}}">
                                                                                </a>
                                                                                <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                   data-lens-image="{{asset('images/' . $all_product->img)}}"
                                                                                   data-big-image="{{asset('images/' . $all_product->img)}}">
                                                                                    <img style="width:25px;height:25px;" src="{{asset('images/' . $all_product->img)}}">
                                                                                </a>

                                                                                <a href="#" class="simpleLens-thumbnail-wrapper"
                                                                                   data-lens-image="{{asset('images/' . $all_product->img2)}}"
                                                                                   data-big-image="{{asset('images/' . $all_product->img2)}}">
                                                                                    <img style="width:25px;height:25px;" src="{{asset('images/' . $all_product->img2)}}">
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- Modal view content -->
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="aa-product-view-content">
                                                                        <h3>{{$all_product->name}}</h3>
                                                                        <div class="aa-price-block">
                                                                            @if(!is_null($all_product->price_after))

                                                                                <del class="aa-product-price">{{$all_product->price_before}}</del>
                                                                                <span class="aa-product-price">{{$all_product->price_after}}</span>

                                                                            @else
                                                                                <span class="aa-product-price">{{$all_product->price_before}}</span>

                                                                            @endif
                                                                            <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                                                                        </div>
                                                                        <p>{{$all_product->description}}</p>
                                                                        <h4>Size</h4>
                                                                        <div class="aa-prod-view-size">
                                                                            <a href="#">S</a>
                                                                            <a href="#">M</a>
                                                                            <a href="#">L</a>
                                                                            <a href="#">XL</a>
                                                                        </div>
                                                                        <div class="aa-prod-quantity">
                                                                            <form action="">
                                                                                <select name="" id="">
                                                                                    <option value="0" selected="1">1</option>
                                                                                    <option value="1">2</option>
                                                                                    <option value="2">3</option>
                                                                                    <option value="3">4</option>
                                                                                    <option value="4">5</option>
                                                                                    <option value="5">6</option>
                                                                                </select>
                                                                            </form>
                                                                            <p class="aa-prod-category">
                                                                                القسم: <a href="#">{{$all_product->section->name}}</a>
                                                                            </p>
                                                                        </div>
                                                                        <div class="aa-prod-view-bottom">
                                                                            <a class="aa-add-card-btn card-change" cid="{{$all_product->id}}"><span class="class-card{{$all_product->id}} @if(auth()->user()) @if (in_array($all_product->id, $cart)) fa fa-shopping-cart card-color @else fa fa-shopping-cart @endif @else fa fa-shopping-cart @endif"></span>ضيفى إلى العربة</a>
                                                                            <a href="{{route('product.detail',['id'=>$all_product->id])}}" class="aa-add-to-cart-btn">أظهر التفاصيل</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- / quick view modal -->

                                        @endforeach

                                        @endif

                                </ul>

                            </div>

                        </div>

                        <div class="text-center">

                            @if(isset($id))
                            {{$dir_products->links()}}
                                @else
                                {{$all_products->links()}}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9" dir="rtl">
                    <aside class="aa-sidebar">

                        {{-- filter start --}}

                        <form id="filter_form" action="{{route('site-filter')}}" class="" method="get">

                        <!-- single sidebar -->
                        <div class="aa-sidebar-widget">
                            <h3>الأقسام</h3>
                            <ul class="aa-catg-nav">
                                    <select name="section_id">
                                        <option disabled selected>اختر القسم</option>
                                    @foreach($sections as $section)
                                            <option value="{{$section->id}}">{{$section->name}}</option>
                                        @endforeach
                                    </select>
                            </ul>
                        </div>

                        <!-- single sidebar -->
                        <div class="aa-sidebar-widget" style="margin-bottom: 35px;">
                            <h3>بحث حسب السعر</h3>
                            <!-- price range -->

                            <div class="aa-sidebar-price-range">

                                <input type="" id="min" style="float:left;" class="example-val" name="min_price"
                                       value="{{$min}}">
                                <input type="" id="max" class="example-val" name="max_price"
                                       value="{{$max}}">
                                <div id="slider-range"></div>
                            </div>


                        </div>
                        <!-- single sidebar -->
                            <div class="catalog_filters">
                                <div class="aa-sidebar-widget">

                                <h3>بحث حسب اللون</h3>
                                <ul id="ul_catalog_color" class="list-unstyled catalog_filter_ul color-group" style="display: flex;">
                                    @foreach($all_colors as $i=>$all_color)
                                        @if($i < 5)
                                            <li class="advanced-filter" data-handle="white" style="padding-left: 10px;">
                                                <a href="javascript:void(0);" class="catalog_color">
                                                    <div class="contain-color" style="background:{{$all_color->class}};width: 13px;height: 12px;border-radius: 4px;"></div>
                                                </a>
                                                <input style="display: block !important;" type="checkbox" name="color_id[]" value="{{$all_color->id}}">

                                            </li>
                                        @endif
                                    @endforeach
                                </ul>

                                <ul id="ul_catalog_color" class="list-unstyled catalog_filter_ul color-group" style="display: flex;">
                                    @foreach($all_colors as $i=>$all_color)
                                        @if($i >= 5)
                                            <li class="advanced-filter" data-handle="white" style="padding-left: 10px;">
                                                <a href="javascript:void(0);" class="catalog_color">
                                                    <div class="contain-color" style="background:{{$all_color->class}};width: 13px;height: 12px;border-radius: 4px;"></div>
                                                </a>
                                                <input style="display: block !important;" type="checkbox" name="color_id[]" value="{{$all_color->id}}">
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                                </div>
                            </div>

                            <button class="aa-filter-btn" type="submit">بحث</button>

                        </form>
                        {{-- filter end --}}

                        <!-- single sidebar -->
                        <div class="aa-sidebar-widget">
                            <h3>المشاهدات الحديثة</h3>
                            <div class="aa-recently-views">
                                <ul>
                                    @foreach($new_products as $new_product)
                                        <li>
                                            <a href="{{route('product.detail',['id'=>$new_product->id])}}" class="aa-cartbox-img"><img alt="img" src="{{asset('images/' . $new_product->img)}}"></a>
                                            <div class="aa-cartbox-info">
                                                <h4><a href="{{route('product.detail',['id'=>$new_product->id])}}">{{$new_product->name}}</a></h4>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- single sidebar -->
                        <div class="aa-sidebar-widget">
                            <h3>الفساتين المميزة</h3>
                            <div class="aa-recently-views">
                                <ul>
                                    @foreach($special_Products as $special_product)
                                        <li>
                                            <a href="#" class="aa-cartbox-img"><img alt="img" src="{{asset('images/' . $special_product->img)}}"></a>
                                            <div class="aa-cartbox-info">
                                                <h4><a href="{{route('product.detail',['id'=>$special_product->id])}}">{{$special_product->name}}</a></h4>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>

            </div>
        </div>
    </section>
    <!-- / product category -->

    @stop

@section('js')

    {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

        $(function() {
            $( "#slider-range" ).slider({
                range: true,
                min: {{$min}},
                max: {{$max}},
                values: [ {{$min}}, {{$max}} ],
                slide: function( event, ui ) {
                    $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                    $( "#min" ).val(ui.values[ 0 ]);
                    $( "#max" ).val(ui.values[ 1 ]);
                }
            });
            $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
                " - $" + $( "#slider-range" ).slider( "values", 1 ) );
        });

        $('#filter_form').on('submit', function (e) {
            e.preventDefault();
            let form = $('#filter_form');
            let data = form.serialize();
            console.log(data);
            console.log(form.attr('method'));
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: data,
                success: function (data) {
                    if (data.response === 'success') {
                        $('#my-products').html(data.data);
                    }
                },
                error: function () {
                    console.log(data);
                    alert('Error')
                }
            });
        })

        $(document).on("change", ".show-num", function (e) {
            e.preventDefault();
            let num = $(this).val();
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('site.get.num') }}",
                type: "post",
                dataType: "json",
                data: {num: num, _token: token},
                success: function (data) {
                    if (data.response === 'success') {
                        $('#my-products').html(data.data);
                    }
                },
                error: function () {
                    console.log(data);
                    alert('Error')
                }
            });
        })

        $(document).on("change", ".order-pro", function (e) {
            e.preventDefault();
            let order = $(this).val();
            let num = $('.show-num').val();
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('site.get.order') }}",
                type: "post",
                dataType: "json",
                data: {order: order, _token: token, num: num},
                success: function (data) {
                    if (data.response === 'success') {
                        $('#my-products').html(data.data);
                    }
                },
                error: function () {
                    console.log(data);
                    alert('Error')
                }
            });
        })

        $(document).on("click", ".card-change", function (e) {
            e.preventDefault();
            var id = $(this).attr("cid");
            var field = $(".class-card" + id);
            var counter = document.getElementById('CartCount');
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('Cart-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message === 'user_not_logged_in') {
                        $("#login-modal").modal('show');
                    }
                    if (data.data.message === 'order_added') {
                        counter.innerText = Number(counter.innerText) + 1;
                        field.addClass('card-color');
                    }

                    if (data.data.message === 'order_deleted') {
                        counter.innerText = Number(counter.innerText) - 1;
                        field.removeClass('card-color');
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })


        $(document).on("click", ".wishlist", function (e) {
            e.preventDefault();
            var id = $(this).attr("pid");
            var field = $(".add-class" + id);
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('wish-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token,},

                success: function (data) {
                    if ((data.data.message === 'user_not_logged_in')) {
                        // console.log(data.data.message);
                        $("#login-modal").modal('show');
                    }
                    if (data.data.message === 'wish_added') {
                        field.removeClass('fa fa-heart-o');
                        field.addClass('fa fa-heart');
                    }
                    if (data.data.message === 'wish_deleted') {
                        field.removeClass('fa fa-heart');
                        field.addClass('fa fa-heart-o');

                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })
    </script>

@stop