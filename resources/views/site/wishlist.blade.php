@extends('layouts.Site-Layout')

@section('title')

    <title> Tohfa | Wish-List</title>

@stop

@section('content')
    {{--@if(is_null($wishs))--}}

    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="{{asset('site/images/fashion/heading-bg.jpg')}}" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>المفضلة</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{route('site-index')}}">الرئيسية</a></li>
                        <li class="active">المفضلة</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <!-- Cart view section -->
    <section id="cart-view" dir="rtl">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cart-view-area">
                        <div class="cart-view-table aa-wishlist-table">
                            <form action="">
                                <div class="table-responsive">
                                    @foreach($wishs as $wish)
                                    <table class="table" id="row{{$wish->id}}">
                                        <thead>
                                        <tr>
                                            <th>حذف</th>
                                            <th>الصورة</th>
                                            <th>اسم المنتج</th>
                                            <th>السعر</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><a class="remove del" aid="{{$wish->id}}" href="#"><fa class="fa fa-close"></fa></a></td>
                                            <td><a href="#"><img src="{{asset('images/' . $wish->img)}}" alt="img"></a></td>
                                            <td><a class="aa-cart-title" href="#">{{$wish->name}}</a></td>
                                            <td>{{$wish->price_before}}</td>
                                            <td><a  cid="{{$wish->id}}" class="aa-add-to-cart-btn card-change class-card{{$wish->id}} @if(auth()->user()) @if (in_array($wish->id, $cart)) card-color @endif @endif" href="#">ضيفي للعربة</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    @endforeach
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / Cart view section -->

@stop

@section('js')

    <script>

        $(document).on("click", ".card-change", function (e) {
            e.preventDefault();
            var id = $(this).attr("cid");
            var field = $(".class-card" + id);
            var counter = document.getElementById('CartCount');
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('Cart-change') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message === 'user_not_logged_in') {
                        $('#login-modal').modal('show');
                    }
                    if (data.data.message === 'order_added') {
                        counter.innerText = Number(counter.innerText) + 1;
                        field.addClass('card-color');
                    }

                    if (data.data.message === 'order_deleted') {
                        counter.innerText = Number(counter.innerText) - 1;
                        field.removeClass('card-color');
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })

        $(document).on("click", ".del", function () {
            var id = $(this).attr("aid");
            var table = $('#row' + id);
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('wish-delete') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    if (data.data.message == 'wishlist_delete') {
                        table.remove();
                    }

                },
                error: function (error) {
                    console.log(error);
                    alert("ERROR");
                }
            })
        })
    </script>

    @stop