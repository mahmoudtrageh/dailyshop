<?php

/* Site login , logout  and Reset routes*/

Route::group(['namespace' => 'Api', 'prefix' => 'auth'], function () {

    Route::post('/registered', 'LoginController@registered');

    Route::post('/login', 'LoginController@login');

    Route::post('/check-email', 'LoginController@check');

    Route::post('/code-confirmation', 'LoginController@code_confirmation');

    Route::post('/pass-change', 'LoginController@pass_change');

});


/* Routes With No Login Required */


Route::group(['namespace' => 'Api'], function () {

    // Sliders

    Route::get('sliders', 'MainController@get_sliders');

    // Contact Us

    Route::post('contact-us', 'MainController@contact_send');

    // About Us

    Route::get('about-us', 'MainController@get_about');

    // Products

    Route::get('get-products', 'MainController@get_products');
    Route::get('get-sections', 'MainController@get_sections');

    //Cart
    Route::get('get-delivery-data', 'CartController@get_delivery_data');

});

/* Routes With Login Required */

Route::group(['namespace' => 'Api', 'middleware' => 'Api'], function () {

    // Wish List

    Route::get('Wish-change/{id}', 'WishListController@wish_change');
    
    Route::get('get-wishlist', 'WishListController@get_wishlist');

    // Cart

    Route::post('cart-change', 'CartController@Cart_change');


    Route::post('cart-checkout', 'CartController@check_out');
    
    Route::get('get-cart', 'CartController@get_cart');


//invoices
    Route::get('get-invoice', 'MainController@get_invoice');

    
    Route::group([ 'prefix' => 'auth'], function () {
    
    // profile 

		Route::post('update-info', 'LoginController@update_data');
         	   Route::post('/update-password', 'LoginController@update_password');



	});

});


