<?php

/* Admin login and logout routes*/

Route::group(['namespace' => 'Admin','prefix' => 'admin'], function () {


    Route::get('/login', 'LoginController@index')->name('login');

    Route::post('/check', 'LoginController@login')->name('logincheck');

    Route::get('/logout', 'LoginController@logout')->name('logout');


});





/* Admin Routes With Prefix "Admin" , namespace "Admin And MiddleWare "Admin" */

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'namespace' => 'Admin'], function () {


    /*
     * Mark Notification As Read
     */


    Route::get('/readNotiy','NotificationController@readNotiy')->name('read');




    /*
     * Profile
     */


    Route::get('Profile', 'ProfileController@index')->name('profile');
    Route::post('Edit-Profile', 'ProfileController@edit_profile')->name('edit-profile');
    Route::get('Password', 'ProfileController@password')->name('password');
    Route::post('Edit-Password', 'ProfileController@edit_password')->name('edit-password');


    /*
     * Index
     */


    Route::get('', 'IndexController@index')->name('admin-index');

    /*
     * Admins
     */

    Route::get('Admins', 'AdminsController@index')->name('admins');
    Route::post('Add-Admins', 'AdminsController@add_admins')->name('add-admins');
    Route::post('Edit-Admins', 'AdminsController@edit_admins')->name('edit-admins');
    Route::get('Delete-Admins', 'AdminsController@delete_admins')->name('delete-admins');


    /*
     * Sections
     */

    Route::get('Sections', 'SectionsController@index')->name('sections');
    Route::post('Add-Sections', 'SectionsController@add_sections')->name('add-sections');
    Route::post('Edit-Sections', 'SectionsController@edit_sections')->name('edit-sections');
    Route::get('Delete-Sections', 'SectionsController@delete_sections')->name('delete-sections');


    /*
     * Products
     */

    Route::get('Products', 'ProductsController@index')->name('products');
    Route::post('Add-Products', 'ProductsController@add_products')->name('add-products');
    Route::post('Edit-Products', 'ProductsController@edit_products')->name('edit-products');
    Route::get('Delete-Products', 'ProductsController@delete_products')->name('delete-products');
    Route::post('/update-status', 'ProductsController@updateStatus');

    /*
    * Coupons
    */

    Route::get('coupons', 'CouponsController@index')->name('coupons');
    Route::post('Add-coupons', 'CouponsController@add_coupons')->name('add-coupons');
    Route::post('Edit-coupons', 'CouponsController@edit_coupons')->name('edit-coupons');
    Route::get('Delete-coupons', 'CouponsController@delete_coupons')->name('delete-coupons');


    /*
     * Users
     */

    Route::get('Users', 'UsersController@index')->name('users');
    Route::post('Add-Users', 'UsersController@add_users')->name('add-users');
    Route::post('Edit-Users', 'UsersController@edit_users')->name('edit-users');
    Route::get('Delete-Users', 'UsersController@delete_users')->name('delete-users');
    Route::post('/update-statuss', 'UsersController@updateStatus');

    /*
    * Sizes
    */

    Route::get('sizes/{id}', 'SizeController@index')->name('sizes');
    Route::post('Edit-size', 'SizeController@edit_size')->name('edit-size');
    Route::get('Delete-size', 'SizeController@delete_size')->name('delete-size');


    /*
     * Countries
     */

    Route::get('Countries', 'CountriesController@index')->name('countries');
    Route::post('Add-Countries', 'CountriesController@add_countries')->name('add-countries');
    Route::post('Edit-Countries', 'CountriesController@edit_countries')->name('edit-countries');
    Route::get('Delete-Countries', 'CountriesController@delete_countries')->name('delete-countries');


    /*
     * Cities
     */

    Route::get('Cities/{id}', 'CitiesController@index')->name('cities');
    Route::post('Add-Cities', 'CitiesController@add_cities')->name('add-cities');
    Route::post('Edit-Cities', 'CitiesController@edit_cities')->name('edit-cities');
    Route::get('Delete-Cities', 'CitiesController@delete_cities')->name('delete-cities');



    /*
     * Messages
     */

    Route::get('Messages','MessagesController@index')->name('messages');
    Route::get('Delete-Message/{id}','MessagesController@delete_message')->name('delete-message');
    Route::post('Delete-all','MessagesController@delete_all')->name('delete-all-message');


    /*
     * About
     */

    Route::get('About', 'AboutController@index')->name('About');
    Route::post('Edit-About-Main', 'AboutController@edit_about_main')->name('edit-about-main');
    Route::post('Edit-About-Sub', 'AboutController@edit_about_sub')->name('edit-about-sub');


    /*
     * Settings
     */

    Route::get('Settings', 'SettingsController@index')->name('settings');
    Route::post('Edit-Settings', 'SettingsController@edit_settings')->name('edit-settings');


    /*
     * Sliders
     */

    Route::post('Add-Sliders', 'SettingsController@add_sliders')->name('add-sliders');
    Route::post('Edit-Sliders', 'SettingsController@edit_sliders')->name('edit-sliders');
    Route::post('Delete-Sliders', 'SettingsController@delete_sliders')->name('delete-sliders');

    Route::post('Add-Marka', 'SettingsController@add_marka')->name('add-marka');
    Route::post('Edit-Marka', 'SettingsController@edit_marka')->name('edit-marka');
    Route::post('Delete-Marka', 'SettingsController@delete_marka')->name('delete-marka');

    Route::post('Edit-Banarat', 'SettingsController@edit_banarat')->name('edit-banarat');
    Route::post('Edit-Markat', 'SettingsController@edit_markat')->name('edit-markat');

    /*
     * Contact
     */

    Route::post('Edit-Contact', 'SettingsController@edit_contact')->name('edit-contact');



    /*
     * Invoices
     */

    Route::get('Invoice','InvoiceController@index')->name('invoice');
    Route::get('Invoice-Details/{id}','InvoiceController@details')->name('Invoice-Details');
    Route::post('Invoice-Delete','InvoiceController@delete')->name('Invoice-Delete');

});



/*
 * ***********************************************************************************************************************
 * ***********************************************************************************************************************
 */




/* Site login , logout  and Reset routes*/

Route::group(['namespace' => 'Site'], function () {


    Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {

        Route::get('/register', 'AuthController@get_register')->name('site.get.register');
        Route::post('/registered', 'AuthController@register')->name('site.register');
        Route::get('/login', 'AuthController@get_login')->name('site.get.login');
        Route::post('/check', 'AuthController@login')->name('site-login-check');
        Route::get('/logout', 'AuthController@logout')->name('site.logout');
        Route::get('/forget', 'AuthController@forget')->name('site-forget');
        Route::post('/check-email', 'AuthController@check')->name('site-check');
        Route::get('/code', 'AuthController@code')->name('site.code');
        Route::post('/code-confirmation', 'AuthController@code_confirmation')->name('code.confirmation');
        Route::post('/pass-change', 'AuthController@pass_change')->name('site.pass.change');

    });



});





/* Site Routes With namespace "Site" And MiddleWare "Site" */

Route::group(['namespace' => 'Site'], function () {

    /*
     * Index
     */

    Route::get('','IndexController@index')->name('site-index');

    Route::get('shop', 'ShopController@index')->name('site-shop');

    Route::post('get-num', 'ShopController@get_num')->name('site.get.num');

    Route::post('get-order', 'ShopController@get_order')->name('site.get.order');

    Route::get('product-detail/{id}', 'ShopController@product_detail')->name('product.detail');

    Route::get('section-detail/{id}', 'ShopController@section_details')->name('section.detail');

    Route::get('about-us', 'AboutController@index')->name('site.about.us');

    Route::get('contact-us', 'ContactController@index')->name('site.contact.us');

    Route::get('receipt', 'ReceiptController@index')->name('site.receipt');


    /*
     * Filter
     */


    Route::get('filter','ShopController@filter')->name('site-filter');


    /*
     * About
     */


    Route::get('About','AboutController@index')->name('about');


    /*
     * Contact
     */


    Route::get('Contact','ContactController@index')->name('contact');
    Route::post('Send-Contact','ContactController@send_contact')->name('send-contact');


    /*
     * Cart
     */

    Route::get('cart', 'CartController@index')->name('cart.index');
    Route::post('Cart-change','CartController@Cart_change')->name('Cart-change');
    Route::post('Get-Cities','CartController@get_city')->name('get-city');
    Route::post('Get-Delivery','CartController@get_delivery')->name('get-delivery');
    Route::post('Cart-Delete','CartController@Cart_Delete')->name('Cart-Delete');
    Route::post('Check-Out','CartController@check_out')->name('check-out');
    Route::post('check-coupon','CartController@check_coupon')->name('check-coupon');


    Route::post('add-size', 'ShopController@add_size')->name('add-size');



    /*
     * Wish List
     */

    Route::get('Wishlist','WishListController@index')->name('site.wishlist');
    Route::post('Wish-change','WishListController@wish_change')->name('wish-change');
    Route::post('Cart-Add','WishListController@Cart_add')->name('Cart-add');
    Route::post('Wish-Delete','WishListController@wish_delete')->name('wish-delete');


    /*
     * Profile
     */

    Route::get('Profile','ProfileController@index')->name('site-profile');
    Route::post('Profile-Update','ProfileController@update_info')->name('profile.update');
    Route::post('Password-Edit','ProfileController@edit_password')->name('password.edit');


});


/*
 * Site Routes That Needs To Be Logged To Access
 */


Route::group(['namespace' => 'Site','middleware'=>'site'], function () {



    /*
     * Cart
     */

    Route::get('Cart','CartController@index')->name('cart');


    /*
     * Wish List
     */

    Route::get('Wish','WishListController@index')->name('wish');

});

