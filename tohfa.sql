-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2018 at 03:10 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tohfa`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_main`
--

CREATE TABLE `about_main` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_main`
--

INSERT INTO `about_main` (`id`, `title`, `content`, `date`, `img`, `created_at`, `updated_at`) VALUES
(1, 'لماذا نحن؟', 'Mellentesque faucibus dapibus dapibus. Morbi aliquam aliquet neque. Donec placerat dapibus sollicitudin. Morbi arcu nisi, mattis ut ullamcorper in, dapibus ac nunc. Cras bibendum mauementum lectus ultrices bibendumMellentesque fa', '2018-10-10', '13691537952297.jpg', NULL, '2018-10-10 11:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `about_sub`
--

CREATE TABLE `about_sub` (
  `id` int(10) UNSIGNED NOT NULL,
  `title1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_sub`
--

INSERT INTO `about_sub` (`id`, `title1`, `content1`, `title2`, `content2`, `title3`, `content3`, `created_at`, `updated_at`) VALUES
(1, 'رسالتنا', 'hjhsnhshjshdjsahxnxbns', 'هدفنا', 'Huis nostrud exerci tation ullamcorper suscipites', 'مهمتنا', 'jkijjjjjjjjjjjj ikkkkkkkkkkkkk iiiiiiiiiiiiiii', NULL, '2018-10-16 08:39:27');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delivery` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `country_id`, `created_at`, `updated_at`, `delivery`) VALUES
(1, 'الف مسكن', '1', '2018-09-30 07:04:19', '2018-09-30 07:04:19', '200'),
(2, 'اول جمال', '1', '2018-09-30 07:04:55', '2018-09-30 07:04:55', '200'),
(7, '2', '1', '2018-09-30 09:13:23', '2018-09-30 09:13:23', '2'),
(8, '21', '1', '2018-09-30 09:14:01', '2018-09-30 09:14:01', '1'),
(9, 'test', '2', '2018-10-03 14:04:43', '2018-10-03 14:04:43', '100'),
(10, '2test', '2', '2018-10-03 14:04:49', '2018-10-03 14:04:49', '300');

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'i28Ib', '2018-10-02 08:45:34', '2018-10-02 13:38:50'),
(2, 22, 'B9AVY', '2018-10-11 10:21:22', '2018-10-11 10:21:22');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `class`, `created_at`, `updated_at`) VALUES
(1, 'أحمر', 'red', '0000-00-00 00:00:00', NULL),
(2, 'أصفر', 'yellow', NULL, NULL),
(3, 'أخضر', 'green', NULL, NULL),
(4, 'بني', 'brown', NULL, NULL),
(5, 'أزرق', 'blue', NULL, NULL),
(6, 'بنفسجي', 'violet', NULL, NULL),
(7, 'أبيض ', 'white', NULL, NULL),
(8, 'إسود', 'black', NULL, NULL),
(9, 'رصاصى', 'gray', NULL, NULL),
(10, 'بيج', 'beige', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `color_product`
--

CREATE TABLE `color_product` (
  `color_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `color_product`
--

INSERT INTO `color_product` (`color_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(4, 2, NULL, NULL),
(1, 3, NULL, NULL),
(4, 3, NULL, NULL),
(1, 19, NULL, NULL),
(1, 20, NULL, NULL),
(2, 20, NULL, NULL),
(3, 20, NULL, NULL),
(5, 20, NULL, NULL),
(6, 20, NULL, NULL),
(7, 20, NULL, NULL),
(8, 20, NULL, NULL),
(9, 20, NULL, NULL),
(10, 20, NULL, NULL),
(4, 21, NULL, NULL),
(5, 21, NULL, NULL),
(8, 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `phone`, `address`, `email`, `created_at`, `updated_at`) VALUES
(1, '0123456789', '32 شارع الزهور , مدينة العبور', 'Info@yoursite.com', NULL, '2018-10-10 12:22:47');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'مصر', '2018-09-26 18:05:44', '2018-09-26 18:19:11'),
(2, 'test', '2018-10-03 14:04:36', '2018-10-03 14:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bbox` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grand_total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `name`, `phone`, `country`, `city`, `address`, `delivery`, `bbox`, `grand_total`, `created_at`, `updated_at`) VALUES
(18, 'iki', '123456', 'مصر', 'الف مسكن', '332 شارع الزهور , مدينة العبور', '200', '11111', '150', '2018-10-10 11:39:50', '2018-10-10 11:39:50'),
(19, 'Test', '123456', 'مصر', 'الف مسكن', '332 شارع الزهور , مدينة العبور', '200', '11111', '0', '2018-10-11 10:23:37', '2018-10-11 10:23:37'),
(20, 'zozo', '2', 'مصر', 'الف مسكن', '1', '200', '1', '0', '2018-10-16 08:33:09', '2018-10-16 08:33:09'),
(21, 'iki', '2', 'مصر', 'الف مسكن', '1', '200', '3', '1500', '2018-10-16 08:34:33', '2018-10-16 08:34:34'),
(22, 'iki', '123456', 'مصر', 'الف مسكن', '332 شارع الزهور , مدينة العبور', '200', '11111', '2150', '2018-10-16 09:08:02', '2018-10-16 09:08:02');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `invoice_id`, `quantity`, `product_id`, `total`, `created_at`, `updated_at`) VALUES
(22, 18, '1', '19', '150', '2018-10-10 11:39:50', '2018-10-10 11:39:50'),
(23, 21, '5', '2', '750', '2018-10-16 08:34:34', '2018-10-16 08:34:34'),
(24, 21, '2', '3', '300', '2018-10-16 08:34:34', '2018-10-16 08:34:34'),
(25, 21, '3', '19', '450', '2018-10-16 08:34:34', '2018-10-16 08:34:34'),
(26, 22, '1', '3', '150', '2018-10-16 09:08:02', '2018-10-16 09:08:02'),
(27, 22, '1', '20', '2000', '2018-10-16 09:08:02', '2018-10-16 09:08:02');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_18_082458_create_roles_table', 2),
(4, '2018_09_18_082630_create_role_user_table', 3),
(5, '2018_09_24_162542_create_sections_table', 4),
(6, '2018_09_25_091558_create_products_table', 5),
(7, '2018_09_25_100523_create_colors_table', 5),
(8, '2018_09_25_100615_create_color_product_table', 6),
(9, '2018_09_25_102319_create_prices_table', 6),
(10, '2018_09_26_082637_create_about_main_table', 7),
(11, '2018_09_26_082654_create_about_sub_table', 7),
(12, '2018_09_26_092912_create_settings_table', 8),
(13, '2018_09_26_095610_create_sliders_table', 9),
(14, '2018_09_26_101614_create_contacts_table', 10),
(15, '2018_09_26_191019_create_messages_table', 11),
(16, '2018_09_26_195834_create_countries_table', 12),
(17, '2018_09_26_200705_create_cities_table', 13),
(18, '2018_09_30_091020_add_delivery_to_cities', 14),
(19, '2018_09_30_132130_create_notifications_table', 15),
(20, '2018_10_02_100950_create_codes_table', 16),
(21, '2018_10_02_161444_create_carts_table', 17),
(22, '2018_10_03_110818_create_wish_lists_table', 18),
(23, '2018_10_04_133920_create_invoices_table', 19),
(24, '2018_10_04_135313_create_invoice_details_table', 20);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('145df07b-bfc4-42ff-be53-137ab84914d8', 'App\\Notifications\\NewNotification', 'App\\User', 17, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-10 11:02:40', '2018-10-10 11:02:40'),
('1b3ef90e-f5a3-457b-a5e4-c888affa0702', 'App\\Notifications\\NewNotification', 'App\\User', 16, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-01 08:51:28', '2018-10-01 08:51:28'),
('35501bff-e7ec-4339-ab4f-c0473e816803', 'App\\Notifications\\NewNotification', 'App\\User', 16, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-10 11:02:40', '2018-10-10 11:02:40'),
('50cdc028-0ebf-40a5-9e28-e09eeb64e43f', 'App\\Notifications\\NewNotification', 'App\\User', 16, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-10 11:03:16', '2018-10-10 11:03:16'),
('6f41ac6f-6568-4fcc-aa87-497c74f7dd7a', 'App\\Notifications\\NewNotification', 'App\\User', 1, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-10 11:03:16', '2018-10-10 11:03:16'),
('854729a2-4996-499f-a279-9bfd9c1cfb5d', 'App\\Notifications\\NewNotification', 'App\\User', 16, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-01 08:58:15', '2018-10-01 08:58:15'),
('93a43f8d-9eba-4768-aec6-28b535cab38a', 'App\\Notifications\\NewNotification', 'App\\User', 17, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-10 11:03:16', '2018-10-10 11:03:16'),
('a54e45a1-60ee-41ab-9803-02104a153128', 'App\\Notifications\\NewNotification', 'App\\User', 1, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', '2018-10-01 08:58:26', '2018-10-01 08:58:15', '2018-10-01 08:58:26'),
('ab21c0c9-0f90-49d7-a528-5f6b5c076e19', 'App\\Notifications\\NewNotification', 'App\\User', 1, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', '2018-10-01 08:57:43', '2018-10-01 08:51:28', '2018-10-01 08:57:43'),
('bea13126-1cf1-4bad-96ef-78f94976cbce', 'App\\Notifications\\NewNotification', 'App\\User', 1, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-10 11:02:40', '2018-10-10 11:02:40'),
('ecf1b82a-f172-45f3-b15c-d055151897d4', 'App\\Notifications\\NewNotification', 'App\\User', 17, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-01 08:58:15', '2018-10-01 08:58:15'),
('edca1d9d-9ad4-4a89-b54e-dbb2d8c8362a', 'App\\Notifications\\NewNotification', 'App\\User', 17, '{\"Message\":\" \\u062a\\u0645 \\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0631\\u0633\\u0627\\u0644\\u0647 \\u062c\\u062f\\u064a\\u062f\\u0647\",\"Url\":\"message\"}', NULL, '2018-10-01 08:51:28', '2018-10-01 08:51:28');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_before` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_after` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_id` int(11) NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `code`, `price_before`, `price_after`, `quantity`, `section_id`, `category`, `description`, `img`, `is_active`, `created_at`, `updated_at`) VALUES
(2, 'product 1', '123', '200', '150', '500', 2, 'جديد', 'description', '90321537969345.jpg', 1, '2018-09-25 09:53:01', '2018-10-02 15:31:37'),
(3, 'ibrahem', '63794', '200', '150', '54', 3, 'مميز', 'wqe', '86081537969186.jpg', 1, '2018-09-25 09:59:26', '2018-09-26 13:12:01'),
(19, 'BasketBall Item', '63794w3', '200', '150', '500', 4, 'مميز', 'w', '65471537969166.jpg', 1, '2018-09-26 11:33:29', '2018-09-26 13:28:42'),
(20, 'mndndn', '1232', '0', '2000', '23141', 2, 'مميز', '3', '48661537970851.jpg', 1, '2018-09-26 12:07:31', '2018-09-26 12:09:20'),
(21, 'mahmoud', '200', '1000', '1200', '200', 5, 'مميز', 'محمود مبسوط اووى', '40271539170492.jpg', 1, '2018-10-10 11:21:32', '2018-10-10 11:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'المشرفين', NULL, NULL),
(2, 'الاقسام', NULL, NULL),
(3, 'المنتجات', NULL, NULL),
(4, 'الفواتير', NULL, NULL),
(5, 'المستخدمين', NULL, NULL),
(6, 'الرسائل', NULL, NULL),
(7, 'من نحن', NULL, NULL),
(8, 'الاعدادات', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(19, 16, 1, NULL, NULL),
(20, 16, 4, NULL, NULL),
(21, 17, 1, NULL, NULL),
(22, 17, 4, NULL, NULL),
(23, 17, 2, NULL, NULL),
(25, 17, 5, NULL, NULL),
(29, 16, 2, NULL, NULL),
(30, 16, 3, NULL, NULL),
(31, 16, 5, NULL, NULL),
(32, 16, 6, NULL, NULL),
(33, 16, 7, NULL, NULL),
(34, 16, 8, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'mahmoudhh', '2018-09-24 14:48:42', '2018-10-10 11:19:58'),
(3, 'القسم الثانى', '2018-09-26 13:11:16', '2018-09-26 13:11:16'),
(4, 'القسم الثالث', '2018-09-26 13:11:23', '2018-09-26 13:11:23'),
(5, 'mahmoud', '2018-10-10 11:18:07', '2018-10-10 11:18:07'),
(6, 'ىلاا', '2018-10-10 11:35:07', '2018-10-10 11:35:07');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `water_mark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `facebook`, `twitter`, `instagram`, `logo`, `icon`, `water_mark`, `site_name`, `site_description`, `created_at`, `updated_at`) VALUES
(1, 'https://www.facebook.com', 'https://twitter.com', 'https://www.instagram.com', '66501539171353.jpg', '68811539169769.jpg', '44881539171515.jpg', 'Tohfa', 'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', NULL, '2018-10-10 12:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `img`, `created_at`, `updated_at`) VALUES
(6, '57721537973587.jpg', '2018-09-26 12:53:07', '2018-09-26 12:53:07'),
(8, '10051537973601.jpg', '2018-09-26 12:53:21', '2018-09-26 12:53:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `email_verified_at`, `password`, `user_type`, `is_active`, `img`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'iki', 'ibrahem.kamal70@gmail.com', '123456', NULL, '$2y$10$lEvndNcVtVwaEeYhvGvCjuB9L6B77KxO241v8vi7N1AmOlDotq5iK', 'admin', 1, '5841537961432.jpg', '9TzuRpHKTDkpJ1NjKNMc4Ir4vPwC0j7pgLvs0V09dikLDnFdBjCO5GuUUdX0', NULL, '2018-10-16 08:35:12'),
(16, 'ibrahem', 'ibrahem.kamal95@yahoo.com', NULL, NULL, '$2y$10$ghXbHkBeT64/xI13oTI6QenGZ8r1TfO7K2KKX827WDhPLnNKasxaO', 'admin', 1, NULL, NULL, '2018-09-24 13:57:56', '2018-09-24 13:57:56'),
(17, 'BasketBall Item', 'ibrahem.kamalb71@gmail.com', NULL, NULL, '$2y$10$HgPBha4gXv4pLG5yXjaIhubjdgSQAowW67/AQJgou9FUxEm/Dwrxy', 'admin', 1, NULL, NULL, '2018-09-24 13:59:35', '2018-09-24 13:59:35'),
(22, 'zoz', 'zeinabmostafa29@outlook.sa', '01090466590', NULL, '$2y$10$J5To3erZoTM5rRMrmP.0d.8iXSGIQ0V7Es6LWZzDgImjGSCIfOeee', 'user', 1, NULL, NULL, '2018-10-10 11:16:41', '2018-10-10 11:16:53'),
(25, 'zozo', 'zoz@zoz.com', '123456789', NULL, '$2y$10$9/uLCqkdt3NN/xV72x072uohNFHg7h0RMAhqtxjIpcInCPgxSw20W', 'user', 1, NULL, 'tKj7fzeNlI8BVgLXP1LwhARrgGPEOHiuVB1FWCimZNvvoYYcRvvfG2SlhJyX', '2018-10-16 08:22:11', '2018-10-16 08:22:11');

-- --------------------------------------------------------

--
-- Table structure for table `wish_lists`
--

CREATE TABLE `wish_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wish_lists`
--

INSERT INTO `wish_lists` (`id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(24, 20, 25, '2018-10-16 08:31:44', '2018-10-16 08:31:44'),
(25, 19, 25, '2018-10-16 08:31:47', '2018-10-16 08:31:47'),
(26, 3, 25, '2018-10-16 08:31:50', '2018-10-16 08:31:50'),
(27, 2, 25, '2018-10-16 08:31:52', '2018-10-16 08:31:52'),
(28, 21, 25, '2018-10-16 08:31:56', '2018-10-16 08:31:56'),
(36, 2, 1, '2018-10-16 13:08:24', '2018-10-16 13:08:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_main`
--
ALTER TABLE `about_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about_sub`
--
ALTER TABLE `about_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_user_id_foreign` (`user_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codes_user_id_foreign` (`user_id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color_product`
--
ALTER TABLE `color_product`
  ADD KEY `color_product_color_id_foreign` (`color_id`),
  ADD KEY `color_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_details_invoice_id_foreign` (`invoice_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wish_lists`
--
ALTER TABLE `wish_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wish_lists_product_id_foreign` (`product_id`),
  ADD KEY `wish_lists_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_main`
--
ALTER TABLE `about_main`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `about_sub`
--
ALTER TABLE `about_sub`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `wish_lists`
--
ALTER TABLE `wish_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `codes`
--
ALTER TABLE `codes`
  ADD CONSTRAINT `codes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `color_product`
--
ALTER TABLE `color_product`
  ADD CONSTRAINT `color_product_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`),
  ADD CONSTRAINT `color_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD CONSTRAINT `invoice_details_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `wish_lists`
--
ALTER TABLE `wish_lists`
  ADD CONSTRAINT `wish_lists_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `wish_lists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
