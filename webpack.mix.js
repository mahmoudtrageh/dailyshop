const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

// admin

mix.styles([
    'manager/app-assets/css-rtl/vendors.css',
    'manager/app-assets/vendors/css/tables/datatable/datatables.min.css',
    'manager/app-assets/css-rtl/app.css',
    'manager/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css',
    'manager/app-assets/css-rtl/core/colors/palette-gradient.css',
    'manager/assets/css/style-rtl.css',
], 'public/css/admin.css');

mix.scripts([
    'manager/app-assets/vendors/js/vendors.min.js',
    'manager/app-assets/vendors/js/charts/chart.min.js',
    'manager/app-assets/vendors/js/charts/raphael-min.js',
    'manager/app-assets/vendors/js/charts/morris.min.js',
    'manager/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js',
    'manager/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js',
    'manager/app-assets/data/jvector/visitor-data.js',
    'manager/app-assets/js/core/app-menu.js',
    'manager/app-assets/js/core/app.js',
    'manager/app-assets/js/scripts/customizer.js',
    'manager/app-assets/js/scripts/pages/dashboard-sales.js',
], 'public/js/admin.js');

// site

mix.styles([
    'site/css/font-awesome.css',
    'site/css/bootstrap.css',
    'site/css/jquery.smartmenus.bootstrap.css',
    'site/css/jquery.simpleLens.css',
    'site/css/slick.css',
    'site/css/nouislider.css',
    'site/css/theme-color/default-theme.css',
    'site/css/sequence-theme.modern-slide-in.css',
    'site/css/style.css',
],'public/css/site.css');

mix.scripts([
    'site/js/bootstrap.js',
    'site/js/jquery.smartmenus.js',
    'site/js/jquery.smartmenus.bootstrap.js',
    'site/js/sequence.js',
    'site/js/sequence-theme.modern-slide-in.js',
    'site/js/jquery.simpleGallery.js',
    'site/js/slick.js',
    'site/js/nouislider.js',
    'site/js/custom.js',
],'public/js/site.js');